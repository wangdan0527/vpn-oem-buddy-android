package com.keepsolid.vpn.buddy.rest

import android.content.Context
import androidx.databinding.ObservableField
import com.keepsolid.vpn.buddy.interstitial.InterstitialViewModel
import com.keepsolid.vpn.buddy.utils.SharedPrefManager
import retrofit2.http.GET
import io.fabric.sdk.android.services.settings.IconRequest.build
import okhttp3.*
import java.io.IOException


class ChartMogulService{

    companion object {

        private var DATASOURCE_ID: String = "ds_5aac0af0-b440-11e9-bca0-43731a01e610"
        private var AUTH_TOKEN: String = "MmMyMjkyMmVhNzllYjA5YWNhMzE2YzU5NTU2NjM1MDQ6MDY5NDdmOWU2NmU4ZmI4NDNlODk3YTdiYTAzNzI5YTc="

        public fun postPuchaseData(context: Context, productID: String, purchaseToken: String)
        {
            var sharedPrefManager :SharedPrefManager = SharedPrefManager(context);
            var email: String = sharedPrefManager.getEmail()


            var content = "{\n\t\"purchase_data\": {\n     \t\"product_id\": \"" + productID + "\",\n      \t\"purchase_token\": \"" + purchaseToken + "\"\n      },\n    \"customer\": {\n        \"email\": \"" + email + "\"\n    }\n}"
            val client = OkHttpClient()

            val mediaType = MediaType.parse("application/json")
            val body = RequestBody.create(mediaType, content)
            val request = Request.Builder()
                    .url("https://googleplay.chartmogul.com/data_sources/" + DATASOURCE_ID + "/purchases")
                    .post(body)
                    .addHeader("Content-Type", "application/json")
                    .addHeader("Authorization", "Basic " + AUTH_TOKEN)
                    .addHeader("Host", "googleplay.chartmogul.com")
                    .addHeader("Connection", "keep-alive")
                    .addHeader("cache-control", "no-cache")
                    .build()

            client.newCall(request).enqueue(object: Callback{
                override fun onFailure(call: Call, e: IOException) {
                }

                override fun onResponse(call: Call, response: Response) {
                    var a : Int = 0
                }
            })

        }
    }
}