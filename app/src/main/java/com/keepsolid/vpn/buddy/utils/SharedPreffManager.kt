package com.keepsolid.vpn.buddy.utils

import android.content.Context
import android.text.TextUtils
import com.android.billingclient.api.SkuDetails
import com.keepsolid.androidkeepsolidcommon.vpnunlimitedsdk.entities.VPNUProtoConfig
import com.keepsolid.vpn.buddy.utils.Constans.PrefValues.Companion.AVAILABLE_PROTOCOLS_LIST
import com.keepsolid.vpn.buddy.utils.Constans.PrefValues.Companion.FAVORITE_SERVERS_SET
import com.keepsolid.vpn.buddy.utils.Constans.PrefValues.Companion.FINGERPRINT_ENABLED
import com.keepsolid.vpn.buddy.utils.Constans.PrefValues.Companion.PREFERRED_PROTOCOLS_AUTO_ENABLED
import com.keepsolid.vpn.buddy.utils.Constans.PrefValues.Companion.PREFERRED_PROTOCOLS_CURRENT
import com.keepsolid.vpn.buddy.utils.Constans.PrefValues.Companion.PREFERRED_PROTOCOLS_INITIALIZED
import com.keepsolid.vpn.buddy.utils.Constans.PrefValues.Companion.PREF_EMAIL
import com.keepsolid.vpn.buddy.utils.Constans.PrefValues.Companion.PREF_PASSWORD
import com.keepsolid.vpn.buddy.utils.Constans.PrefValues.Companion.REMEMBER_PASSWORD_ENABLED
import com.keepsolid.vpn.buddy.utils.Constans.PrefValues.Companion.TRANSACTION_DATA
import com.keepsolid.vpn.buddy.utils.Constans.PrefValues.Companion.VPN_PROFILE_RELOAD_NEEDED
import com.keepsolid.vpn.buddy.utils.Constans.PrefValues.Companion.VPN_PROTO_PREFERRED
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton
import kotlin.collections.HashSet

@Singleton
class SharedPrefManager @Inject constructor(context: Context) {

    private val sharedPreferences = context.getSharedPreferences(Constans.PrefValues.MY_PREF, Context.MODE_PRIVATE)

    internal fun saveEmail(email: String) {
        sharedPreferences.edit().putString(PREF_EMAIL, email).apply()
    }

    internal fun getEmail() = sharedPreferences.getString(PREF_EMAIL, "") ?: ""

    internal fun savePassword(password: String) {
        sharedPreferences.edit().putString(PREF_PASSWORD, password).apply()
    }

    internal fun getPassword() = sharedPreferences.getString(PREF_PASSWORD, "") ?: ""


    internal fun saveFavorite(value: Set<String>) {
        sharedPreferences.edit().putStringSet(FAVORITE_SERVERS_SET, value).apply()
    }

    internal fun getFavorite(): HashSet<String> {
        return HashSet(sharedPreferences.getStringSet(FAVORITE_SERVERS_SET, HashSet())
                ?: mutableSetOf())
    }

    internal fun setPreferredProtocol(protoConfig: VPNUProtoConfig) {
        sharedPreferences.edit().putString(VPN_PROTO_PREFERRED, protoConfig.toString()).apply()
    }

    internal fun getPreferredProtocol(): VPNUProtoConfig {
        return VPNUProtoConfig(sharedPreferences.getString(VPN_PROTO_PREFERRED, null))
    }

    internal fun setPreferredProtocolsInitialized(initialized: Boolean) {
        sharedPreferences.edit().putBoolean(PREFERRED_PROTOCOLS_INITIALIZED, initialized).apply()
    }

    internal fun isPreferredProtocolsInitialized(): Boolean {
        return sharedPreferences.getBoolean(PREFERRED_PROTOCOLS_INITIALIZED, false)
    }

    internal fun saveCurrentVPNUProtoConfig(vpnuProtoConfig: VPNUProtoConfig?) {
        if (vpnuProtoConfig != null) {
            sharedPreferences.edit().putString(PREFERRED_PROTOCOLS_CURRENT, vpnuProtoConfig.toString()).apply()
        }

    }

    internal fun getCurrentVPNUProtoConfig(): VPNUProtoConfig {
        return VPNUProtoConfig(sharedPreferences.getString(PREFERRED_PROTOCOLS_CURRENT, null))
    }

    internal fun getVPNUProtoConfigToConnect(): VPNUProtoConfig {
        if (isVpnProtoAuto() && getCurrentVPNUProtoConfig() != getPreferredProtocol()) {
            saveCurrentVPNUProtoConfig(getPreferredProtocol())
        }
        return getCurrentVPNUProtoConfig()
    }

    internal fun setRememberPasswordEnabled(isEnabled: Boolean) {
        sharedPreferences.edit().putBoolean(REMEMBER_PASSWORD_ENABLED, isEnabled).apply()
    }

    internal fun isRememberPasswordEnabled(): Boolean {
        return sharedPreferences.getBoolean(REMEMBER_PASSWORD_ENABLED, true)
    }

    internal fun setFingerprintProtectionEnabled(isEnabled: Boolean) {
        sharedPreferences.edit().putBoolean(FINGERPRINT_ENABLED, isEnabled).apply()
    }

    internal fun isFingerprintProtectionEnabled(): Boolean {
        return sharedPreferences.getBoolean(FINGERPRINT_ENABLED, false)
    }

    internal fun setVpnProtoAuto(isProtoAuto: Boolean) {
        sharedPreferences.edit().putBoolean(PREFERRED_PROTOCOLS_AUTO_ENABLED, isProtoAuto).apply()
    }

    internal fun isVpnProtoAuto(): Boolean {
        return sharedPreferences.getBoolean(PREFERRED_PROTOCOLS_AUTO_ENABLED, true)
    }

    internal fun setProfileReloadNeeded(isReloadNeeded: Boolean) {
        sharedPreferences.edit().putBoolean(VPN_PROFILE_RELOAD_NEEDED, isReloadNeeded).apply()
    }

    internal fun isProfileReloadNeeded(): Boolean {
        val forceReload = sharedPreferences.getBoolean(VPN_PROFILE_RELOAD_NEEDED, false)
        val isProtoWise = getVPNUProtoConfigToConnect().protocolType == VPNUProtoConfig.ProtocolType.WISE

        return isProtoWise || forceReload
    }

    internal fun saveProtocolList(protoList: List<String>) {

        var availableProtocol: List<String> = protoList

        if (availableProtocol.isEmpty()) {
            availableProtocol = VPNUProtoConfig.getSupportedProtocolsString()
        }

        var preferredProtoConfig: VPNUProtoConfig? = null

        for (protocol in availableProtocol) {
            if (VPNUProtoConfig.isSupportedProto(protocol)) {
                preferredProtoConfig = VPNUProtoConfig(protocol)
                setPreferredProtocol(preferredProtoConfig)
                break
            }
        }

        if (!availableProtocol.contains(getCurrentVPNUProtoConfig().getProtoString())) {
            setVpnProtoAuto(true)
            setProfileReloadNeeded(true)
        }

        if (!isPreferredProtocolsInitialized()) {
            saveCurrentVPNUProtoConfig(preferredProtoConfig)
            setPreferredProtocolsInitialized(true)
        }

        sharedPreferences.edit().putString(AVAILABLE_PROTOCOLS_LIST, JSONArray(availableProtocol).toString()).apply()

    }

    internal fun getProtocolList(): List<String> {
        val arrayStr = sharedPreferences.getString(AVAILABLE_PROTOCOLS_LIST, null)
        var array: JSONArray? = null

        if (!TextUtils.isEmpty(arrayStr)) {
            array = JSONArray(arrayStr)
        }

        return try {
            if (array != null && array.length() > 0) {
                val arrayList = ArrayList<String>()
                for (i in 0 until array.length()) {
                    arrayList.add(array.getString(i))
                }
                arrayList
            } else {
                VPNUProtoConfig.getSupportedProtocolsString()
            }
        } catch (ex: JSONException) {
            VPNUProtoConfig.getSupportedProtocolsString()
        }

    }

    internal fun isUserLoggedIn(): Boolean {
        return sharedPreferences.getString(PREF_PASSWORD, null) != null
    }

    internal fun clearAccountSensitivePrefs() {
        sharedPreferences.edit()
                .remove(REMEMBER_PASSWORD_ENABLED)
                .remove(FINGERPRINT_ENABLED)
                .remove(PREF_PASSWORD)
                .apply()
    }

    internal fun putTransactionData(skuDetails: JSONObject)
    {
        var json: String = skuDetails.toString()
        var transactionData = HashSet<String>(sharedPreferences.getStringSet(TRANSACTION_DATA, HashSet<String>()))


        transactionData.add(json)

        sharedPreferences.edit().putStringSet(TRANSACTION_DATA, transactionData).commit()
    }

    internal fun putTransactionDataList(transactionData: Set<JSONObject>)
    {
        var transactionStringList : MutableSet<String> = mutableSetOf()
        for(i in 0 until transactionData.size)
        {
            transactionStringList.add(transactionData.elementAt(i).toString())
        }

        sharedPreferences.edit().putStringSet(TRANSACTION_DATA, transactionStringList).commit()
    }

    internal fun getTransactionData(): Set<JSONObject>
    {
        var transactionData = sharedPreferences.getStringSet(TRANSACTION_DATA, null)
        if(transactionData == null)
        {
            transactionData = mutableSetOf()
        }

        var transactionList : MutableSet<JSONObject> = mutableSetOf()

        for(i in 0 until transactionData.size)
        {
            var skuDetails = JSONObject(transactionData.elementAt(i))
            transactionList.add(skuDetails)
        }

        return transactionList
    }

}