package com.keepsolid.vpn.buddy.utils.models;

import android.text.TextUtils;

import java.util.HashMap;

public class PurchaseProviderInfo {

    private String sku;
    private String price;
    private String priceType;
    private HashMap<String, String> additionalInfo;

    public PurchaseProviderInfo(String sku, String price) {
        this.sku = sku;
        this.price = price;
    }

    public PurchaseProviderInfo(String sku, String price, String priceType) {
        this.sku = sku;
        this.price = price;
        this.priceType = priceType;
    }

    public String getSku() {
        return sku;
    }

    public void setSku(String sku) {
        this.sku = sku;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getPriceType() {
        return priceType;
    }

    public boolean hasPriceType() {
        return !TextUtils.isEmpty(priceType);
    }

    public void setPriceType(String priceType) {
        this.priceType = priceType;
    }

    public HashMap<String, String> getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(HashMap<String, String> additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public void addInfo(String key, String value) {
        if (additionalInfo == null) {
            additionalInfo = new HashMap<>();
        }
        additionalInfo.put(key, value);
    }

    @Override
    public int hashCode() {
        return sku.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        return o instanceof PurchaseProviderInfo && ((PurchaseProviderInfo) o).getSku().equals(getSku());
    }

    @Override
    public String toString() {
        return "\n" + "Sku: " + sku
                + "\n" + "Price: " + price;
    }
}
