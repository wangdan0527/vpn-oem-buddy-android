package com.keepsolid.vpn.buddy.vpn

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.keepsolid.androidkeepsolidcommon.vpnunlimitedsdk.entities.VPNUServer
import com.keepsolid.vpn.buddy.R
import com.keepsolid.vpn.buddy.base.BaseFragment
import com.keepsolid.vpn.buddy.base.IOnBackPressed
import com.keepsolid.vpn.buddy.databinding.FragmentVpnBinding
import com.keepsolid.vpn.buddy.interstitial.InterstitialFragment
import com.keepsolid.vpn.buddy.purchase.PurchaseFragment
import com.keepsolid.vpn.buddy.rest.ReceiptValidator
import com.keepsolid.vpn.buddy.servers.ServersFragment
import com.keepsolid.vpn.buddy.settings.SettingsFragment
import com.keepsolid.vpn.buddy.utils.AnimationUtils
import com.keepsolid.vpn.buddy.utils.NetworkProvider
import com.keepsolid.vpn.buddy.utils.ShowMsgManager
import com.keepsolid.vpn.buddy.utils.models.AccountStatusModel
import com.keepsolid.vpn.buddy.vpn.VpnViewModel.Companion.VPN_CONNECTED
import com.keepsolid.vpn.buddy.vpn.VpnViewModel.Companion.VPN_CONNECTING
import com.keepsolid.vpn.buddy.vpn.VpnViewModel.Companion.VPN_DISCONNECTED
import javax.inject.Inject

class VpnFragment @Inject constructor() : BaseFragment(), IOnBackPressed {

    private lateinit var model: VpnViewModel
    private lateinit var binding: FragmentVpnBinding

    override fun getLayoutRes() = R.layout.fragment_vpn

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = ViewModelProviders.of(this, viewModelFactory).get(VpnViewModel::class.java)
        hideKeyboard()

//        this.activity?.let { ReceiptValidator.validate(it) }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentVpnBinding.inflate(inflater, container, false)
        binding.model = model
        subscribe()
        model.vpnViewSubscribed()
        return binding.root
    }

    private fun subscribe() {
        model.vpnStatusChangedLiveData.observe(this, Observer<Int> { vpnStatusChanged(it) })
        model.accountStatusChangedLiveData.observe(this, Observer<AccountStatusModel> { updateServerInfo(it) })
        model.isNeedShowPurchasesScreenLiveData.observe(this, Observer<Boolean> { if (it) showFragment(PurchaseFragment::class.java.simpleName) })
        model.isNeedShowInterstitialScreenLiveData.observe(this, Observer<Boolean> { if (it) showFragment(InterstitialFragment::class.java.simpleName) })
        model.isNeedShowTrustedAddDialogLiveData.observe(this, Observer { if (it) showAddTrustedNetworksDialog() })
        model.isNeedShowTrustedRemoveDialogLiveData.observe(this, Observer { if (it) showRemoveTrustedNetworksDialog() })
        binding.vpnServersLayout.setOnClickListener { showFragment(ServersFragment::class.java.simpleName) }
        binding.vpnBtnSettings.setOnClickListener { showFragment(SettingsFragment::class.java.simpleName) }
    }

    private fun vpnStatusChanged(vpnStatus: Int) {
        when (vpnStatus) {
            VPN_DISCONNECTED -> {
                binding.vpnImgLightning.visibility = View.INVISIBLE
                binding.vpnServersLayout.setBackgroundResource(R.drawable.btn_stroke_primary_dark)
                binding.vpnBtnMain.setBackgroundColor(activity!!.resources.getColor(R.color.colorTextBlack))
                binding.vpnImgMain.setImageResource(R.drawable.img_man_disconnected)
                binding.vpnBtnMain.text = activity!!.resources.getText(R.string.btn_main_status_start)
                stopLightAnimation()
            }
            VPN_CONNECTING -> {
                binding.vpnImgMain.setImageResource(R.drawable.img_man_connecting)
                binding.vpnBtnMain.text = activity!!.resources.getText(R.string.btn_main_status_connecting)
                startLightAnimation()
            }
            VPN_CONNECTED -> {
                stopLightAnimation()
                binding.vpnServersLayout.setBackgroundResource(R.drawable.btn_stroke_connecnted)
                binding.vpnImgMain.setImageResource(R.drawable.img_man_connected)
                binding.vpnBtnMain.setBackgroundColor(activity!!.resources.getColor(R.color.colorConnected))
                binding.vpnBtnMain.text = activity!!.resources.getText(R.string.btn_main_status_stop)
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun updateServerInfo(accountStatusModel: AccountStatusModel) {
        if (accountStatusModel.isConnectionEnabled) {
            binding.vpnVirtualIp.text = accountStatusModel.virtualIP
        } else {
            binding.vpnRealIp.text = accountStatusModel.realIP
        }

        if (accountStatusModel.server == null) {
            binding.vpnServersLayoutTvChoose.visibility = View.VISIBLE
            binding.vpnServerFlag.visibility = View.INVISIBLE
            binding.vpnServerCountry.visibility = View.INVISIBLE
            binding.vpnServerCity.visibility = View.INVISIBLE
        } else {
            binding.vpnServersLayoutTvChoose.visibility = View.INVISIBLE

            binding.vpnServerCountry.text = accountStatusModel.server.name
            if (accountStatusModel.server.isOptimal) {
                binding.vpnServerCity.text = getString(R.string.optimal_server_desc)
            } else {
                binding.vpnServerCity.text = accountStatusModel.server.description
            }

            if (accountStatusModel.server.isOptimal) downloadOptimalImg()
            else downloadServerIcon(accountStatusModel.server)

            binding.vpnServerFlag.visibility = View.VISIBLE
            binding.vpnServerCountry.visibility = View.VISIBLE
            binding.vpnServerCity.visibility = View.VISIBLE
        }
    }

    private fun showAddTrustedNetworksDialog() {
        val ssid = NetworkProvider.getCurrentWiFiName(activity!!)
        var finalSSID = ssid
        if (ssid == null) {
            finalSSID = getString(R.string.settings_trusted_networks_lte)
        }

        val message = String.format(getString(R.string.S_ADD_TO_TRUSTED_DESC), finalSSID)
        val dialog = ShowMsgManager.showAlertDialog(activity!!, getString(R.string.information),
                message,
                getString(R.string.S_STAY_CONNECTED),
                Runnable { model.onTrustedAddDialogNegativeClick() },
                getString(R.string.S_ADD_BTN),
                Runnable { model.onTrustedAddDialogPositiveClick(ssid) })
        dialog.setOnCancelListener { model.onTrustedAddDialogNegativeClick() }
        dialog.show()


    }

    private fun showRemoveTrustedNetworksDialog() {
        val ssid = NetworkProvider.getCurrentWiFiName(activity!!)
        var finalSSID = ssid
        if (ssid == null) {
            finalSSID = getString(R.string.settings_trusted_networks_lte)
        }

        val message = String.format(getString(R.string.S_REMOVE_FROM_TRUSTED_DESC), finalSSID)
        val dialog = ShowMsgManager.showAlertDialog(activity!!, getString(R.string.information),
                message,
                getString(R.string.S_STAY_NOT_CONNECTED),
                Runnable { model.onTrustedRemoveDialogNegativeClick() },
                getString(R.string.S_REMOVE_BTN),
                Runnable { model.onTrustedRemoveDialogPositiveClick(ssid) })
        dialog.setOnCancelListener { model.onTrustedRemoveDialogNegativeClick() }
        dialog.show()
    }

    private fun downloadServerIcon(server: VPNUServer) {
        Glide.with(this)
                .load(server.iconUrl)
                .centerCrop()
                .fitCenter()
                .into(binding.vpnServerFlag)
    }

    private fun downloadOptimalImg() {
        Glide.with(this)
                .load(R.drawable.ic_optimal)
                .centerCrop()
                .fitCenter()
                .into(binding.vpnServerFlag)
    }

    private fun startLightAnimation() {
        binding.vpnImgLightning.visibility = View.VISIBLE
        AnimationUtils.startLightAnimation(binding.vpnImgLightning)
    }

    private fun stopLightAnimation() {
        binding.vpnImgLightning.visibility = View.INVISIBLE
    }

    override fun onBackPressed(): Boolean {
        return true
    }
}