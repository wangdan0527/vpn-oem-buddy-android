package com.keepsolid.vpn.buddy.utils.recycler

import android.view.View
import androidx.recyclerview.widget.RecyclerView


abstract class AbstractViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

    abstract fun buildView(metadata: AbstractRecyclerItem)

}
