package com.keepsolid.vpn.buddy.auth

import com.keepsolid.vpn.buddy.R
import com.keepsolid.vpn.buddy.base.BaseActivity
import dagger.Lazy
import javax.inject.Inject

class SignInActivity : BaseActivity() {

    @Inject
    internal lateinit var providerSignInFragment: Lazy<SignInFragment>

    override fun getLayoutRes(): Int = R.layout.activity_sign_in

    override fun getContainerId() = R.id.fragment_container

    override fun getDefaultFragment() = providerSignInFragment.get()


}