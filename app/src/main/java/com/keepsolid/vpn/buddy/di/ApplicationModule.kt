package com.keepsolid.vpn.buddy.di

import android.app.Application
import android.content.Context
import android.os.Build
import androidx.annotation.RequiresApi
import com.keepsolid.vpn.buddy.base.ViewModelFactory
import com.keepsolid.vpn.buddy.network.KeepSolidApi
import com.keepsolid.vpn.buddy.utils.EventManager
import com.keepsolid.vpn.buddy.utils.MainUtils
import com.keepsolid.vpn.buddy.utils.SharedPrefManager
import com.keepsolid.vpn.buddy.utils.ShowMsgManager
import com.keepsolid.vpn.buddy.utils.fingerprints.GoogleFingerprintProvider
import dagger.Binds
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module
abstract class ApplicationModule {

    @Binds
    internal abstract fun bindContext(application: Application): Context

    @Module
    companion object {

        @Provides
        @Singleton
        @JvmStatic
        fun provideSharedPrefManager(context: Context) = SharedPrefManager(context)

        @RequiresApi(Build.VERSION_CODES.M)
        @Provides
        @Singleton
        @JvmStatic
        fun provideFingerprintManager(context: Context) = GoogleFingerprintProvider(context)

        @Provides
        @Singleton
        @JvmStatic
        fun provideViewModelFactory(context: Context, ksApi: KeepSolidApi,
                                    sharedPrefManager: SharedPrefManager, eventManager: EventManager, fingerprintManager:GoogleFingerprintProvider)
                = ViewModelFactory(context, ksApi, sharedPrefManager, fingerprintManager, eventManager)

        @Provides
        @Singleton
        @JvmStatic
        fun provideShowMsgManager(context: Context) = ShowMsgManager(context)

        @Provides
        @Singleton
        @JvmStatic
        fun provideKeepSolidApi(sharedPrefManager: SharedPrefManager) = KeepSolidApi(sharedPrefManager)

        @Provides
        @Singleton
        @JvmStatic
        fun provideMainUtils(context: Context, ksApi: KeepSolidApi) = MainUtils(context, ksApi)

        @Provides
        @Singleton
        @JvmStatic
        fun provideEventManager(context: Context) = EventManager(context)
    }

}