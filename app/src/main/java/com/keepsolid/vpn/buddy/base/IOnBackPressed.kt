package com.keepsolid.vpn.buddy.base

interface IOnBackPressed {

    fun onBackPressed(): Boolean

}