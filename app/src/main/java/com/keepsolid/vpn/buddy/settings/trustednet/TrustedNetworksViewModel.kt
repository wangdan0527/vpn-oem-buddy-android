package com.keepsolid.vpn.buddy.settings.trustednet

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.os.Build
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.lifecycle.MutableLiveData
import com.keepsolid.vpn.buddy.R
import com.keepsolid.vpn.buddy.base.BaseViewModel
import com.keepsolid.vpn.buddy.network.KeepSolidApi
import com.keepsolid.vpn.buddy.utils.NetworkProvider
import com.keepsolid.vpn.buddy.utils.SharedPrefManager
import com.keepsolid.vpn.buddy.utils.recycler.AbstractRecyclerItem
import com.keepsolid.vpn.buddy.utils.recycler.TrustedNetworkRecyclerItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class TrustedNetworksViewModel(private val context: Context,
                               private val ksApi: KeepSolidApi,
                               private val sharedPrefManager: SharedPrefManager) : BaseViewModel() {

    private val trustedNetworksItems = mutableListOf<AbstractRecyclerItem>()

    val currentNetworkUpdateLiveData = MutableLiveData<String>()
    val updateTrustedListLiveDeta = MutableLiveData<MutableList<AbstractRecyclerItem>>()
    val currentNetworkAddedLiveData = MutableLiveData<Boolean>()
    val lteNetworkAddedLiveData = MutableLiveData<Boolean>()
    val showLocationDialogLiveData = MutableLiveData<Boolean>()
    val showPermissionDialogLiveData = MutableLiveData<Boolean>()

    internal inner class NetworkActionReceiver : BroadcastReceiver() {

        override fun onReceive(context: Context, intent: Intent) {
            setCurrentNetwork()
        }

    }

    private val receiver = NetworkActionReceiver()

    fun subscribe() {
        uiScope.launch {
            withContext(Dispatchers.IO) {
                if (!isLocationPermissionGranted()) {
                    showPermissionDialogLiveData.postValue(true)
                }
                generateList()
            }
        }
    }

    private fun generateList() {
        val trustedNetworksNames = ksApi.networkPrefsManager.trustedNetworkList
        val lteTrusted = ksApi.networkPrefsManager.isCellularNetworkTrusted

        trustedNetworksItems.clear()

        if (lteTrusted) {
            val lteItem = TrustedNetworkRecyclerItem(context.getString(R.string.settings_trusted_networks_lte))
            lteItem.onClickListener = View.OnClickListener { removeLte() }
            trustedNetworksItems.add(lteItem)
        }
        if (trustedNetworksNames != null && trustedNetworksNames.size > 0) {
            trustedNetworksNames.forEach { ssid ->
                val item = TrustedNetworkRecyclerItem(ssid)
                item.onClickListener = View.OnClickListener { removeNetwork(ssid) }
                trustedNetworksItems.add(item)
            }
        }

        updateTrustedListLiveDeta.postValue(trustedNetworksItems)

    }

    private fun setCurrentNetwork() {

        val currentSSID = NetworkProvider.getCurrentWiFiName(context)

        lteNetworkAddedLiveData.postValue(ksApi.networkPrefsManager.isCellularNetworkTrusted)

        currentNetworkUpdateLiveData.postValue(currentSSID)
        if (currentSSID == null || currentSSID.isEmpty()) {
            currentNetworkAddedLiveData.postValue(true)
            return
        }
        currentNetworkAddedLiveData.postValue(isNetworkTrusted(currentSSID))

    }

    fun onResumed() {
        setCurrentNetwork()
        val filter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        context.registerReceiver(receiver, filter)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1 &&
                isLocationPermissionGranted() &&
                !isLocationEnabled()) {
            showLocationDialogLiveData.postValue(true)
        }

    }

    private fun isLocationPermissionGranted(): Boolean {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O_MR1) {
            return true
        }
        return ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }

    private fun isLocationEnabled(): Boolean {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O_MR1) {
            return true
        }
        return NetworkProvider.isLocationEnabled(context)
    }

    fun onPaused() {
        context.unregisterReceiver(receiver)
    }

    private fun isNetworkTrusted(ssid: String): Boolean {
        return ksApi.networkPrefsManager.isNetworkTrusted(ssid)
    }

    fun addCurrentNetwork() {
        ksApi.networkPrefsManager.addNetworkToTrusted(NetworkProvider.getCurrentWiFiName(context))
        generateList()
        setCurrentNetwork()

    }

    fun removeNetwork(ssid: String) {
        ksApi.networkPrefsManager.removeNetworkFromTrusted(ssid)
        generateList()
        setCurrentNetwork()
    }

    fun removeLte() {
        ksApi.networkPrefsManager.isCellularNetworkTrusted = false
        generateList()
        setCurrentNetwork()
    }

    fun addLteNetwork() {
        ksApi.networkPrefsManager.isCellularNetworkTrusted = true
        generateList()
        setCurrentNetwork()
    }


}