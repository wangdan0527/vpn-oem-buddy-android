package com.keepsolid.vpn.buddy.utils.recycler

class TextContentRecyclerItem(val title: String,
                              override var sortPriority: Int = 0,
                              override var sortName: String = "") : AbstractRecyclerItem() {

    override val viewHolderType: Int = ViewHolderTypeProvider.ITEM_TEXT_CONTENT

}