package com.keepsolid.vpn.buddy.utils.models

import com.keepsolid.androidkeepsolidcommon.vpnunlimitedsdk.entities.VPNUServer

data class AccountStatusModel constructor(val realIP: String,
                                          val virtualIP: String = "",
                                          val server: VPNUServer? = null,
                                          val isConnectionEnabled: Boolean,
                                          val timeLeft: String)