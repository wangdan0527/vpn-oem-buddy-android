package com.keepsolid.vpn.buddy.utils.recycler

import android.widget.CompoundButton
import com.keepsolid.androidkeepsolidcommon.vpnunlimitedsdk.entities.VPNUServer

class ServerRecyclerItem(val server: VPNUServer,
                         var isConnected: Boolean) : AbstractRecyclerItem() {

    lateinit var onCheckedChangeListener: CompoundButton.OnCheckedChangeListener

    override val viewHolderType: Int = ViewHolderTypeProvider.ITEM_SERVER

    override var sortPriority: Int = 0

    var isFavorite: Boolean = false
        set(value) {
            sortPriority = if (value) {
                when {
                    server.priority == 100 -> PRIORITY_FAVORITE_OPTIMAL
                    server.priority == 14 -> PRIORITY_FAVORITE_STREAMING
                    else -> PRIORITY_FAVORITE_USUAL
                }
            } else {
                when {
                    server.priority == 100 -> PRIORITY_ALL_OPTIMAL
                    server.priority == 14 -> PRIORITY_ALL_STREAMING
                    else -> PRIORITY_ALL_USUAL
                }
            }
            field = value
        }

    override var sortName: String = server.name.toLowerCase()

}