package com.keepsolid.vpn.buddy.settings.protocol

import android.text.TextUtils
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.SwitchCompat
import com.keepsolid.vpn.buddy.R
import com.keepsolid.vpn.buddy.utils.recycler.AbstractRecyclerItem
import com.keepsolid.vpn.buddy.utils.recycler.AbstractViewHolder
import com.keepsolid.vpn.buddy.utils.recycler.OptimalProtocolRecyclerItem

class OptimalProtocolViewHolder(itemView: View) : AbstractViewHolder(itemView) {

    val name: TextView = itemView.findViewById(R.id.standard_recycler_item_title)
    private val description: TextView = itemView.findViewById(R.id.standard_recycler_item_subtitle)
    private val optimalSwitch: SwitchCompat = itemView.findViewById(R.id.sw_menu_toggle)
    private val holder: View = itemView.findViewById(R.id.standard_recycler_item_cv)

    override fun buildView(metadata: AbstractRecyclerItem) {
        val recyclerItem = metadata as OptimalProtocolRecyclerItem

        name.text = recyclerItem.protoname

        if (TextUtils.isEmpty(recyclerItem.protoDesc)) {
            description.visibility = View.GONE
        } else {
            description.text = recyclerItem.protoDesc
            description.visibility = View.VISIBLE
        }

        holder.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                optimalSwitch.requestFocus()
            }
        }

        optimalSwitch.setOnCheckedChangeListener(null)
        optimalSwitch.isChecked = recyclerItem.isChecked
        optimalSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            recyclerItem.isChecked = isChecked
            if (recyclerItem.onCheckedChangeListener != null) {
                recyclerItem.onCheckedChangeListener.onCheckedChanged(buttonView, isChecked)
            }
        }

        itemView.setOnClickListener(null)
    }

}
