package com.keepsolid.vpn.buddy.utils.models;

/**
 * Entity to subscrive VPNU purchase service.
 */
public enum PurchaseService {

    VPN, CHECKLIST_VPN, VPN_SLOT, VPN_IP, VPN_SERVER, UNDEF;

    private final static String VPN_VAL = "vpn";
    private final static String CHECKLIST_VPN_VAL = "checklistvpn";
    private final static String VPN_SLOT_VAL = "vpnslot";
    private final static String VPN_IP_VAL = "vpnip";
    private final static String VPN_SERVER_VAL = "vpnserver";
    private final static String UNDEF_VAL = "undef";

    @Override
    public String toString() {
        switch (this) {
            case VPN:
                return VPN_VAL;
            case CHECKLIST_VPN:
                return CHECKLIST_VPN_VAL;
            case VPN_SLOT:
                return VPN_SLOT_VAL;
            case VPN_IP:
                return VPN_IP_VAL;
            case VPN_SERVER:
                return VPN_SERVER_VAL;
            case UNDEF:
                return UNDEF_VAL;
        }
        return UNDEF_VAL;
    }

    public static PurchaseService parse(String serviceString) {
        switch (serviceString.toLowerCase()) {
            case VPN_VAL:
                return VPN;
            case CHECKLIST_VPN_VAL:
                return CHECKLIST_VPN;
            case VPN_SLOT_VAL:
                return VPN_SLOT;
            case VPN_IP_VAL:
                return VPN_IP;
            case VPN_SERVER_VAL:
                return VPN_SERVER;
        }

        return UNDEF;
    }

}
