package com.keepsolid.vpn.buddy.utils.recycler

import com.keepsolid.vpn.buddy.utils.models.PurchaseInfo

class PurchaseRecyclerItem(var purchaseInfo: PurchaseInfo) : AbstractRecyclerItem() {

    override val viewHolderType: Int = ViewHolderTypeProvider.ITEM_PURCHASE

    override var sortPriority: Int = PRIORITY_PURCHASE_ITEM

    override var sortName: String = ""

}