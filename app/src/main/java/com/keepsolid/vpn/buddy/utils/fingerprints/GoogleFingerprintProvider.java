package com.keepsolid.vpn.buddy.utils.fingerprints;

import android.app.Activity;
import android.content.Context;
import android.os.Build;

import javax.inject.Inject;
import javax.inject.Singleton;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.hardware.fingerprint.FingerprintManagerCompat;

@RequiresApi(api = Build.VERSION_CODES.M)
@Singleton
public class GoogleFingerprintProvider {

    private boolean isFeatureEnabled = false;
    private boolean isFingerprintAdded = false;
    private FingerprintManagerCompat fingerprintManager;
    AlertDialog fingerprintDialog;
    private Context context;

    @Inject
    public GoogleFingerprintProvider(Context context) {
        try {//kostil to avoid crashes on Samsung devices
            fingerprintManager = FingerprintManagerCompat.from(context);
            isFeatureEnabled = fingerprintManager.isHardwareDetected();
            isFingerprintAdded = fingerprintManager.hasEnrolledFingerprints();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void showIdentifyDialog(Activity activity, FingerprintIdentifyCallback callback) {

        if (isFingerprintAdded && activity != null && !activity.isFinishing()) {
            fingerprintDialog = new GoogleFingerprintDialog(activity, fingerprintManager, callback);
            activity.runOnUiThread(() -> fingerprintDialog.show());
        }

    }

    public void dismissIdentifyDialog() {
        if (fingerprintDialog != null && fingerprintDialog.isShowing()) {
            ((GoogleFingerprintDialog) fingerprintDialog).cancelAuthentication();
        }
    }

    public boolean isFeatureEnabled() {
        return isFeatureEnabled;
    }

    public boolean isFingerprintAdded() {
        if (fingerprintManager != null) {
            try {
                //kostil to avoid crashes on Samsung devices
                return fingerprintManager.hasEnrolledFingerprints();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return isFingerprintAdded;
    }

}
