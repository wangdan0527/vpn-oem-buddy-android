package com.keepsolid.vpn.buddy.utils.recycler

import android.widget.CompoundButton

class ProtocolRecyclerItem(var protoname: String, var protoDesc: String) : AbstractRecyclerItem() {

    lateinit var onCheckedChangeListener: CompoundButton.OnCheckedChangeListener

    override val viewHolderType: Int = ViewHolderTypeProvider.ITEM_PROTOCOL

    override var sortPriority: Int = 0

    var isChecked: Boolean = false
    var isEnabled: Boolean = true

    override var sortName: String = protoname.toLowerCase()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ProtocolRecyclerItem

        if (protoname != other.protoname) return false
        if (protoDesc != other.protoDesc) return false
        if (onCheckedChangeListener != other.onCheckedChangeListener) return false
        if (isChecked != other.isChecked) return false
        if (isEnabled != other.isEnabled) return false

        return true
    }

    override fun hashCode(): Int {
        var result = protoname.hashCode()
        result = 31 * result + protoDesc.hashCode()
        result = 31 * result + isChecked.hashCode()
        result = 31 * result + isEnabled.hashCode()
        return result
    }


}