package com.keepsolid.vpn.buddy.auth

import android.content.Context
import android.os.Build
import android.text.TextUtils
import androidx.annotation.RequiresApi
import androidx.databinding.ObservableField
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.MutableLiveData
import com.keepsolid.androidkeepsolidcommon.commonsdk.api.exceptions.KSException
import com.keepsolid.androidkeepsolidcommon.commonsdk.utils.stringutils.KSStringProviderManager
import com.keepsolid.vpn.buddy.R
import com.keepsolid.vpn.buddy.base.BaseViewModel
import com.keepsolid.vpn.buddy.di.ActivityScoped
import com.keepsolid.vpn.buddy.network.KeepSolidApi
import com.keepsolid.vpn.buddy.utils.Constans.DefaultValue.Companion.EMAIL_REGEX
import com.keepsolid.vpn.buddy.utils.Constans.DefaultValue.Companion.PASSWORD_REGEX
import com.keepsolid.vpn.buddy.utils.EventManager
import com.keepsolid.vpn.buddy.utils.NetworkProvider
import com.keepsolid.vpn.buddy.utils.SharedPrefManager
import com.keepsolid.vpn.buddy.utils.fingerprints.FingerprintIdentifyCallback
import com.keepsolid.vpn.buddy.utils.fingerprints.GoogleFingerprintProvider
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.TimeoutCancellationException
import kotlinx.coroutines.launch

@ActivityScoped
class SignInViewModel(private val context: Context,
                      private val ksApi: KeepSolidApi,
                      private val sharedPrefManager: SharedPrefManager,
                      private val eventManager: EventManager,
                      private val fingerprintManager: GoogleFingerprintProvider) : BaseViewModel() {

    val isNeedShowProgressBarLiveData = MutableLiveData<Boolean>()
    val isNeedShowNoInternetErrorLiveData = MutableLiveData<Boolean>()
    val isNeedShowVpnActivityLiveData = MutableLiveData<Boolean>()
    val showAlertTextLiveData = MutableLiveData<String>()
    val showErrorForEmailLayoutLiveData = MutableLiveData<String>()
    val showErrorForPasswordLayoutLiveData = MutableLiveData<String>()
    val showErrorForConfirmPasswordLayoutLiveData = MutableLiveData<String>()
    val changeCurrentViewStateLiveData = MutableLiveData<Int>()
    val showFingerPrintDialogLiveData = MutableLiveData<Boolean>()

    var email: ObservableField<String> = ObservableField(sharedPrefManager.getEmail())
    var pass: ObservableField<String> = ObservableField(sharedPrefManager.getPassword())
    var passConf: ObservableField<String> = ObservableField()
    var checkBoxTerms: ObservableField<Boolean> = ObservableField()
    var checkBoxEmail: ObservableField<Boolean> = ObservableField()
    internal var currentViewState = AFTER_SPLASH_VIEW_STATE

    fun subscribe() {
        if (!TextUtils.isEmpty(email.get()) && !TextUtils.isEmpty(pass.get())) {
            if (!sharedPrefManager.isRememberPasswordEnabled()) {
                pass.set("")
                changeViewState(LOGIN_LOCAL_VIEW_STATE)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M
                        && sharedPrefManager.isFingerprintProtectionEnabled()
                        && fingerprintManager.isFeatureEnabled
                        && fingerprintManager.isFingerprintAdded) {
                    showFingerPrintDialogLiveData.postValue(true)
                }
            } else {
                onNextBtnClick()
            }
        } else changeViewState(AFTER_SPLASH_VIEW_STATE)
    }

    fun onNextBtnClick() = GlobalScope.launch {
        val emailStr = email.get()
        if ((currentViewState == DEFAULT_TOP_SATE || currentViewState == DEFAULT_VIEW_SATE) && isEmailCorrect(emailStr)) {
            if (currentViewState == DEFAULT_VIEW_SATE) changeViewState(DEFAULT_TOP_SATE)
            try {
                isNeedShowProgressBarLiveData.postValue(true)
                val result = ksApi.checkIsUserExist(emailStr!!)
                val needToLogout = sharedPrefManager.isUserLoggedIn()
                        && !sharedPrefManager.isRememberPasswordEnabled()
                        && (sharedPrefManager.getEmail() != emailStr)

                if (needToLogout) {
                    ksApi.logout()
                }

                if (result.isSuccess()) changeViewState(LOGIN_VIEW_SATE)
                else changeViewState(REGISTRATION_VIEW_SATE)
            } catch (e: Exception) {
                checkException(e)
            } finally {
                isNeedShowProgressBarLiveData.postValue(false)
            }
        } else if (isEmailAndPassCorrect(currentViewState == REGISTRATION_VIEW_SATE)) {
            try {
                if (currentViewState != AFTER_SPLASH_VIEW_STATE) isNeedShowProgressBarLiveData.postValue(true)
                val email = email.get() ?: ""
                val password = pass.get() ?: ""
                val isMarketingEmails = checkBoxEmail.get() ?: false
                val isRegistration = currentViewState == REGISTRATION_VIEW_SATE
                val currentUser = ksApi.authInKsBackend(isRegistration, email, password, isMarketingEmails)

                sharedPrefManager.saveEmail(currentUser.email)
                sharedPrefManager.savePassword(currentUser.password)

                if (isRegistration) eventManager.addRegistrationEvent()

                isNeedShowVpnActivityLiveData.postValue(true)
            } catch (e: Exception) {
                checkException(e)
            } finally {
                isNeedShowProgressBarLiveData.postValue(false)
            }
        } else changeViewState(currentViewState)
    }

    fun onForgotPasswordBtnClick() {
        if (!isEmailCorrect(email.get())) return
        uiScope.launch {
            try {
                isNeedShowProgressBarLiveData.postValue(true)
                val email = email.get() ?: ""

                val isSuccess = ksApi.forgotPassword(email)
                if (isSuccess) showAlertTextLiveData.postValue(context.resources.getString(R.string.recovery_pass_dialog))
            } catch (e: Exception) {
                checkException(e)
            } finally {
                isNeedShowProgressBarLiveData.postValue(false)
            }
        }
    }

    fun onTextChanged(id: Int) {
        when (id) {
            EMAIL_ET_ID -> showErrorForEmailLayoutLiveData.postValue(null)
            PASSWORD_ET_ID -> showErrorForPasswordLayoutLiveData.postValue(null)
            CONFIRM_PASSWORD_ET_ID -> showErrorForConfirmPasswordLayoutLiveData.postValue(null)
        }
    }

    fun onFocusChange(hasFocus: Boolean) {
        if (hasFocus && currentViewState == DEFAULT_VIEW_SATE) changeViewState(DEFAULT_TOP_SATE)
        else if (hasFocus && (currentViewState == LOGIN_VIEW_SATE || currentViewState == REGISTRATION_VIEW_SATE)) changeViewState(DEFAULT_TOP_SATE)
    }

    private fun changeViewState(newViewState: Int) {
        if (currentViewState != AFTER_SPLASH_VIEW_STATE && currentViewState == newViewState) return
        currentViewState = if (newViewState == AFTER_SPLASH_VIEW_STATE) DEFAULT_VIEW_SATE else newViewState
        changeCurrentViewStateLiveData.postValue(newViewState)
    }

    private fun doFingerprintLogin(email: String, password: String) {
        GlobalScope.launch {
            try {
                isNeedShowProgressBarLiveData.postValue(true)
                val currentUser = ksApi.authInKsBackend(false, email, password, false)

                sharedPrefManager.saveEmail(currentUser.email)
                sharedPrefManager.savePassword(currentUser.password)

                isNeedShowVpnActivityLiveData.postValue(true)
            } catch (e: Exception) {
                checkException(e)
            } finally {
                isNeedShowProgressBarLiveData.postValue(false)
            }
        }

    }

    private fun checkException(e: Exception) {
        isNeedShowProgressBarLiveData.postValue(false)
        if (currentViewState == AFTER_SPLASH_VIEW_STATE) changeViewState(AFTER_SPLASH_VIEW_STATE)
        else if (!NetworkProvider.isNetworkEnabled(context)) isNeedShowNoInternetErrorLiveData.postValue(true)
        else if (e is TimeoutCancellationException) {
            if (currentViewState == AFTER_SPLASH_VIEW_STATE) changeViewState(DEFAULT_TOP_SATE)
            else showAlertTextLiveData.postValue(context.getString(R.string.error_server_not_responding))
        } else {
            if (e is KSException && e.response.responseCode == 309) {
                val msg = "${context.getString(R.string.error_many_devices_start)} " +
                        "${KSStringProviderManager.getInstance().stringProvider.supportAddress} ${context.getString(R.string.error_many_devices_end)}"
                showAlertTextLiveData.postValue(msg)
            } else e.message?.let { if (!it.contains("job", true)) showAlertTextLiveData.postValue(it) }
        }
    }

    private fun isEmailAndPassCorrect(isCheckConfirmField: Boolean): Boolean {
        val email = this.email.get()
        val password = this.pass.get()
        val confirmPassword = this.passConf.get()
        val isTermsChecked = checkBoxTerms.get() ?: false

        val isEmailCorrect = isEmailCorrect(email)
        var isPasswordGood = true

        if (TextUtils.isEmpty(password)) {
            showErrorForPasswordLayoutLiveData.postValue(context.resources.getString(R.string.error_empty_new_password))
            isPasswordGood = false
        } else showErrorForPasswordLayoutLiveData.postValue(null)

        if (isCheckConfirmField && TextUtils.isEmpty(confirmPassword)) {
            showErrorForConfirmPasswordLayoutLiveData.postValue(context.resources.getString(R.string.error_empty_confirm_password))
            isPasswordGood = false
        } else showErrorForConfirmPasswordLayoutLiveData.postValue(null)

        if (!isPasswordGood) return false
        else if (isCheckConfirmField && password != confirmPassword) {
            showErrorForPasswordLayoutLiveData.postValue(context.resources.getString(R.string.error_password_not_mutch))
            showErrorForConfirmPasswordLayoutLiveData.postValue(context.resources.getString(R.string.error_password_not_mutch))
            isPasswordGood = false
        } else if (password!!.length < PASSWORD_CHANGE_MIN_LENGTH) {
            showErrorForPasswordLayoutLiveData.postValue(context.resources.getString(R.string.error_bad_password))
            isPasswordGood = false
        } else if (password.contains(" ")) {
            showErrorForPasswordLayoutLiveData.postValue(context.resources.getString(R.string.error_space_in_password))
            isPasswordGood = false
        } else if (!password.matches(PASSWORD_REGEX.toRegex())) {
            showErrorForPasswordLayoutLiveData.postValue(context.resources.getString(R.string.error_bad_password_format))
            isPasswordGood = false
        }

        if (isCheckConfirmField && !isTermsChecked) showAlertTextLiveData.postValue(context.resources.getString(R.string.error_terms_disabled))

        return isEmailCorrect && isPasswordGood && if (isCheckConfirmField) isTermsChecked else true
    }

    private fun isEmailCorrect(email: String?): Boolean {
        return when {
            TextUtils.isEmpty(email) -> {
                showErrorForEmailLayoutLiveData.postValue(context.resources.getString(R.string.error_no_email))
                false
            }
            !email!!.matches(EMAIL_REGEX.toRegex()) -> {
                showErrorForEmailLayoutLiveData.postValue(context.resources.getString(R.string.error_wrong_email_format))
                false
            }
            else -> {
                showErrorForEmailLayoutLiveData.postValue(null)
                true
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun showFingerprintDialog(activity: FragmentActivity) {
        showFingerPrintDialogLiveData.value = false
        val fingerprintListener = object : FingerprintIdentifyCallback {
            override fun onIdentifySuccess() {
                doFingerprintLogin(sharedPrefManager.getEmail(), sharedPrefManager.getPassword())
            }

            override fun onIdentifyFail() {

            }

            override fun onUserCancel() {

            }
        }

        fingerprintManager.showIdentifyDialog(activity, fingerprintListener)


    }

    companion object {
        internal const val AFTER_SPLASH_VIEW_STATE = 0
        internal const val DEFAULT_VIEW_SATE = 1
        internal const val DEFAULT_TOP_SATE = 2
        internal const val LOGIN_VIEW_SATE = 3
        internal const val REGISTRATION_VIEW_SATE = 4
        internal const val PASSWORD_CHANGE_MIN_LENGTH = 8
        internal const val EMAIL_ET_ID = 1
        internal const val PASSWORD_ET_ID = 2
        internal const val CONFIRM_PASSWORD_ET_ID = 3
        internal const val LOGIN_LOCAL_VIEW_STATE = 9
    }

}