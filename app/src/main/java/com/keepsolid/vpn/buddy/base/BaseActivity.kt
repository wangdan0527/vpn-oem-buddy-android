package com.keepsolid.vpn.buddy.base

import android.os.Bundle
import androidx.annotation.LayoutRes
import com.adjust.sdk.Adjust
import dagger.android.support.DaggerAppCompatActivity

abstract class BaseActivity : DaggerAppCompatActivity() {

    @LayoutRes
    internal abstract fun getLayoutRes(): Int

    internal abstract fun getContainerId(): Int

    internal abstract fun getDefaultFragment(): BaseFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(getLayoutRes())
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(getContainerId(), getDefaultFragment())
                    .commitAllowingStateLoss()
        }
    }

    override fun onBackPressed() {
        val fragment = this.supportFragmentManager.findFragmentById(getContainerId())
        val isNeedClose = (fragment as? IOnBackPressed)?.onBackPressed() ?: true
        if (isNeedClose) super.onBackPressed()
    }

    override fun onResume() {
        super.onResume()
        Adjust.onResume()
    }

    override fun onPause() {
        super.onPause()
        Adjust.onPause()
    }

}