package com.keepsolid.vpn.buddy.settings

import android.content.Intent
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.CompoundButton
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.keepsolid.androidkeepsolidcommon.commonsdk.utils.stringutils.KSStringProviderManager
import com.keepsolid.vpn.buddy.BuildConfig
import com.keepsolid.vpn.buddy.R
import com.keepsolid.vpn.buddy.base.BaseFragment
import com.keepsolid.vpn.buddy.base.IOnBackPressed
import com.keepsolid.vpn.buddy.databinding.FragmentSettingsBinding
import com.keepsolid.vpn.buddy.purchase.PurchaseFragment
import com.keepsolid.vpn.buddy.settings.killswitch.KillSwitchFragment
import com.keepsolid.vpn.buddy.settings.protocol.ProtocolsFragment
import com.keepsolid.vpn.buddy.settings.trustednet.TrustedNetworksFragment
import com.keepsolid.vpn.buddy.utils.AnimationUtils
import com.keepsolid.vpn.buddy.utils.ShowMsgManager
import com.keepsolid.vpn.buddy.vpn.VpnFragment
import java.util.*
import javax.inject.Inject

class SettingsFragment @Inject constructor() : BaseFragment(), IOnBackPressed {

    private lateinit var model: SettingsViewModel
    private lateinit var binding: FragmentSettingsBinding
    private var logoutDialog: AlertDialog? = null
    private var isChangePasswordOnScreen = false

    private val toggleListener = CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
        model.toggleRememberPasswordOption(isChecked)
    }

    private val fingerprintToggleListener = CompoundButton.OnCheckedChangeListener { buttonView, isChecked ->
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            model.toggleFingerprintProtectionOption(isChecked)
        }
    }

    override fun getLayoutRes() = R.layout.fragment_settings

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = ViewModelProviders.of(this, viewModelFactory).get(SettingsViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentSettingsBinding.inflate(inflater, container, false)
        binding.model = model
        subscribe()
        model.subscribe()
        return binding.root
    }

    private fun subscribe() {
        model.isNeedShowProgressBarLiveData.observe(this, Observer<Boolean> {
            if (it != null && it) showProgressbarDialog()
            else hideProgressbarDialog()
        })
        binding.settingsClose.setOnClickListener { onBackPressed() }
        binding.settingBtnLogout.setOnClickListener { showLogOutDialog() }
        binding.settingsPurchaseClicker.setOnClickListener { showFragment(PurchaseFragment::class.java.simpleName) }
        binding.settingsChangePasswordClicker.setOnClickListener { showChangePasswordScreen() }
        binding.settingsRememberPasswordClicker.setOnClickListener { binding.swRememberToggle.toggle() }
        binding.settingsFingerprintClicker.setOnClickListener { binding.swFingerprintToggle.toggle() }
        binding.swRememberToggle.setOnCheckedChangeListener(toggleListener)
        binding.swFingerprintToggle.setOnCheckedChangeListener(fingerprintToggleListener)
        binding.settingsRateUsClicker.setOnClickListener { showRateUsScreen() }
        binding.settingsSupportClicker.setOnClickListener { showEmailScreen() }
        binding.settingsProtocolsClicker.setOnClickListener { showProtocolsScreen() }
        binding.settingsKillswitchClicker.setOnClickListener { showKillSwitchScreen() }
        binding.settingsTrustednetClicker.setOnClickListener { showTrustedNetworksFragment() }
        model.showErrorForCurrentPassLayoutLiveData.observe(this, Observer<String> {
            binding.changePasswordCurrentEtLayout.isErrorEnabled = it == null
            binding.changePasswordCurrentEtLayout.error = it
        })
        model.showErrorForNewPasswordLayoutLiveData.observe(this, Observer<String> {
            binding.changePasswordNewPassEtLayout.isErrorEnabled = it == null
            binding.changePasswordNewPassEtLayout.error = it
        })
        model.showErrorForConfirmPasswordLayoutLiveData.observe(this, Observer<String> {
            binding.changePasswordConfNewPassEtLayout.isErrorEnabled = it == null
            binding.changePasswordConfNewPassEtLayout.error = it
        })
        model.rememberPasswordEnabledLiveData.observe(this, Observer {
            binding.swRememberToggle.setOnCheckedChangeListener(null)
            binding.swRememberToggle.isChecked = it
            binding.swRememberToggle.setOnCheckedChangeListener(toggleListener)
        })
        model.currentProtoNameLiveData.observe(this, Observer<String> { binding.settingsProtocolsTvSecond.text = it })
        model.showKillSwitchItemLiveData.observe(this, Observer<Boolean> {
            if (it) {
                binding.settingsKillswitchClicker.visibility = View.VISIBLE
            } else {
                binding.settingsKillswitchClicker.visibility = View.GONE
            }
        })
        model.fingerprintAvailableLiveData.observe(this, Observer { binding.settingsFingerprintClicker.visibility = if (it) View.VISIBLE else View.GONE })
        model.fingerprintEnabledLiveData.observe(this, Observer {
            binding.swFingerprintToggle.setOnCheckedChangeListener(null)
            binding.swFingerprintToggle.isChecked = it
            binding.swFingerprintToggle.setOnCheckedChangeListener(fingerprintToggleListener)
        })
        binding.changePasswordBackBtn.setOnClickListener { showSettingsScreen() }
        binding.changePasswordConfNewPassEt.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    model.confirmBtnClick()
                    return true
                }
                return false
            }
        })
        model.showAlertTextLiveDataLiveData.observe(viewLifecycleOwner, Observer<String?> { msg ->
            if (msg == null) {
                return@Observer
            }
            hideKeyboard()
            if (msg == activity!!.getString(R.string.done)) {
                ShowMsgManager.showShortToast(activity!!, msg)
                showSettingsScreen()
            } else {
                ShowMsgManager.showAlertDialog(activity!!,
                        activity!!.getString(R.string.error),
                        msg,
                        activity!!.getString(R.string.btn_ok_title)).show()
            }
        })
        model.fingerprintNotAddedLiveData.observe(this, Observer {
            if (it) showFingerprintNotAddedDialog()
        })
    }

    private fun showFingerprintNotAddedDialog() {
        val dialog = ShowMsgManager.showAlertDialog(activity!!,
                getString(R.string.information),
                getString(R.string.S_FINGERPRINT_NOT_ADDED),
                getString(R.string.S_OK), null, null, null)
        dialog.show()
    }

    private fun showProtocolsScreen() {
        showFragment(ProtocolsFragment::class.java.simpleName)
    }

    private fun showTrustedNetworksFragment() {
        showFragment(TrustedNetworksFragment::class.java.simpleName)
    }

    private fun showKillSwitchScreen() {
        showFragment(KillSwitchFragment::class.java.simpleName)
    }

    private fun showEmailScreen() {
        val emailIntent = Intent(android.content.Intent.ACTION_SENDTO)
        emailIntent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        emailIntent.data = Uri.parse("mailto:" + KSStringProviderManager.getInstance().stringProvider.supportAddress);
        emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, activity!!.getString(R.string.email_subject))
        emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, getSupportEmailBody())
        try {
            startActivity(emailIntent)
        } catch (e: Exception) {
            ShowMsgManager.showLongToast(activity!!, activity!!.getString(R.string.no_email_app_error))
        }
    }

    private fun showChangePasswordScreen() {
        isChangePasswordOnScreen = true
        AnimationUtils.hideSlideUpWithAlpha(binding.settingsLayout)
        AnimationUtils.showSlidedUpWithAlpha(binding.changePasswordLayout)
    }

    private fun showSettingsScreen() {
        isChangePasswordOnScreen = false
        model.clearPasswordFields()
        AnimationUtils.hideSlideDownWithAlpha(binding.changePasswordLayout)
        AnimationUtils.showSlideDownWithAlpha(binding.settingsLayout)
    }

    private fun showRateUsScreen() {
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse("https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID)
        startActivity(i)
    }

    private fun showLogOutDialog() {
        if (logoutDialog != null) return
        logoutDialog = ShowMsgManager.showAlertDialog(activity!!,
                activity!!.getString(R.string.information),
                activity!!.getString(R.string.log_out_confirm),
                activity!!.getString(R.string.log_out),
                Runnable { model.doOnLogOut() },
                activity!!.getString(R.string.cancel))
        logoutDialog!!.setOnDismissListener { logoutDialog = null }
        logoutDialog!!.show()
    }

    private fun getSupportEmailBody(): String {
        return "${activity!!.getString(R.string.feedback_email_body_title)} ${Build.MODEL}\n" +
                "Android version: ${Build.VERSION.RELEASE}\nApp version: ${BuildConfig.VERSION_NAME}\n" +
                "Account: ${model.email.get()}\nDevice language ISO: ${Locale.getDefault().isO3Language}"
    }

    override fun onBackPressed(): Boolean {
        if (isChangePasswordOnScreen) showSettingsScreen()
        else showFragment(VpnFragment::class.java.simpleName)
        return false
    }
}