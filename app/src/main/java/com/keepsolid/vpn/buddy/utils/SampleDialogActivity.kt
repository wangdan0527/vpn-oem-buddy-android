package com.keepsolid.vpn.buddy.utils

import android.os.Bundle

import com.keepsolid.androidkeepsolidcommon.vpnunlimitedsdk.api.dialogs.KSDefaultDialogActivity

class SampleDialogActivity : KSDefaultDialogActivity() {

    override fun onCreate(bundle: Bundle?) {
        super.onCreate(bundle)
    }

    override fun onPause() {
        super.onPause()
    }
}