package com.keepsolid.vpn.buddy.di

import com.keepsolid.vpn.buddy.auth.SignInActivity
import com.keepsolid.vpn.buddy.auth.SignInActivityModule
import com.keepsolid.vpn.buddy.vpn.VpnActivity
import com.keepsolid.vpn.buddy.vpn.VpnActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {

    @ActivityScoped
    @ContributesAndroidInjector(modules = [VpnActivityModule::class])
    internal abstract fun bindVpnActivityModule(): VpnActivity

    @ActivityScoped
    @ContributesAndroidInjector(modules = [SignInActivityModule::class])
    internal abstract fun bindSignInActivityModule(): SignInActivity

}
