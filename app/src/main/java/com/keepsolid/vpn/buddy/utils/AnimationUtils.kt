package com.keepsolid.vpn.buddy.utils

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.AnimatorSet
import android.animation.ValueAnimator
import android.view.View

class AnimationUtils {

    companion object {
        internal const val DURATION_LIGHT = 1500
        internal const val DEFAULT_SLIDE_ANIMATION_DURATION = 300
        internal const val DEFAULT_ROTATE_ANIMATION_DURATION = 150
        internal const val DEFAULT_FADE_ANIMATION_DURATION = 250
        internal const val DEFAULT_FAST_ANIMATION = 100
        internal const val DEFAULT_ULTRA_FAST_ANIMATION = 50

        internal fun slideUp(view: View) {
            view.visibility = View.VISIBLE

            view.post {
                val animatorSlide = ValueAnimator.ofFloat(view.height.toFloat(), 0.toFloat())
                animatorSlide.addUpdateListener { animation ->
                    val value = animation.animatedValue as Float
                    view.translationY = value
                }

                val animatorSet = AnimatorSet()
                animatorSet.play(animatorSlide)
                animatorSet.duration = DEFAULT_SLIDE_ANIMATION_DURATION.toLong()
                animatorSet.start()
            }
        }

        internal fun hideSlideUpWithAlpha(view: View) {
            hideSlideUpWithAlpha(view, DEFAULT_SLIDE_ANIMATION_DURATION)
        }

        internal fun hideSlideUpWithAlpha(view: View, duration: Int) {
            view.animate().cancel()
            val height = view.measuredHeight.toFloat()
            //create slideUpAnimation
            val animatorSlideUp = ValueAnimator.ofFloat(0.toFloat(), -height)
            animatorSlideUp.addUpdateListener { animation ->
                val value = animation.animatedValue as Float
                view.translationY = value
            }
            //create alphaAnimation
            val alpHaAnimator = ValueAnimator.ofFloat(1.0f, 0.toFloat())
            alpHaAnimator.addUpdateListener { animation ->
                val value = animation.animatedValue as Float
                view.alpha = value
            }
            //start animation
            val animatorSet = AnimatorSet()
            animatorSet.play(animatorSlideUp).with(alpHaAnimator)
            animatorSet.duration = duration.toLong()
            animatorSet.start()
        }

        internal fun hideSlideDownWithAlpha(view: View) {
            hideSlideDownWithAlpha(view, DEFAULT_SLIDE_ANIMATION_DURATION)
        }

        internal fun hideSlideDownWithAlpha(view: View, duration: Int) {
            view.animate().cancel()
            val height = view.measuredHeight.toFloat()
            //create slideUpAnimation
            val animatorSlideUp = ValueAnimator.ofFloat(0f, height)
            animatorSlideUp.addUpdateListener { animation ->
                val value = animation.animatedValue as Float
                view.translationY = value
            }
            //create alphaAnimation
            val alpHaAnimator = ValueAnimator.ofFloat(1.0f, 0f)
            alpHaAnimator.addUpdateListener { animation ->
                val value = animation.animatedValue as Float
                view.alpha = value
            }
            //start animation
            val animatorSet = AnimatorSet()
            animatorSet.play(animatorSlideUp).with(alpHaAnimator)
            animatorSet.duration = duration.toLong()
            animatorSet.start()
        }

        internal fun showSlideDownWithAlpha(view: View) {
            showSlideDownWithAlpha(view, DEFAULT_SLIDE_ANIMATION_DURATION)
        }

        internal fun showSlideDownWithAlpha(view: View, duration: Int) {
            view.animate().cancel()
            view.visibility = View.VISIBLE
            view.alpha = 0f
            val height = view.measuredHeight.toFloat()
            //create slideUpAnimation
            val animatorSlideDown = ValueAnimator.ofFloat(-height, 0f)
            animatorSlideDown.addUpdateListener { animation ->
                val value = animation.animatedValue as Float
                view.translationY = value
            }
            //create alphaAnimation
            val alpHaAnimator = ValueAnimator.ofFloat(0f, 1.0f)
            alpHaAnimator.addUpdateListener { animation ->
                val value = animation.animatedValue as Float
                view.alpha = value
            }
            //start animation
            val animatorSet = AnimatorSet()
            animatorSet.play(animatorSlideDown).with(alpHaAnimator)
            animatorSet.duration = duration.toLong()
            animatorSet.start()
        }

        internal fun showSlidedUpWithAlpha(view: View) {
            showSlidedUpWithAlpha(view, DEFAULT_SLIDE_ANIMATION_DURATION)
        }

        internal fun showSlidedUpWithAlpha(view: View, duration: Int) {
            view.animate().cancel()
            view.visibility = View.VISIBLE
            view.alpha = 0f
            val height = view.measuredHeight.toFloat()
            //create slideUpAnimation
            val animatorSlideDown = ValueAnimator.ofFloat(height, 0f)
            animatorSlideDown.addUpdateListener { animation ->
                val value = animation.animatedValue as Float
                view.translationY = value
            }
            //create alphaAnimation
            val alpHaAnimator = ValueAnimator.ofFloat(0f, 1.0f)
            alpHaAnimator.addUpdateListener { animation ->
                val value = animation.animatedValue as Float
                view.alpha = value
            }
            //start animation
            val animatorSet = AnimatorSet()
            animatorSet.play(animatorSlideDown).with(alpHaAnimator)
            animatorSet.duration = duration.toLong()
            animatorSet.start()
        }

        fun startLightAnimation(view: View) {
            view.animate().cancel()
            //create alphaAnimation
            val alphaDownAnimator = ValueAnimator.ofFloat(1.0.toFloat(), 0.5.toFloat(), 1.0.toFloat())
            alphaDownAnimator.addUpdateListener { animation ->
                val value = animation.animatedValue as Float
                view.alpha = value
            }
            val scaleAnimator = ValueAnimator.ofFloat(1.0.toFloat(), 0.9.toFloat(), 1.0.toFloat())
            scaleAnimator.addUpdateListener { animation ->
                val value = animation.animatedValue as Float
                view.scaleX = value
                view.scaleY = value
            }
            alphaDownAnimator.repeatCount = 20
            scaleAnimator.repeatCount = 20
            //start animation
            val animatorSet = AnimatorSet()
            animatorSet.duration = DURATION_LIGHT.toLong()
            animatorSet.play(alphaDownAnimator).with(scaleAnimator)
            animatorSet.start()
        }

        fun showSlideUp(view: View) {
            showSlideUp(view, DEFAULT_SLIDE_ANIMATION_DURATION)
        }

        fun showSlideUp(view: View, animDuration: Int) {
            view.animate().cancel()
            view.visibility = View.VISIBLE
            if (view.height > 0) {
                showSlideUpNow(view, animDuration)
            } else {
                // wait till height is measured
                view.post { showSlideUpNow(view, animDuration) }
            }

        }

        private fun showSlideUpNow(view: View, animDuration: Int) {
            view.translationY = view.height.toFloat()
            view.animate()
                    .translationY(0f)
                    .setDuration(animDuration.toLong())
                    .setListener(object : AnimatorListenerAdapter() {
                        override fun onAnimationCancel(animation: Animator) {
                            onAnimationEnd(animation)
                        }

                        override fun onAnimationEnd(animation: Animator) {
                            view.translationY = 0f
                        }
                    })
        }
    }

}