package com.keepsolid.vpn.buddy.di;

import com.keepsolid.vpn.buddy.utils.ServiceNotificationClickBroadcastReceiver;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class BroadcastReceiverBindingModule {

    @ContributesAndroidInjector
    abstract ServiceNotificationClickBroadcastReceiver provideServiceNotificationClickBroadcastReceiver();

}
