package com.keepsolid.vpn.buddy.utils.fingerprints;

public interface FingerprintIdentifyCallback {

    void onIdentifySuccess();

    void onIdentifyFail();

    void onUserCancel();

}
