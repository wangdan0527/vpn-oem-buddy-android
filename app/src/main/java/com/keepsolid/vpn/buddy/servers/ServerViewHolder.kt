package com.keepsolid.vpn.buddy.servers

import android.view.View
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import com.bumptech.glide.Glide
import com.keepsolid.vpn.buddy.R
import com.keepsolid.vpn.buddy.utils.recycler.AbstractRecyclerItem
import com.keepsolid.vpn.buddy.utils.recycler.AbstractViewHolder
import com.keepsolid.vpn.buddy.utils.recycler.ServerRecyclerItem

class ServerViewHolder(itemView: View) : AbstractViewHolder(itemView) {

    val name: TextView = itemView.findViewById(R.id.item_server_name)
    private val region: TextView = itemView.findViewById(R.id.item_server_region)
    private val flag: ImageView = itemView.findViewById(R.id.item_server_flag)
    private val favorite: CheckBox = itemView.findViewById(R.id.item_server_favorite)
    private val ivConnected: ImageView = itemView.findViewById(R.id.item_server_connected)

    override fun buildView(metadata: AbstractRecyclerItem) {
        val recyclerItem = metadata as ServerRecyclerItem
        val server = recyclerItem.server

        name.text = recyclerItem.server.name
        region.text = recyclerItem.server.description

        if (server.isOptimal) {
            Glide.with(itemView)
                    .load(R.drawable.ic_optimal)
                    .centerCrop()
                    .fitCenter()
                    .into(flag)
            favorite.visibility = View.GONE
        } else {
            Glide.with(itemView)
                    .load(server.iconUrl)
                    .centerCrop()
                    .fitCenter()
                    .into(flag)
            favorite.visibility = View.VISIBLE
        }

        favorite.setOnCheckedChangeListener(null)
        favorite.isChecked = recyclerItem.isFavorite
        favorite.setOnCheckedChangeListener(recyclerItem.onCheckedChangeListener)

        if (recyclerItem.isConnected) {
            itemView.setBackgroundResource(R.drawable.server_connected_bg)
            ivConnected.visibility = View.VISIBLE
        } else {
            itemView.background = null
            ivConnected.visibility = View.GONE
        }

        itemView.setOnClickListener(recyclerItem.onClickListener)
    }

}
