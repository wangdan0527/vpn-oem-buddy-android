package com.keepsolid.vpn.buddy.network

import com.keepsolid.androidkeepsolidcommon.commonsdk.api.exceptions.KSException
import com.keepsolid.androidkeepsolidcommon.commonsdk.api.transport.KSResponse
import com.keepsolid.androidkeepsolidcommon.commonsdk.entities.KSAccountStatus
import com.keepsolid.androidkeepsolidcommon.commonsdk.entities.KSAccountUserInfo
import com.keepsolid.androidkeepsolidcommon.vpnunlimitedsdk.api.VPNUFacade
import com.keepsolid.androidkeepsolidcommon.vpnunlimitedsdk.api.managers.vpn.VPNUNetworkPrefsManager
import com.keepsolid.androidkeepsolidcommon.vpnunlimitedsdk.entities.VPNUServer
import com.keepsolid.androidkeepsolidcommon.vpnunlimitedsdk.entities.VpnStatus
import com.keepsolid.androidkeepsolidcommon.vpnunlimitedsdk.vpn.VpnStatusChangedListener
import com.keepsolid.vpn.buddy.utils.Result
import com.keepsolid.vpn.buddy.utils.SharedPrefManager
import com.keepsolid.vpn.buddy.utils.models.CurrentUser
import com.keepsolid.vpn.buddy.utils.models.PurchaseInfo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.withContext
import kotlinx.coroutines.withTimeout
import org.json.JSONObject
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class KeepSolidApi @Inject constructor(var sharedPrefManager: SharedPrefManager) {

    companion object {
        private const val IS_USER_EXIST_ACTION_NAME = "04161eb489"
        private const val USER_EMAIL_PARAM_NAME = "email"
        private const val REQUEST_TIMEOUT = 15_000.toLong()
    }

    var currentUser: CurrentUser? = null
    var serversList = mutableListOf<VPNUServer>()
    var listener: VpnStatusChangedListener? = null
    var isConnecting = false
    var connectionStateHolder = false
    var lastSelectedServerFromServerList: VPNUServer? = null
    var networkPrefsManager: VPNUNetworkPrefsManager = VPNUFacade.getInstance().networkPrefsManager

    internal suspend fun startVPN() = withContext(Dispatchers.IO) {
        val vpnuConfigurator = VPNUFacade.getInstance().vpnuConfigurator

        val server = when {
            currentUser?.lastConfiguredServer != null -> currentUser?.lastConfiguredServer!!
            vpnuConfigurator.lastConfiguredServer != null -> vpnuConfigurator.lastConfiguredServer
            else -> serversList[0]
        }

        currentUser?.lastConfiguredServer = server

        val forceProfileReload = sharedPrefManager.isProfileReloadNeeded()
        vpnuConfigurator.prepare()
        vpnuConfigurator.setup(server, sharedPrefManager.getVPNUProtoConfigToConnect(), !forceProfileReload)
        sharedPrefManager.setProfileReloadNeeded(false)
        vpnuConfigurator.startVpn()
    }

    internal suspend fun setupVpnWithputStart() = withContext(Dispatchers.IO) {
        val vpnuConfigurator = VPNUFacade.getInstance().vpnuConfigurator

        val server = when {
            currentUser?.lastConfiguredServer != null -> currentUser?.lastConfiguredServer!!
            vpnuConfigurator.lastConfiguredServer != null -> vpnuConfigurator.lastConfiguredServer
            else -> serversList[0]
        }

        currentUser?.lastConfiguredServer = server

        val forceProfileReload = sharedPrefManager.isProfileReloadNeeded()
        vpnuConfigurator.prepare()
        vpnuConfigurator.setup(server, sharedPrefManager.getVPNUProtoConfigToConnect(), !forceProfileReload)
        sharedPrefManager.setProfileReloadNeeded(false)
    }


    internal suspend fun stopVpn() = withContext(Dispatchers.IO) {
        val vpnuConfigurator = VPNUFacade.getInstance().vpnuConfigurator
        vpnuConfigurator.stopVpn()
    }

    internal fun setVpnStatusListener(listener: VpnStatusChangedListener) {
        val vpnuConfigurator = VPNUFacade.getInstance().vpnuConfigurator
        this.listener = listener
        vpnuConfigurator.addOnStatusChangedListener(listener)
    }

    internal fun removeVpnStatusListener() {
        val vpnuConfigurator = VPNUFacade.getInstance().vpnuConfigurator
        vpnuConfigurator.removeOnStatusChangedListener(listener)
        listener = null
    }

    internal suspend fun getVpnStatus(): VpnStatus {
        return withContext(Dispatchers.IO) {
            val vpnuConfigurator = VPNUFacade.getInstance().vpnuConfigurator
            return@withContext vpnuConfigurator.vpnStatus
        }
    }

    internal suspend fun updateAccountStatus() = withContext(Dispatchers.IO) {
        delay(5000)
        manageAndSaveAccountStatus(VPNUFacade.getInstance().accountManager.status)

    }

    internal suspend fun checkIsUserExist(email: String): Result<KSResponse> {
        return withContext(Dispatchers.IO) {
            val requestBuilder = VPNUFacade.getInstance().requestBuilder
            val request = requestBuilder.buildAuthRequest(IS_USER_EXIST_ACTION_NAME)
            request.isUseSession = false
            request.putParameterObject(USER_EMAIL_PARAM_NAME, email)

            val transport = VPNUFacade.getInstance().requestTransport

            try {
                val response = transport.sendRequest(request)
                return@withContext Result(success = response!!)
            } catch (e: KSException) {
                if (e.response.responseCode == 402) {
                    return@withContext Result(success = e.response, exception = e)
                } else {
                    throw KSException(null)
                }
            }
        }
    }

    internal suspend fun authInKsBackend(isRegistration: Boolean, email: String, password: String, isMarketingEmails: Boolean): CurrentUser {
        return withTimeout(REQUEST_TIMEOUT) {
            withContext(Dispatchers.IO) {
                val userInfo: KSAccountUserInfo = if (isRegistration) {
                    VPNUFacade.getInstance()
                            .authorizer
                            .registerWithLogin(email, password, isMarketingEmails)
                } else {
                    VPNUFacade.getInstance()
                            .authorizer
                            .authorizeWithLogin(email, password)
                }
                return@withContext initUserData(userInfo, password)
            }
        }
    }

    internal suspend fun forgotPassword(email: String): Boolean {
        return withContext(Dispatchers.IO) { VPNUFacade.getInstance().authorizer.recoverPassword(email) }
    }

    internal suspend fun changePass(oldPass: String, newPass: String): Boolean {
        return withContext(Dispatchers.IO) {
            val requestTransport = VPNUFacade.getInstance().requestTransport
            val params = HashMap<String, String>()
            params["password"] = oldPass
            params["newpassword"] = newPass
            val changePassRequest = VPNUFacade.getInstance().requestBuilder.buildAuthRequest("changeaccountpassword", params)
            val response = requestTransport.sendRequest(changePassRequest)
            return@withContext response.isResultSuccessful
        }
    }

    internal suspend fun getPurchases(): MutableList<PurchaseInfo> {
        return withTimeout(REQUEST_TIMEOUT) {
            withContext(Dispatchers.IO) {
                val loadedPurchases = ArrayList<PurchaseInfo>()

                val requestTransport = VPNUFacade.getInstance().requestTransport
                val request = VPNUFacade.getInstance().requestBuilder.buildKeepSolidAPIRequest("purchases_by_services")
                request.putParameterObject("purchase_platform", "Android")
                request.putParameterObject("product", "vpn")

                val response = requestTransport.sendRequest(request)
                val responseJson = JSONObject(response.responseMessage)

                val purchasesJson = responseJson.getJSONArray("purchases")

                for (i in 0 until purchasesJson.length()) {
                    loadedPurchases.add(PurchaseInfo(purchasesJson.optJSONObject(i)))
                }
                return@withContext loadedPurchases
            }
        }

    }

    private fun initUserData(userInfo: KSAccountUserInfo, password: String): CurrentUser {

        VPNUFacade.getInstance().vpnuConfigurator.prepare()

        val vpnuServersManager = VPNUFacade.getInstance().serversManager
        val vpnuAccountManager = VPNUFacade.getInstance().accountManager

        serversList.clear()
        vpnuServersManager.servers.forEach { if (!it.isVps) serversList.add(it) }

        val accountStatus = vpnuAccountManager.status

        currentUser = CurrentUser(userInfo.userName, password, accountStatus, userInfo, VPNUFacade.getInstance().vpnuConfigurator.lastConfiguredServer)
        manageAndSaveAccountStatus(accountStatus)

        return currentUser!!
    }

    private fun manageAndSaveAccountStatus(accountStatus: KSAccountStatus?) {

        if (accountStatus == null) {
            return
        }

        currentUser!!.accountStatus = accountStatus

        val isProtoAuto = sharedPrefManager.isVpnProtoAuto()
        val currentPreferredProtoConfig = sharedPrefManager.getPreferredProtocol()

        sharedPrefManager.saveProtocolList(accountStatus.availableProtocol)
        if (isProtoAuto && currentPreferredProtoConfig != sharedPrefManager.getPreferredProtocol()) {
            sharedPrefManager.setProfileReloadNeeded(true)
        }

    }

    internal fun setConnectionFlagEnabled() {
        isConnecting = true
    }

    internal fun setConnectionFlagDisabled() {
        isConnecting = false
    }

    internal fun setConnectionStateHolder(isConnecting: Boolean) {
        connectionStateHolder = isConnecting
    }

    internal suspend fun logout() {
        stopVpn()
        VPNUFacade.getInstance().vpnuConfigurator.onLogout()
        currentUser = null
        isConnecting = false
        sharedPrefManager.clearAccountSensitivePrefs()
        removeVpnStatusListener()
        serversList.clear()
    }

}