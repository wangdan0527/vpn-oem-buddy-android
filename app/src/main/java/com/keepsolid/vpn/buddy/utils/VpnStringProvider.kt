package com.keepsolid.vpn.buddy.utils

import com.keepsolid.androidkeepsolidcommon.commonsdk.utils.stringutils.KSStringProvider

class VpnStringProvider : KSStringProvider() {

    override fun getPolicyLink(): String = "http://vpnbuddy.net/privacy-policy/"

    override fun getFAQLink(): String? = null

    override fun getAppShareLink(): String? = null

    override fun getAgeCertificationLink(): String = "http://vpnbuddy.net/privacy-policy/"

    override fun getAppWebsiteLink(): String? = null

    override fun getProductName(): String? = "VPN BUDDY"

    override fun getFeedbackAddress(): String? = null

    override fun getDataUsageLink(): String = "http://vpnbuddy.net/data-usage/"

    override fun getTellFriendLinks(): String? = null

    override fun getSupportAddress(): String = "support@vpnbuddy.net"

    override fun getPrivacyLink(): String = "http://vpnbuddy.net/privacy-policy/"

    override fun getDownloadsLink(): String? = null

    override fun getCompanyName(): String = "VPN BUDDY"

}