package com.keepsolid.vpn.buddy.settings.trustednet

import android.view.View
import android.widget.RelativeLayout
import android.widget.TextView
import com.keepsolid.vpn.buddy.R
import com.keepsolid.vpn.buddy.utils.recycler.AbstractRecyclerItem
import com.keepsolid.vpn.buddy.utils.recycler.AbstractViewHolder
import com.keepsolid.vpn.buddy.utils.recycler.TrustedNetworkRecyclerItem

class TrustedNetworksViewHolder(itemView: View) : AbstractViewHolder(itemView) {

    val name: TextView = itemView.findViewById(R.id.standard_recycler_item_title)
    private val btnRemove: RelativeLayout = itemView.findViewById(R.id.rv_btn_block)
    private val holder: View = itemView.findViewById(R.id.standard_recycler_item_cv)

    override fun buildView(metadata: AbstractRecyclerItem) {
        val recyclerItem = metadata as TrustedNetworkRecyclerItem

        name.text = recyclerItem.networkName

        holder.setOnClickListener(null)
        btnRemove.setOnClickListener(recyclerItem.onClickListener)

    }

}
