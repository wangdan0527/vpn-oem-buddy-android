package com.keepsolid.vpn.buddy.purchase

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.billingclient.api.BillingClient
import com.android.billingclient.api.BillingFlowParams
import com.android.billingclient.api.Purchase
import com.android.billingclient.api.SkuDetailsParams
import com.keepsolid.vpn.buddy.R
import com.keepsolid.vpn.buddy.base.BaseFragment
import com.keepsolid.vpn.buddy.base.IOnBackPressed
import com.keepsolid.vpn.buddy.databinding.FragmentPurchaseBinding
import com.keepsolid.vpn.buddy.rest.ReceiptValidator
import com.keepsolid.vpn.buddy.settings.SettingsFragment
import com.keepsolid.vpn.buddy.utils.ShowMsgManager
import com.keepsolid.vpn.buddy.utils.recycler.AbstractRecyclerItem
import com.keepsolid.vpn.buddy.utils.recycler.BaseRecyclerAdapter
import com.keepsolid.vpn.buddy.utils.recycler.PurchaseRecyclerItem
import com.revenuecat.purchases.PurchaserInfo
import com.revenuecat.purchases.Purchases
import com.revenuecat.purchases.PurchasesError
import com.revenuecat.purchases.interfaces.MakePurchaseListener
import javax.inject.Inject

class PurchaseFragment @Inject constructor() : BaseFragment(), IOnBackPressed, MakePurchaseListener {
    override fun onError(error: PurchasesError, userCancelled: Boolean?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onCompleted(purchase: Purchase, purchaserInfo: PurchaserInfo) {
        model.onPurchaseCompleted(purchase)
    }

    private lateinit var model: PurchaseViewModel
    private lateinit var binding: FragmentPurchaseBinding

    private val adapter = BaseRecyclerAdapter()
    private lateinit var billingClient: BillingClient

    override fun getLayoutRes() = R.layout.fragment_purchase

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = ViewModelProviders.of(this, viewModelFactory).get(PurchaseViewModel::class.java)
        billingClient = BillingClient
                .newBuilder(activity!!.applicationContext)
                .enablePendingPurchases()
                .setListener(model)
                .build()

        model.billingClient = billingClient
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentPurchaseBinding.inflate(inflater, container, false)
        binding.model = model
        subscribe()
        billingClient.startConnection(model)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        billingClient.endConnection()
    }

    private fun subscribe() {
        binding.purchaseRecycler.layoutManager = LinearLayoutManager(activity)
        binding.purchaseRecycler.adapter = adapter

        binding.btnClose.setOnClickListener { onBackPressed() }
        model.isNeedShowProgressBarLiveData.observe(this, Observer<Boolean> {
            if (it != null && it) showProgressbarDialog()
            else hideProgressbarDialog()
        })
        model.updateSubscriptionsListLiveData.observe(this, Observer<MutableList<AbstractRecyclerItem>> { adapter.addButch(it) })
        model.showPurchaseDialogLiveData.observe(this, Observer<PurchaseRecyclerItem> { if (it != null) showPurchaseDialog(it) })
        model.showAlertTextLiveData.observe(this, Observer<String> {
            if (TextUtils.isEmpty(it)) return@Observer
            ShowMsgManager.showAlertDialog(
                    context = activity!!,
                    title = activity!!.getString(R.string.information),
                    msg = it,
                    positiveBtnTitle = activity!!.getString(R.string.btn_ok_title),
                    positiveBtnCallback = Runnable { onBackPressed() })
                    .show()
            model.showAlertTextLiveData.value = ""
        })
        model.isNeedClosePurchaseLiveData.observe(this, Observer<Boolean> { if (it != null && it) closePurchases() })
        model.getSkuListLiveData.observe(this, Observer<MutableList<String>> { if (it != null && it.isNotEmpty()) getSkuDetails(it) })
    }

    private fun getSkuDetails(skuList: MutableList<String>) {
        val params = SkuDetailsParams.newBuilder()
                .setSkusList(skuList)
                .setType(BillingClient.SkuType.SUBS)
                .build()
        billingClient.querySkuDetailsAsync(params, model)
    }

    private fun showPurchaseDialog(item: PurchaseRecyclerItem) {
//        val flowParams = BillingFlowParams.newBuilder()
//                .setSkuDetails(item.purchaseInfo.details)
//                .build()
//        billingClient.launchBillingFlow(activity, flowParams)

        activity?.let { Purchases.sharedInstance.makePurchase(it, item.purchaseInfo.details, this) }
    }

    private fun closePurchases() {
        ShowMsgManager.showLongToast(activity!!, getString(R.string.error_no_data))
        hideProgressbarDialog()
        onBackPressed()
    }

    override fun onBackPressed(): Boolean {
        showFragment(SettingsFragment::class.java.simpleName)
        return false
    }
}