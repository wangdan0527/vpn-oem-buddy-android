package com.keepsolid.vpn.buddy.utils.recycler

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

class BaseRecyclerAdapter(private val viewHolders: MutableList<AbstractRecyclerItem> = mutableListOf()) : RecyclerView.Adapter<AbstractViewHolder>() {

    private val viewHolderTypeProvider: ViewHolderTypeProvider = ViewHolderTypeProvider()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AbstractViewHolder {
        return viewHolderTypeProvider.createViewHolder(parent, viewType)
    }

    override fun onBindViewHolder(holder: AbstractViewHolder, position: Int) {
        holder.buildView(viewHolders[position])
    }

    override fun getItemCount(): Int {
        return viewHolders.size
    }

    override fun getItemViewType(position: Int): Int {
        return viewHolders[position].viewHolderType
    }

    fun addButch(itemList: MutableList<AbstractRecyclerItem>, forceRefresh: Boolean) {
        if (viewHolders == itemList && !forceRefresh) return
        viewHolders.clear()
        viewHolders.addAll(itemList)
        notifyDataSetChanged()
    }

    fun addButch(itemList: MutableList<AbstractRecyclerItem>) {
        addButch(itemList, false)
    }

}