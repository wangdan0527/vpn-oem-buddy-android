package com.keepsolid.vpn.buddy.servers

import android.content.Context
import android.view.View
import android.widget.CompoundButton
import androidx.lifecycle.MutableLiveData
import com.keepsolid.androidkeepsolidcommon.vpnunlimitedsdk.entities.VPNUServer
import com.keepsolid.vpn.buddy.R
import com.keepsolid.vpn.buddy.base.BaseViewModel
import com.keepsolid.vpn.buddy.network.KeepSolidApi
import com.keepsolid.vpn.buddy.utils.SharedPrefManager
import com.keepsolid.vpn.buddy.utils.recycler.AbstractRecyclerItem
import com.keepsolid.vpn.buddy.utils.recycler.ServerRecyclerItem
import com.keepsolid.vpn.buddy.utils.recycler.TitleRecyclerItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ServersViewModel(private val context: Context,
                       private val ksApi: KeepSolidApi,
                       private val sharedPrefManager: SharedPrefManager) : BaseViewModel() {

    val showVpnFragmentLiveData = MutableLiveData<Boolean>()
    val isNeedShowProgressBarLiveData = MutableLiveData<Boolean>()
    val updateServersListLiveData = MutableLiveData<MutableList<AbstractRecyclerItem>>()

    private val favSet: HashSet<String> = sharedPrefManager.getFavorite()
    private var itemList = mutableListOf<AbstractRecyclerItem>()

    private val titleSearch = TitleRecyclerItem(context.getString(R.string.search_results), AbstractRecyclerItem.PRIORITY_SEARCH_TITLE)
    private val titleFav = TitleRecyclerItem(context.getString(R.string.favorite_servers), AbstractRecyclerItem.PRIORITY_FAVORITE_TITLE)
    private val titleAll = TitleRecyclerItem(context.getString(R.string.generals_title), AbstractRecyclerItem.PRIORITY_ALL_TITLE)


    fun subscribe(needShowProgress: Boolean) {
        isNeedShowProgressBarLiveData.value = true && needShowProgress
        uiScope.launch {
            withContext(Dispatchers.IO) {
                itemList.clear()
                itemList.add(titleAll)
                ksApi.serversList.forEach { server ->
                    val isConnected = ksApi.isConnecting && ksApi.currentUser!!.lastConfiguredServer?.uniqueStringId == server.uniqueStringId ?: false
                    if (server.isOptimal) {
                        server.description = context.getString(R.string.optimal_server_desc)
                    }
                    val recyclerItem = ServerRecyclerItem(server, isConnected)
                    recyclerItem.isFavorite = favSet.contains(server.uniqueStringId)

                    recyclerItem.onCheckedChangeListener = CompoundButton.OnCheckedChangeListener { _, isChecked ->
                        recyclerItem.isFavorite = isChecked

                        if (isChecked) favSet.add(recyclerItem.server.uniqueStringId)
                        else favSet.remove(recyclerItem.server.uniqueStringId)

                        rearrangeServers()

                        sharedPrefManager.saveFavorite(favSet)
                    }
                    recyclerItem.onClickListener = View.OnClickListener {
                        setNewSelectedServerAndShowVpnScreen(server)
                    }
                    itemList.add(recyclerItem)
                }

                rearrangeServers()
            }
            isNeedShowProgressBarLiveData.value = false
        }
    }

    private fun rearrangeServers() {
        if (!containFavorites()) itemList.remove(titleFav)
        else if (!itemList.contains(titleFav)) itemList.add(titleFav)

        itemList = ArrayList(itemList.sortedWith(compareBy({ it.sortPriority }, { it.sortName })))

        updateServersListLiveData.postValue(itemList)
    }

    private fun containFavorites(): Boolean {
        itemList.forEach {
            if (it.sortPriority == AbstractRecyclerItem.PRIORITY_FAVORITE_OPTIMAL
                    || it.sortPriority == AbstractRecyclerItem.PRIORITY_FAVORITE_STREAMING
                    || it.sortPriority == AbstractRecyclerItem.PRIORITY_FAVORITE_USUAL) {
                return true
            }
        }
        return false
    }

    fun searchItems(search: String) = uiScope.launch {
        withContext(Dispatchers.IO) {
            var recyclerItems: ArrayList<AbstractRecyclerItem> = ArrayList()
            recyclerItems.add(titleSearch)
            ksApi.serversList.forEach { server ->
                if (server.name.toLowerCase().contains(search.toLowerCase()) || server.description.toLowerCase().contains(search.toLowerCase())) {
                    val recyclerItem = ServerRecyclerItem(server, false)
                    recyclerItem.isFavorite = favSet.contains(server.uniqueStringId)

                    recyclerItem.onCheckedChangeListener = CompoundButton.OnCheckedChangeListener { _, isChecked ->
                        recyclerItem.isFavorite = isChecked
                        if (isChecked) {
                            favSet.add(recyclerItem.server.uniqueStringId)
                        } else {
                            favSet.remove(recyclerItem.server.uniqueStringId)
                        }
                        sharedPrefManager.saveFavorite(favSet)
                    }
                    recyclerItem.onClickListener = View.OnClickListener {
                        setNewSelectedServerAndShowVpnScreen(server)
                    }
                    recyclerItems.add(recyclerItem)
                }
            }
            recyclerItems = ArrayList(recyclerItems.sortedWith(compareBy({ it.sortPriority }, { it.sortName })))
            updateServersListLiveData.postValue(recyclerItems)
        }
    }

    private fun setNewSelectedServerAndShowVpnScreen(server: VPNUServer) {
        ksApi.lastSelectedServerFromServerList = server
        showVpnFragmentLiveData.value = true
    }
}