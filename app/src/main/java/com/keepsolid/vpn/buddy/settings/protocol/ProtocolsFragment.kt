package com.keepsolid.vpn.buddy.settings.protocol

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.keepsolid.vpn.buddy.R
import com.keepsolid.vpn.buddy.base.BaseFragment
import com.keepsolid.vpn.buddy.base.IOnBackPressed
import com.keepsolid.vpn.buddy.databinding.FragmentProtocolsBinding
import com.keepsolid.vpn.buddy.settings.SettingsFragment
import com.keepsolid.vpn.buddy.utils.recycler.AbstractRecyclerItem
import com.keepsolid.vpn.buddy.utils.recycler.BaseRecyclerAdapter
import com.keepsolid.vpn.buddy.vpn.VpnFragment
import javax.inject.Inject

class ProtocolsFragment @Inject constructor() : BaseFragment(), IOnBackPressed {

    override fun getLayoutRes() = R.layout.fragment_protocols

    private lateinit var model: ProtocolsViewModel
    private lateinit var binding: FragmentProtocolsBinding

    private val adapter = BaseRecyclerAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = ViewModelProviders.of(this, viewModelFactory).get(ProtocolsViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentProtocolsBinding.inflate(inflater, container, false)
        binding.model = model
        subscribe()
        model.viewSubscribed()
        return binding.root
    }

    private fun subscribe() {
        binding.protocolsRecycler.layoutManager = LinearLayoutManager(activity)
        binding.protocolsRecycler.adapter = adapter

        binding.btnClose.setOnClickListener { onBackPressed() }
        model.updateProtocolListLiveData.observe(this, Observer<MutableList<AbstractRecyclerItem>> { adapter.addButch(it, true) })
        model.vpnRestartedLiveData.observe(this, Observer<Boolean> { showFragment(VpnFragment::class.java.simpleName) })
        model.hideIkeDescLiveData.observe(this, Observer { setIkeDescVisible(!it) })
        model.hideOvpnDescLiveData.observe(this, Observer { setOvpnDescVisible(!it) })
        model.hideWiseDescLiveData.observe(this, Observer { setWiseDescVisible(!it) })
    }

    private fun setWiseDescVisible(isVisible: Boolean) {
        binding.llWiseProtoBlock.visibility = if (isVisible) View.VISIBLE else View.GONE
    }

    private fun setOvpnDescVisible(isVisible: Boolean) {
        binding.llOvpnProtoBlock.visibility = if (isVisible) View.VISIBLE else View.GONE
    }

    private fun setIkeDescVisible(isVisible: Boolean) {
        binding.llIkeProtoBlock.visibility = if (isVisible) View.VISIBLE else View.GONE
    }

    override fun onBackPressed(): Boolean {
        showFragment(SettingsFragment::class.java.simpleName)
        return false
    }


}