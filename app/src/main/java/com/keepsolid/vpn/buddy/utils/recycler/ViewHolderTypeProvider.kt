package com.keepsolid.vpn.buddy.utils.recycler

import android.view.LayoutInflater
import android.view.ViewGroup
import com.keepsolid.vpn.buddy.R
import com.keepsolid.vpn.buddy.purchase.PurchaseViewHolder
import com.keepsolid.vpn.buddy.servers.ServerViewHolder
import com.keepsolid.vpn.buddy.settings.protocol.OptimalProtocolViewHolder
import com.keepsolid.vpn.buddy.settings.protocol.ProtocolViewHolder
import com.keepsolid.vpn.buddy.settings.trustednet.TrustedNetworksViewHolder
import java.util.*


class ViewHolderTypeProvider {

    companion object {
        const val ITEM_TITLE = 0
        const val ITEM_SERVER = 1
        const val ITEM_PURCHASE = 2
        const val ITEM_TEXT_CONTENT = 3
        const val ITEM_PROTOCOL = 4
        const val ITEM_PROTOCOL_OPTIMAL = 5
        const val ITEM_TRUSTED_NETWORK = 6
    }

    private var viewHolderFactory: HashMap<Int, Int> = HashMap()

    init {
        viewHolderFactory[ITEM_TITLE] = R.layout.item_server_title
        viewHolderFactory[ITEM_SERVER] = R.layout.item_server
        viewHolderFactory[ITEM_PURCHASE] = R.layout.item_purchase
        viewHolderFactory[ITEM_TEXT_CONTENT] = R.layout.item_text_content
        viewHolderFactory[ITEM_PROTOCOL] = R.layout.radio_settings_recycler_item
        viewHolderFactory[ITEM_PROTOCOL_OPTIMAL] = R.layout.optimal_protocol_settings_recycler_item
        viewHolderFactory[ITEM_TRUSTED_NETWORK] = R.layout.trusted_networks_recycler_item
    }

    fun createViewHolder(parent: ViewGroup, itemType: Int): AbstractViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(viewHolderFactory[itemType]
                ?: 0, parent, false)

        var holder: AbstractViewHolder? = null

        when (itemType) {
            ITEM_TITLE -> holder = TitleViewHolder(view)
            ITEM_SERVER -> holder = ServerViewHolder(view)
            ITEM_PURCHASE -> holder = PurchaseViewHolder(view)
            ITEM_TEXT_CONTENT -> holder = TextContentViewHolder(view)
            ITEM_PROTOCOL -> holder = ProtocolViewHolder(view)
            ITEM_PROTOCOL_OPTIMAL -> holder = OptimalProtocolViewHolder(view)
            ITEM_TRUSTED_NETWORK -> holder = TrustedNetworksViewHolder(view)
        }

        return holder!!
    }
}