package com.keepsolid.vpn.buddy.base

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.keepsolid.vpn.buddy.auth.SignInViewModel
import com.keepsolid.vpn.buddy.interstitial.InterstitialViewModel
import com.keepsolid.vpn.buddy.network.KeepSolidApi
import com.keepsolid.vpn.buddy.purchase.PurchaseViewModel
import com.keepsolid.vpn.buddy.servers.ServersViewModel
import com.keepsolid.vpn.buddy.settings.SettingsViewModel
import com.keepsolid.vpn.buddy.settings.killswitch.KillSwitchViewModel
import com.keepsolid.vpn.buddy.settings.protocol.ProtocolsViewModel
import com.keepsolid.vpn.buddy.settings.trustednet.TrustedNetworksViewModel
import com.keepsolid.vpn.buddy.utils.EventManager
import com.keepsolid.vpn.buddy.utils.SharedPrefManager
import com.keepsolid.vpn.buddy.utils.fingerprints.GoogleFingerprintProvider
import com.keepsolid.vpn.buddy.vpn.VpnViewModel

open class ViewModelFactory constructor(private val context: Context,
                                        private val ksApi: KeepSolidApi,
                                        private val sharedPrefManager: SharedPrefManager,
                                        private val fingerprintManager: GoogleFingerprintProvider,
                                        private val eventManager: EventManager) : ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>) = with(modelClass) {
        when {
            isAssignableFrom(SignInViewModel::class.java) -> SignInViewModel(context, ksApi, sharedPrefManager, eventManager, fingerprintManager)
            isAssignableFrom(VpnViewModel::class.java) -> {
                if (getViewModel(VpnViewModel::class.java.simpleName) != null) getViewModel(VpnViewModel::class.java.simpleName)
                else {
                    val viewModel = VpnViewModel(context, ksApi)
                    addViewModel(VpnViewModel::class.java.simpleName, viewModel)
                    return@with viewModel
                }
            }

            isAssignableFrom(ServersViewModel::class.java) -> {
                if (getViewModel(ServersViewModel::class.java.simpleName) != null) getViewModel(ServersViewModel::class.java.simpleName)
                else {
                    val viewModel = ServersViewModel(context, ksApi, sharedPrefManager)
                    addViewModel(ServersViewModel::class.java.simpleName, viewModel)
                    return@with viewModel
                }
            }

            isAssignableFrom(SettingsViewModel::class.java) -> {
                if (getViewModel(SettingsViewModel::class.java.simpleName) != null) getViewModel(SettingsViewModel::class.java.simpleName)
                else {
                    val viewModel = SettingsViewModel(context, ksApi, sharedPrefManager, fingerprintManager)
                    addViewModel(SettingsViewModel::class.java.simpleName, viewModel)
                    return@with viewModel
                }
            }

            isAssignableFrom(PurchaseViewModel::class.java) -> PurchaseViewModel(context, ksApi, eventManager, sharedPrefManager)

            isAssignableFrom(InterstitialViewModel::class.java) -> InterstitialViewModel(context, ksApi, eventManager, sharedPrefManager)

            isAssignableFrom(ProtocolsViewModel::class.java) -> ProtocolsViewModel(context, ksApi, eventManager, sharedPrefManager)

            isAssignableFrom(KillSwitchViewModel::class.java) -> KillSwitchViewModel()

            isAssignableFrom(TrustedNetworksViewModel::class.java) -> TrustedNetworksViewModel(context, ksApi, sharedPrefManager)

            else -> throw IllegalArgumentException("Unknown ViewModel class: ${modelClass.name}")
        }
    } as T


    companion object {
        private val hashMapViewModel = HashMap<String, ViewModel>()

        fun addViewModel(key: String, viewModel: ViewModel) {
            hashMapViewModel[key] = viewModel
        }

        fun getViewModel(key: String): ViewModel? = hashMapViewModel[key]

        fun clear() {
            hashMapViewModel.clear()
        }

    }

}