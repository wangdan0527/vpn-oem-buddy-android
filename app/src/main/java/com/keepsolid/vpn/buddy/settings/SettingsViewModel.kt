package com.keepsolid.vpn.buddy.settings

import android.content.Context
import android.content.Intent
import android.os.Build
import android.provider.Settings
import android.text.TextUtils
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import com.keepsolid.androidkeepsolidcommon.vpnunlimitedsdk.entities.VPNUProtoConfig
import com.keepsolid.vpn.buddy.R
import com.keepsolid.vpn.buddy.auth.SignInViewModel.Companion.PASSWORD_CHANGE_MIN_LENGTH
import com.keepsolid.vpn.buddy.base.BaseViewModel
import com.keepsolid.vpn.buddy.base.ViewModelFactory
import com.keepsolid.vpn.buddy.network.KeepSolidApi
import com.keepsolid.vpn.buddy.utils.Constans
import com.keepsolid.vpn.buddy.utils.SharedPrefManager
import com.keepsolid.vpn.buddy.utils.fingerprints.GoogleFingerprintProvider
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*

class SettingsViewModel(private val context: Context,
                        private val ksApi: KeepSolidApi,
                        private val sharedPrefManager: SharedPrefManager,
                        private val fingerprintManager: GoogleFingerprintProvider) : BaseViewModel() {

    val showVpnFragmentLiveData = MutableLiveData<Boolean>()
    val showSigInInActivityLiveData = MutableLiveData<Boolean>()

    val isNeedShowProgressBarLiveData = MutableLiveData<Boolean>()
    val currentProtoNameLiveData = MutableLiveData<String>()
    val showErrorForCurrentPassLayoutLiveData = MutableLiveData<String>()
    val showErrorForNewPasswordLayoutLiveData = MutableLiveData<String>()
    val showErrorForConfirmPasswordLayoutLiveData = MutableLiveData<String>()
    val showAlertTextLiveDataLiveData = MutableLiveData<String>()
    val showKillSwitchItemLiveData = MutableLiveData<Boolean>()
    val rememberPasswordEnabledLiveData = MutableLiveData<Boolean>()
    val fingerprintAvailableLiveData = MutableLiveData<Boolean>()
    val fingerprintEnabledLiveData = MutableLiveData<Boolean>()
    val fingerprintNotAddedLiveData = MutableLiveData<Boolean>()


    var email: ObservableField<String> = ObservableField(sharedPrefManager.getEmail())
    var expires: ObservableField<String> = ObservableField()

    var currentPass: ObservableField<String> = ObservableField()
    var newPass: ObservableField<String> = ObservableField()
    var newPassConf: ObservableField<String> = ObservableField()
    private var savedCurrentPass = sharedPrefManager.getPassword()
    private var isChangePasswordStarted = false

    fun subscribe() {
        ksApi.currentUser?.accountStatus?.let { expires.set(it.timeLeftString) }
        uiScope.launch {
            checkCurrentProtoName()
            checkKillSwitchFeature()
            checkRememberPassword()
            checkFingerprintFeature()
        }

    }

    private fun checkFingerprintFeature() {
        val fingerprintAvailable = Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && fingerprintManager.isFeatureEnabled
        val fingerprintEnabled = sharedPrefManager.isFingerprintProtectionEnabled()

        fingerprintAvailableLiveData.postValue(fingerprintAvailable)

        if (fingerprintAvailable) {
            fingerprintEnabledLiveData.postValue(fingerprintEnabled)
        }

    }

    private fun checkRememberPassword() {
        rememberPasswordEnabledLiveData.postValue(sharedPrefManager.isRememberPasswordEnabled())
    }

    private fun checkKillSwitchFeature() {
        val packageManager = context.packageManager
        val killSwitchSupported = Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1
                && packageManager.resolveActivity(Intent(Settings.ACTION_VPN_SETTINGS), 0) != null
                && packageManager.hasSystemFeature("android.hardware.touchscreen")

        showKillSwitchItemLiveData.postValue(killSwitchSupported)
    }

    private fun checkCurrentProtoName() {

        val protoConfig = sharedPrefManager.getVPNUProtoConfigToConnect()

        var subText = ""

        subText = when (protoConfig.protocolType) {
            VPNUProtoConfig.ProtocolType.OVPN -> context.getString(R.string.proto_openvpn)
            VPNUProtoConfig.ProtocolType.WISE -> {
                if (protoConfig.transport == VPNUProtoConfig.Transport.UDP) {
                    context.getString(R.string.proto_wise_udp)
                } else {
                    context.getString(R.string.proto_wise_tcp)
                }
            }
            VPNUProtoConfig.ProtocolType.IKEV2 -> context.getString(R.string.proto_ikev2)
        }

        currentProtoNameLiveData.postValue(subText)
    }

    fun doOnLogOut() = uiScope.launch {
        try {
            withContext(Dispatchers.IO) {
                sharedPrefManager.savePassword("")
                ViewModelFactory.clear()
                ksApi.logout()
            }
        } catch (e: Exception) {
            Log.d("SettingsViewModel", "SettingsViewModel - Error - doOnLogOut - ${e.message}")
        } finally {
            showSigInInActivityLiveData.value = true
        }
    }

    fun confirmBtnClick() {
        if (!isPasswordCorrect() && !isChangePasswordStarted) return
        isNeedShowProgressBarLiveData.value = true
        isChangePasswordStarted = true
        uiScope.launch {
            try {
                val currentPass = currentPass.get() ?: ""
                val newPass = newPass.get() ?: ""
                ksApi.changePass(currentPass, newPass)
                ksApi.authInKsBackend(false, ksApi.currentUser!!.email, newPass, false)
                ksApi.currentUser!!.password = newPass
                sharedPrefManager.savePassword(newPass)
                savedCurrentPass = newPass
                showAlertTextLiveDataLiveData.value = context.getString(R.string.done)
            } catch (e: Exception) {
                showAlertTextLiveDataLiveData.value = e.message
                Log.d("SettingsViewModel", "SettingsViewModel - Error - confirmBtnClick - ${e.message}")
            } finally {
                clearPasswordFields()
                isNeedShowProgressBarLiveData.value = false
                isChangePasswordStarted = false
            }
        }
    }

    private fun isPasswordCorrect(): Boolean {
        val currentPass = currentPass.get()
        val newPass = newPass.get()
        val newPassConf = newPassConf.get()

        var res = true

        if (TextUtils.isEmpty(currentPass)) {
            showErrorForCurrentPassLayoutLiveData.value = context.getString(R.string.error_empty_current_password)
            res = false
        } else if (currentPass != savedCurrentPass) {
            showErrorForCurrentPassLayoutLiveData.value = context.getString(R.string.error_invalid_current_password)
            res = false
        } else showErrorForCurrentPassLayoutLiveData.value = null

        if (TextUtils.isEmpty(newPass)) {
            showErrorForNewPasswordLayoutLiveData.value = context.getString(R.string.error_empty_new_password)
            res = false
        } else showErrorForNewPasswordLayoutLiveData.value = null

        if (TextUtils.isEmpty(newPassConf)) {
            showErrorForConfirmPasswordLayoutLiveData.value = context.getString(R.string.error_empty_confirm_password)
            res = false
        } else showErrorForConfirmPasswordLayoutLiveData.value = null

        if (!res) {
            return res
        } else if (newPass != newPassConf) {
            showErrorForNewPasswordLayoutLiveData.value = context.resources.getString(R.string.error_password_not_mutch)
            showErrorForConfirmPasswordLayoutLiveData.value = context.resources.getString(R.string.error_password_not_mutch)
            res = false
        } else if (newPass!!.length < PASSWORD_CHANGE_MIN_LENGTH) {
            showErrorForNewPasswordLayoutLiveData.value = context.getString(R.string.error_bad_password)
            res = false
        } else if (newPass.contains(" ")) {
            showErrorForNewPasswordLayoutLiveData.value = context.resources.getString(R.string.error_space_in_password)
            res = false
        } else if (!newPass.matches(Constans.DefaultValue.PASSWORD_REGEX.toRegex())) {
            showErrorForNewPasswordLayoutLiveData.value = context.resources.getString(R.string.error_bad_password_format)
            res = false
        }

        return res
    }

    fun onTextChanged(id: Int) {
        when (id) {
            PASSWORD_ET_ID -> showErrorForCurrentPassLayoutLiveData.value = null
            NEW_PASSWORD_ET_ID -> showErrorForNewPasswordLayoutLiveData.value = null
            CONF_NEW_PASSWORD_ET_ID -> showErrorForConfirmPasswordLayoutLiveData.value = null
        }
    }

    private fun removeAllFavoritesServers() {
        sharedPrefManager.saveFavorite(HashSet())
    }

    override fun onCleared() {
        super.onCleared()
        clearPasswordFields()
        showAlertTextLiveDataLiveData.value=null
    }

    fun clearPasswordFields() {
        showErrorForCurrentPassLayoutLiveData.value = null
        showErrorForNewPasswordLayoutLiveData.value = null
        showErrorForConfirmPasswordLayoutLiveData.value = null
        currentPass.set("")
        newPass.set("")
        newPassConf.set("")
    }

    fun toggleRememberPasswordOption(checked: Boolean) {
        sharedPrefManager.setRememberPasswordEnabled(checked)
        rememberPasswordEnabledLiveData.value = checked

        if (sharedPrefManager.isFingerprintProtectionEnabled() && checked) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                toggleFingerprintProtectionOption(false)
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    fun toggleFingerprintProtectionOption(checked: Boolean) {
        val rememberPasswordEnabled = sharedPrefManager.isRememberPasswordEnabled()
        val fingerprintAdded = fingerprintManager.isFingerprintAdded

        if (!fingerprintAdded) {
            fingerprintNotAddedLiveData.postValue(true)
            fingerprintEnabledLiveData.postValue(false)
            fingerprintNotAddedLiveData.value = false
            return
        }

        if (rememberPasswordEnabled && checked) {
            toggleRememberPasswordOption(false)
        }

        sharedPrefManager.setFingerprintProtectionEnabled(checked)
        fingerprintEnabledLiveData.postValue(checked)

    }

    companion object {
        const val PASSWORD_ET_ID = 1
        const val NEW_PASSWORD_ET_ID = 2
        const val CONF_NEW_PASSWORD_ET_ID = 3
    }
}