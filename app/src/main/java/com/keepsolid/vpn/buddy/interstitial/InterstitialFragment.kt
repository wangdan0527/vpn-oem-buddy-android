package com.keepsolid.vpn.buddy.interstitial

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.android.billingclient.api.BillingClient
import com.android.billingclient.api.BillingFlowParams
import com.android.billingclient.api.SkuDetailsParams
import com.keepsolid.vpn.buddy.R
import com.keepsolid.vpn.buddy.base.BaseFragment
import com.keepsolid.vpn.buddy.base.IOnBackPressed
import com.keepsolid.vpn.buddy.databinding.FragmentInterstitialBinding

import com.keepsolid.vpn.buddy.settings.SettingsFragment
import com.keepsolid.vpn.buddy.utils.ShowMsgManager
import com.keepsolid.vpn.buddy.utils.models.PurchasePeriodType
import com.keepsolid.vpn.buddy.utils.recycler.PurchaseRecyclerItem
import com.keepsolid.vpn.buddy.vpn.VpnFragment
import kotlinx.android.synthetic.main.fragment_interstitial.*
import javax.inject.Inject

class InterstitialFragment @Inject constructor() : BaseFragment(), IOnBackPressed {

    override fun getLayoutRes() = R.layout.fragment_interstitial

    private lateinit var model: InterstitialViewModel
    private lateinit var billingClient: BillingClient
    private lateinit var binding: FragmentInterstitialBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = ViewModelProviders.of(this, viewModelFactory).get(InterstitialViewModel::class.java)
        billingClient = BillingClient
                .newBuilder(activity!!.applicationContext)
                .enablePendingPurchases()
                .setListener(model)
                .build()
        model.billingClient = billingClient
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentInterstitialBinding.inflate(inflater, container, false)
        binding.model = model
        subscribe()
        billingClient.startConnection(model)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        billingClient.endConnection()
    }

    private fun subscribe() {
        binding.btnClose.setOnClickListener { onBackPressed() }
        binding.btnSubscribe.setOnClickListener { onSubscribe() }
        model.isNeedShowProgressBarLiveData.observe(this, Observer<Boolean> {
            if (it != null && it) showProgressbarDialog()
            else hideProgressbarDialog()
        })
        model.updateSubscriptionsListLiveData.observe(this, Observer<MutableList<PurchaseRecyclerItem>> {
            showMonthPrice(it)
        })
        model.showPurchaseDialogLiveData.observe(this, Observer<PurchaseRecyclerItem> { if (it != null) showPurchaseDialog(it) })
        model.showAlertTextLiveData.observe(this, Observer<String> {
            if (TextUtils.isEmpty(it)) return@Observer
            ShowMsgManager.showAlertDialog(
                    context = activity!!,
                    title = activity!!.getString(R.string.information),
                    msg = it,
                    positiveBtnTitle = activity!!.getString(R.string.btn_ok_title),
                    positiveBtnCallback = Runnable { onBackPressed() })
                    .show()
            model.showAlertTextLiveData.value = ""
        })
        model.isNeedClosePurchaseLiveData.observe(this, Observer<Boolean> {
            if (it != null && it) closePurchases()
        })
        model.getSkuListLiveData.observe(this, Observer<MutableList<String>> { if (it != null && it.isNotEmpty()) getSkuDetails(it) })
    }

    private lateinit var item: PurchaseRecyclerItem

    private fun onSubscribe() {

        val flowParams = BillingFlowParams.newBuilder()
                .setSkuDetails(item.purchaseInfo.details)
                .build()
        billingClient.launchBillingFlow(activity, flowParams)

    }

    private fun showMonthPrice(it: MutableList<PurchaseRecyclerItem>) {
        it.forEach { recyclerItem ->
            if(recyclerItem.purchaseInfo.periodType == PurchasePeriodType.DAY && recyclerItem.purchaseInfo.periodLength == 7)
            {
                binding.tvPrice.text = String.format(resources.getString(R.string.then_price), recyclerItem.purchaseInfo.priceString)
                item = recyclerItem
                binding.vScroll.visibility = View.VISIBLE
                return
            }
        }
    }

    private fun getSkuDetails(skuList: MutableList<String>) {
        val params = SkuDetailsParams.newBuilder()
                .setSkusList(skuList)
                .setType(BillingClient.SkuType.SUBS)
                .build()
        billingClient.querySkuDetailsAsync(params, model)
    }

    private fun showPurchaseDialog(item: PurchaseRecyclerItem) {
        val flowParams = BillingFlowParams.newBuilder()
                .setSkuDetails(item.purchaseInfo.details)
                .build()
        billingClient.launchBillingFlow(activity, flowParams)
    }

    private fun closePurchases() {
        ShowMsgManager.showLongToast(activity!!, getString(R.string.error_no_data))
        hideProgressbarDialog()
    }

    override fun onBackPressed(): Boolean {
        showFragment(VpnFragment::class.java.simpleName)
        return false
    }
}