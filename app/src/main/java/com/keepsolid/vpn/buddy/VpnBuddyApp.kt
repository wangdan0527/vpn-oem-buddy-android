package com.keepsolid.vpn.buddy

import android.app.Activity
import android.os.Bundle
import com.adjust.sdk.Adjust
import com.adjust.sdk.AdjustConfig
import com.adjust.sdk.LogLevel
import com.appsflyer.AppsFlyerLib
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.core.CrashlyticsCore
import com.keepsolid.androidkeepsolidcommon.commonsdk.utils.stringutils.KSStringProviderManager
import com.keepsolid.androidkeepsolidcommon.vpnunlimitedsdk.api.VPNUFacade
import com.keepsolid.vpn.buddy.di.DaggerAppComponent
import com.keepsolid.vpn.buddy.utils.EventManager
import com.keepsolid.vpn.buddy.utils.VpnStringProvider
import com.revenuecat.purchases.Purchases
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import io.fabric.sdk.android.Fabric
import javax.inject.Inject


class VpnBuddyApp : DaggerApplication() {

    @Inject
    internal lateinit var eventManager: EventManager

    override fun onCreate() {
        super.onCreate()

        eventManager.init()
        AppsFlyerLib.getInstance().startTracking(this)

        Fabric.with(this, Crashlytics.Builder().core(CrashlyticsCore.Builder().disabled(BuildConfig.DEBUG).build()).build())
        VPNUFacade.getInstance().prepare(this,
                getString(R.string.vpnu_sdk_application_id),
                resources.getString(R.string.vpnu_sdk_application_secret),
                BuildConfig.VERSION_NAME)

        KSStringProviderManager.getInstance().init(VpnStringProvider())

        val appToken = resources.getString(R.string.adjust_token)
        val environment = AdjustConfig.ENVIRONMENT_SANDBOX
        val config = AdjustConfig(this, appToken, environment);
        config.setAppSecret(1, 892918226, 455507841, 2098820706, 135573581)
        config.setLogLevel(LogLevel.VERBOSE)
        Adjust.onCreate(config)

        registerActivityLifecycleCallbacks(AdjustLifecycleCallbakcs())

        Purchases.debugLogsEnabled = true
        Purchases.configure(this, "matDTJFMlkCFVffZUSQpSFKoeCTgTTzM")
    }



    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        val appComponent = DaggerAppComponent.builder().application(this).build()
        appComponent.inject(this)
        return appComponent
    }
    class AdjustLifecycleCallbakcs : ActivityLifecycleCallbacks
    {
        override fun onActivityStarted(activity: Activity?) {
        }

        override fun onActivityDestroyed(activity: Activity?) {
        }

        override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) {
        }

        override fun onActivityStopped(activity: Activity?) {
        }

        override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) {
        }

        override fun onActivityPaused(activity: Activity?) {
            Adjust.onPause()
        }

        override fun onActivityResumed(activity: Activity?) {
            Adjust.onResume()
        }

    }

}