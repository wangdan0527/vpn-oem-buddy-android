package com.keepsolid.vpn.buddy.settings.trustednet

import android.Manifest
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.keepsolid.vpn.buddy.R
import com.keepsolid.vpn.buddy.base.BaseFragment
import com.keepsolid.vpn.buddy.base.IOnBackPressed
import com.keepsolid.vpn.buddy.databinding.FragmentTrustedNetworksBinding
import com.keepsolid.vpn.buddy.settings.SettingsFragment
import com.keepsolid.vpn.buddy.utils.ShowMsgManager
import com.keepsolid.vpn.buddy.utils.recycler.AbstractRecyclerItem
import com.keepsolid.vpn.buddy.utils.recycler.BaseRecyclerAdapter
import javax.inject.Inject

class TrustedNetworksFragment @Inject constructor() : BaseFragment(), IOnBackPressed {

    private lateinit var model: TrustedNetworksViewModel
    private lateinit var binding: FragmentTrustedNetworksBinding

    private val adapter = BaseRecyclerAdapter()

    companion object {
        const val LOCATION_PERMISSION_RC = 173
    }

    override fun getLayoutRes() = R.layout.fragment_trusted_networks

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = ViewModelProviders.of(this, viewModelFactory).get(TrustedNetworksViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentTrustedNetworksBinding.inflate(inflater, container, false)
        binding.model = model
        subscribe()
        model.subscribe()
        return binding.root
    }

    private fun subscribe() {
        binding.rvTrustedNetworksList.layoutManager = LinearLayoutManager(activity)
        binding.rvTrustedNetworksList.adapter = adapter
        binding.btnClose.setOnClickListener { onBackPressed() }

        binding.rvAddNetworkBtnBlock.setOnClickListener { model.addCurrentNetwork() }
        binding.rvAddLteNetworkBtnBlock.setOnClickListener { model.addLteNetwork() }

        model.updateTrustedListLiveDeta.observe(this, Observer<MutableList<AbstractRecyclerItem>> { updateList(it) })
        model.currentNetworkAddedLiveData.observe(this, Observer { binding.rvAddNetworkBtnBlock.visibility = if (it) View.GONE else View.VISIBLE })
        model.lteNetworkAddedLiveData.observe(this, Observer { binding.rvAddLteNetworkBtnBlock.visibility = if (it) View.GONE else View.VISIBLE })
        model.currentNetworkUpdateLiveData.observe(this, Observer { setCurrentNetwork(it) })
        model.showPermissionDialogLiveData.observe(this, Observer { if (it) showPermissionDialog() })
        model.showLocationDialogLiveData.observe(this, Observer { if (it) showLocationDialog() })

    }

    private fun setCurrentNetwork(currentNetwork: String?) {

        var nameToDisplay = currentNetwork

        if (currentNetwork == null || currentNetwork.isEmpty()) {
            nameToDisplay = getString(R.string.S_NO_NETWORKS)
        }

        binding.tvCurrentNetworkName.text = nameToDisplay

    }

    private fun updateList(networkList: MutableList<AbstractRecyclerItem>) {
        if (networkList.isNullOrEmpty()) {
            binding.trustedNetworksTvTitle.visibility = View.GONE
            binding.rvTrustedNetworksList.visibility = View.GONE
        } else {
            binding.trustedNetworksTvTitle.visibility = View.VISIBLE
            binding.rvTrustedNetworksList.visibility = View.VISIBLE
        }

        adapter.addButch(networkList)

    }

    private fun showPermissionDialog() {
        val permissionDialog = ShowMsgManager.showAlertDialog(activity!!,
                getString(R.string.information),
                getString(R.string.S_ANDROID_WIFI_NEED_LOCATION_PERMISSION),
                getString(R.string.proceed_btn),
                Runnable { requestPermissions(arrayOf(Manifest.permission.ACCESS_FINE_LOCATION), LOCATION_PERMISSION_RC) },
                getString(R.string.cancel),
                Runnable { onBackPressed() })
        permissionDialog.setOnCancelListener { onBackPressed() }
        permissionDialog.show()
    }

    private fun showLocationDialog() {
        val locationDialog = ShowMsgManager.showAlertDialog(activity!!,
                getString(R.string.information),
                getString(R.string.S_ANDROID_LOCATION_SERVICES_DISABLED),
                getString(R.string.settings_kill_switch_open_settings),
                Runnable { openLocationSettings() },
                getString(R.string.cancel),
                Runnable { onBackPressed() })
        locationDialog.setOnCancelListener { onBackPressed() }
        locationDialog.show()
    }

    private fun openLocationSettings() {
        try {
            val myIntent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
            startActivity(myIntent)
        } catch (e: ActivityNotFoundException) {
            e.printStackTrace()
        }

    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == LOCATION_PERMISSION_RC) {
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            } else {
                onBackPressed()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        model.onResumed()

    }

    override fun onPause() {
        super.onPause()
        model.onPaused()
    }

    override fun onBackPressed(): Boolean {
        showFragment(SettingsFragment::class.java.simpleName)
        return false
    }

}