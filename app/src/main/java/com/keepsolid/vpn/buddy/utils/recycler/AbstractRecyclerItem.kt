package com.keepsolid.vpn.buddy.utils.recycler


import android.view.View

abstract class AbstractRecyclerItem {

    lateinit var onClickListener: View.OnClickListener

    abstract val viewHolderType: Int

    abstract var sortPriority: Int

    abstract var sortName: String

    companion object {
        const val PRIORITY_SEARCH_TITLE = 1
        const val PRIORITY_FAVORITE_TITLE = 1
        const val PRIORITY_PURCHASE_ITEM = 1
        const val PRIORITY_PURCHASE_DESCRIPTIONS = 2
        const val PRIORITY_FAVORITE_OPTIMAL = 2
        const val PRIORITY_FAVORITE_STREAMING = 3
        const val PRIORITY_FAVORITE_USUAL = 4
        const val PRIORITY_ALL_TITLE = 5
        const val PRIORITY_ALL_OPTIMAL = 6
        const val PRIORITY_ALL_STREAMING = 7
        const val PRIORITY_ALL_USUAL = 8
    }
}
