package com.keepsolid.vpn.buddy.utils.recycler

import android.view.View
import android.widget.TextView
import com.keepsolid.vpn.buddy.R

class TitleViewHolder(itemView: View) : AbstractViewHolder(itemView) {

    val name: TextView = itemView.findViewById(R.id.item_server_tv_title)

    override fun buildView(metadata: AbstractRecyclerItem) {
        val recyclerItem = metadata as TitleRecyclerItem
        name.text = recyclerItem.title
    }

}
