package com.keepsolid.vpn.buddy.utils

class Constans {

    interface DefaultValue {
        companion object {
            const val EMAIL_REGEX = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#" +
                    "$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-" +
                    "\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]" +
                    "*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|" +
                    "[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]" +
                    ":(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09" +
                    "\\x0b\\x0c\\x0e-\\x7f])+)\\])"

            const val PASSWORD_REGEX = "^[ -~]{8,}$"
        }
    }

    interface PrefValues {
        companion object {
            const val MY_PREF = "com.keepsolid.vpn.wl.pref"
            const val PREF_EMAIL = "com.keepsolid.vpn.wl.pref.email"
            const val PREF_PASSWORD = "com.keepsolid.vpn.wl.pref.password"
            const val FAVORITE_SERVERS_SET = "com.keepsolid.vpn.wl.pref.favorite.set"
            const val AVAILABLE_PROTOCOLS_LIST = "com.keepsolid.vpn.wl.pref.proto.available.list"
            const val VPN_PROTO_PREFERRED = "com.keepsolid.vpn.wl.pref.proto.preferred"
            const val PREFERRED_PROTOCOLS_INITIALIZED = "com.keepsolid.vpn.wl.pref.proto.preferred.initialized"
            const val PREFERRED_PROTOCOLS_CURRENT = "com.keepsolid.vpn.wl.pref.proto.current"
            const val PREFERRED_PROTOCOLS_TO_CONNECT = "com.keepsolid.vpn.wl.pref.proto.to_connect"
            const val PREFERRED_PROTOCOLS_AUTO_ENABLED = "com.keepsolid.vpn.wl.pref.proto.auto_enabled"
            const val VPN_PROFILE_RELOAD_NEEDED = "com.keepsolid.vpn.wl.pref.profile.reload.needed"
            const val REMEMBER_PASSWORD_ENABLED = "com.keepsolid.vpn.wl.pref.account.remember.password"
            const val FINGERPRINT_ENABLED = "com.keepsolid.vpn.wl.pref.account.fingerprint_enabled"

            const val TRANSACTION_DATA = "com.keepsolid.vpn.wl.pref.account.transaction_data"

        }
    }

}