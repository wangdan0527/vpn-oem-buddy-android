package com.keepsolid.vpn.buddy.utils.fingerprints;

import android.content.Context;
import android.hardware.fingerprint.FingerprintManager;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.keepsolid.androidkeepsolidcommon.commonsdk.utils.stringutils.StringUtils;
import com.keepsolid.vpn.buddy.R;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.core.hardware.fingerprint.FingerprintManagerCompat;
import androidx.core.os.CancellationSignal;


@RequiresApi(api = Build.VERSION_CODES.M)
public class GoogleFingerprintDialog extends AlertDialog {

    static final long ERROR_TIMEOUT_MILLIS = 1600;
    static final long ERROR_TIMEOUT_LOCKOUT_MILLIS = 65000;
    static final long SUCCESS_DELAY_MILLIS = 1300;

    private FingerprintManagerCompat fingerprintManager;
    private ImageView icon;
    private TextView errorTextView;
    private CancellationSignal cancellationSignal;
    private FingerprintIdentifyCallback identifyCallback;
    private boolean selfCancalled;
    private FingerprintCallback callback;


    protected GoogleFingerprintDialog(@NonNull Context context,
                                      FingerprintManagerCompat fingerprintManager,
                                      FingerprintIdentifyCallback identifyCallback) {
        super(context);
        this.fingerprintManager = fingerprintManager;
        this.identifyCallback = identifyCallback;

        LayoutInflater inflater = LayoutInflater.from(getContext());

        View v = inflater.inflate(R.layout.google_fingerpeint_dialog, null);

        icon = v.findViewById(R.id.fingerprint_icon);
        errorTextView = v.findViewById(R.id.fingerprint_status);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setButton(BUTTON_NEGATIVE, StringUtils.getStringById(getContext(), R.string.S_CANCEL), (dialog, which) -> cancelAuthentication());

        setCancelable(false);

        setView(v);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        startAuthentication();

    }


    public void startAuthentication() {
        callback = new FingerprintCallback();
        cancellationSignal = new CancellationSignal();
        selfCancalled = false;
        fingerprintManager.authenticate(null, 0, cancellationSignal, callback, null);
        icon.setImageResource(R.drawable.ic_fp_40px);

    }

    public void cancelAuthentication() {
        if (cancellationSignal != null) {
            selfCancalled = true;
            cancellationSignal.cancel();
            cancellationSignal = null;
            identifyCallback.onUserCancel();
            dismiss();
        }
    }

    private void showError(CharSequence error) {
        showError(error, ERROR_TIMEOUT_MILLIS);
    }

    private void showError(CharSequence error, long timeoutMillis) {
        icon.setImageResource(R.drawable.ic_fingerprint_error);
        errorTextView.setText(error);
        errorTextView.setAllCaps(false);
        errorTextView.setTextColor(
                errorTextView.getResources().getColor(R.color.primary_red, null));
        errorTextView.removeCallbacks(mResetErrorTextRunnable);
        errorTextView.postDelayed(mResetErrorTextRunnable, timeoutMillis);
    }

    Runnable mResetErrorTextRunnable = new Runnable() {
        @Override
        public void run() {
            errorTextView.setTextColor(
                    errorTextView.getResources().getColor(R.color.colorTextGray, null));
            errorTextView.setAllCaps(true);
            errorTextView.setText(
                    StringUtils.getStringById(errorTextView.getResources(), R.string.S_FINGERPRINT_DIALOG_HINT));
            icon.setImageResource(R.drawable.ic_fp_40px);
        }
    };


    private class FingerprintCallback extends FingerprintManagerCompat.AuthenticationCallback {
        public FingerprintCallback() {
            super();
        }

        @Override
        public void onAuthenticationError(int errorCode, CharSequence errString) {
            super.onAuthenticationError(errorCode, errString);
            if (!selfCancalled) {
                if (errorCode == FingerprintManager.FINGERPRINT_ERROR_LOCKOUT) {
                    showError(errString, ERROR_TIMEOUT_LOCKOUT_MILLIS);
                } else {
                    showError(errString);
                }
            }
        }

        @Override
        public void onAuthenticationHelp(int helpCode, CharSequence helpString) {
            super.onAuthenticationHelp(helpCode, helpString);
            showError(helpString);
        }

        @Override
        public void onAuthenticationSucceeded(FingerprintManagerCompat.AuthenticationResult result) {
            super.onAuthenticationSucceeded(result);
            errorTextView.removeCallbacks(mResetErrorTextRunnable);
            icon.setImageResource(R.drawable.ic_fingerprint_success);
            errorTextView.setTextColor(
                    errorTextView.getResources().getColor(R.color.colorAccent, null));
            errorTextView.setText(
                    StringUtils.getStringById(errorTextView.getResources(), R.string.S_FINGERPRINT_DIALOG_SUCCESS));
            icon.postDelayed(() -> {
                identifyCallback.onIdentifySuccess();
                cancelAuthentication();
            }, SUCCESS_DELAY_MILLIS);
        }

        @Override
        public void onAuthenticationFailed() {
            super.onAuthenticationFailed();
            identifyCallback.onIdentifyFail();
            showError(StringUtils.getStringById(icon.getResources(), R.string.S_FINGERPRINT_DIALOG_FAILURE));
        }
    }
}
