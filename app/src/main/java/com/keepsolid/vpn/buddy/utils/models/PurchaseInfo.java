package com.keepsolid.vpn.buddy.utils.models;

import android.graphics.Bitmap;

import com.android.billingclient.api.SkuDetails;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Entity that describes Purchase item for VPN Unlimited.
 */
public class PurchaseInfo {

    private int id;
    private String identifier;
    private String name;
    private String description;
    private String priceString;
    private int cost;
    private String url;
    private PurchaseService purchaseService;
    private PurchasePeriodType periodType;
    private int periodLength;
    private boolean isSuite;
    private int serverTraffic;
    private int count;
    private int savePercents;
    private HashMap<Float, String> iconUrls;
    private boolean isRecurring;
    private String priceType;
    private Bitmap icon;

    public SkuDetails details;

    public PurchaseInfo(JSONObject jsonObject) throws JSONException {
        this.id = jsonObject.optInt("id", -1);
        this.identifier = jsonObject.optString("identifier", null);
        this.name = jsonObject.optString("name", "");
        this.description = jsonObject.optString("description", "");
        this.cost = jsonObject.optInt("cost", -1);
        this.url = jsonObject.optString("url", null);
        this.periodType = PurchasePeriodType.parse(jsonObject.optString("period_type"));
        this.periodLength = jsonObject.optInt("period", -1);
        this.isSuite = jsonObject.has("suite") && jsonObject.getBoolean("suite");
        this.purchaseService = PurchaseService.parse(jsonObject.optString("service"));
        this.count = jsonObject.optInt("count", 1);
        this.isRecurring = jsonObject.optBoolean("recurring");

        if (jsonObject.optJSONArray("info") != null) {
            JSONArray infoArray = jsonObject.getJSONArray("info");
            for (int i = 0; i < infoArray.length(); i++) {
                JSONObject infoEntry = infoArray.getJSONObject(i);
                if (infoEntry.has("service") && infoEntry.getString("service").equalsIgnoreCase(PurchaseService.VPN_SERVER.toString())) {
                    this.serverTraffic = infoEntry.optInt("traffic", 0);
                }
            }
        }

        iconUrls = new HashMap<>();
        JSONObject iconsJson = jsonObject.optJSONObject("icons");
        if (iconsJson != null) {
            iconUrls.put(1.0f, iconsJson.optString("android_mdpi", null));
            iconUrls.put(1.5f, iconsJson.optString("android_hdpi", null));
            iconUrls.put(2.0f, iconsJson.optString("android_xhdpi", null));
            iconUrls.put(3.0f, iconsJson.optString("android_xxhdpi", null));
            iconUrls.put(4.0f, iconsJson.optString("android_xxxhdpi", null));
        }

        this.icon = null;
        this.priceType = "USD";

    }

    public PurchaseInfo(int id, String identifier, String name, String description, int cost, String url, PurchasePeriodType periodType, int periodLength, boolean isSuite) {
        this.id = id;
        this.identifier = identifier;
        this.name = name;
        this.description = description;
        this.cost = cost;
        this.url = url;
        this.periodType = periodType;
        this.periodLength = periodLength;
        this.isSuite = isSuite;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCost() {
        return cost;
    }

    public float getCostFloat() {
        return cost / 100f;
    }

    public String getCostString() {
        return String.format("%.2f", getCostFloat());
    }

    public float getCostPerDay() {
        return (float) getCost() / getPeriodInDays();
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Bitmap getIcon() {
        return icon;
    }

    public void setIcon(Bitmap icon) {
        this.icon = icon;
    }

    public PurchasePeriodType getPeriodType() {
        return periodType;
    }

    public void setPeriodType(PurchasePeriodType periodType) {
        this.periodType = periodType;
    }

    public int getPeriodLength() {
        return periodLength;
    }

    public void setPeriodLength(int periodLength) {
        this.periodLength = periodLength;
    }

    public boolean isSuite() {
        return isSuite;
    }

    public void setSuite(boolean suite) {
        isSuite = suite;
    }

    public PurchaseService getPurchaseService() {
        return purchaseService;
    }

    public void setPurchaseService(PurchaseService purchaseService) {
        this.purchaseService = purchaseService;
    }

    public int getServerTraffic() {
        return serverTraffic;
    }

    public void setServerTraffic(int serverTraffic) {
        this.serverTraffic = serverTraffic;
    }

    public int getServerTrafficTb() {
        int trafficTb;
        if (getServerTraffic() != -1) {
            trafficTb = getServerTraffic() / 1024;
        } else {
            trafficTb = -1;
        }

        return trafficTb;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getPeriodInDays() {
        return periodLength * periodType.days();
    }

    public boolean isRecurring() {
        return isRecurring;
    }

    public void setRecurring(boolean recurring) {
        isRecurring = recurring;
    }

    public String getPriceType() {
        return priceType;
    }

    public void setPriceType(String priceType) {
        this.priceType = priceType;
    }

    public int getSavePercents() {
        return savePercents;
    }

    public void setSavePercents(int savePercents) {
        this.savePercents = savePercents;
    }

    public String getIconUrl(float screenDensity) {
        if (iconUrls.isEmpty()) {
            return null;
        }

        if (iconUrls.get(screenDensity) != null) {
            return iconUrls.get(screenDensity);
        }

        float inputDensity = (screenDensity - Math.floor(screenDensity)) == 0.5f ? screenDensity + 0.1f : screenDensity;

        float keyToReturn = 2.0f;
        float smallestDifference = Float.MAX_VALUE;
        for (float key : iconUrls.keySet()) {
            float currentDifference = Math.abs(inputDensity - key);
            if (currentDifference < smallestDifference && !iconUrls.get(key).isEmpty() && !iconUrls.get(key).equals("null")) {
                keyToReturn = key;
                smallestDifference = currentDifference;
            }
        }

        String url = iconUrls.get(keyToReturn);

        if (!url.isEmpty() && !url.equals("null")) {
            return url;
        }

        return null;
    }

    public boolean isPackagePurchase() {
        return count > 1;
    }

    public String getContentDescString() {

        if (this.purchaseService == PurchaseService.VPN) {
            return this.getIdentifier();
        }

        String output = "%s %s purchase";
        String prefix;
        String postfix;

        switch (this.purchaseService) {
            case VPN_IP:
                prefix = "Personal IP";
                break;
            case VPN_SERVER:
                prefix = "Personal Server " + getServerTrafficTb() + "TB";
                break;
            case VPN_SLOT:
                if (getCount() > 1) {
                    prefix = "Additional " + this.getCount() + " Devices";
                } else {
                    prefix = "Additional " + this.getCount() + " Device";
                }
                break;
            default:
                prefix = null;
        }

        if (this.periodType == PurchasePeriodType.MONTH) {
            postfix = "monthly";
        } else if (this.periodType == PurchasePeriodType.YEAR) {
            if (this.getPeriodLength() < 100) {
                postfix = "yearly";
            } else {
                postfix = "once";
            }
        } else {
            postfix = "";
        }

        if (prefix != null) {
            return String.format(output, prefix, postfix);
        } else {
            return this.getIdentifier();
        }

    }

    public String getPriceString() {
        return priceString;
    }

    public void setPriceString(String priceString) {
        this.priceString = priceString;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PurchaseInfo that = (PurchaseInfo) o;

        if (id != that.id) return false;
        return identifier.equals(that.identifier);
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + identifier.hashCode();
        return result;
    }
}
