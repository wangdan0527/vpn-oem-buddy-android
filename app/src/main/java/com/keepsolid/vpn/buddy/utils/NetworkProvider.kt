package com.keepsolid.vpn.buddy.utils

import android.content.Context
import android.location.LocationManager
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.wifi.WifiInfo
import android.net.wifi.WifiManager

class NetworkProvider {

    companion object {
        fun isNetworkEnabled(context: Context): Boolean {
            val manager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkInfo = manager.activeNetworkInfo
            return networkInfo != null && networkInfo.isConnected
        }

        /**
         * Indicates whether WiFi network connectivity exists and it is possible to establish
         * connections and pass data.
         *
         * @return `true` if network connectivity exists, `false` otherwise.
         */
        fun checkWiFiConnection(context: Context): Boolean {
            val manager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
            val networkInfo = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
            return networkInfo != null && networkInfo.isConnected
        }

        /**
         * @return Current connected WIFI SSID as string. null if WiFi not connected.
         */
        fun getCurrentWiFiName(context: Context): String? {
            val manager = context.applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
            if (manager.isWifiEnabled) {
                val wifiInfo = manager.connectionInfo
                if (wifiInfo != null) {
                    val state = WifiInfo.getDetailedStateOf(wifiInfo.supplicantState)
                    if (state == NetworkInfo.DetailedState.CONNECTED || state == NetworkInfo.DetailedState.OBTAINING_IPADDR) {
                        return wifiInfo.ssid.replace("^\"|\"$".toRegex(), "")
                    }
                }
            }
            return null
        }

        fun isLocationEnabled(context: Context): Boolean {
            val lm = context.getSystemService(Context.LOCATION_SERVICE) as LocationManager
            var gpsEnabled = false
            var networkEnabled = false

            try {
                gpsEnabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
            } catch (ex: Exception) {
            }

            try {
                networkEnabled = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
            } catch (ex: Exception) {
            }

            return gpsEnabled || networkEnabled


        }

    }
}