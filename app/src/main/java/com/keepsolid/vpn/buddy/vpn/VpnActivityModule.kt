package com.keepsolid.vpn.buddy.vpn

import com.keepsolid.vpn.buddy.di.FragmentScoped
import com.keepsolid.vpn.buddy.interstitial.InterstitialFragment
import com.keepsolid.vpn.buddy.purchase.PurchaseFragment
import com.keepsolid.vpn.buddy.servers.ServersFragment
import com.keepsolid.vpn.buddy.settings.SettingsFragment
import com.keepsolid.vpn.buddy.settings.killswitch.KillSwitchFragment
import com.keepsolid.vpn.buddy.settings.protocol.ProtocolsFragment
import com.keepsolid.vpn.buddy.settings.trustednet.TrustedNetworksFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class VpnActivityModule {

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun provideVpnFragment(): VpnFragment

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun provideServersFragment(): ServersFragment

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun provideSettingsFragment(): SettingsFragment

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun provideSubscriptionsFragment(): PurchaseFragment

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun provideInterstitialFragment(): InterstitialFragment

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun provideProtocolsFragment(): ProtocolsFragment

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun provideKillSwitchFragment(): KillSwitchFragment

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun provideTrustedNetworks(): TrustedNetworksFragment

}