package com.keepsolid.vpn.buddy.utils.models

import com.keepsolid.androidkeepsolidcommon.commonsdk.entities.KSAccountStatus
import com.keepsolid.androidkeepsolidcommon.commonsdk.entities.KSAccountUserInfo
import com.keepsolid.androidkeepsolidcommon.vpnunlimitedsdk.api.VPNUFacade
import com.keepsolid.androidkeepsolidcommon.vpnunlimitedsdk.entities.VPNUServer

data class CurrentUser constructor(internal val email: String,
                                   internal var password: String,
                                   internal var accountStatus: KSAccountStatus,
                                   internal var ksUserInfo: KSAccountUserInfo,
                                   internal var lastConfiguredServer: VPNUServer?)