package com.keepsolid.vpn.buddy.base

interface IActivityCallbacks {

    fun showServersFragment()

    fun showVpnFragment()

    fun showSettingsFragment()

    fun showSubscriptionsFragment()

    fun showInterstitialFragment()

    fun showProtocolsFragment()

    fun showKillSwitchFragment()

    fun showTrustedNetworksFragment()
}