package com.keepsolid.vpn.buddy.servers

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.keepsolid.vpn.buddy.R
import com.keepsolid.vpn.buddy.base.BaseFragment
import com.keepsolid.vpn.buddy.base.IOnBackPressed
import com.keepsolid.vpn.buddy.databinding.FragmentServersBinding
import com.keepsolid.vpn.buddy.utils.SearchView
import com.keepsolid.vpn.buddy.utils.recycler.AbstractRecyclerItem
import com.keepsolid.vpn.buddy.utils.recycler.BaseRecyclerAdapter
import com.keepsolid.vpn.buddy.vpn.VpnFragment
import javax.inject.Inject

class ServersFragment @Inject constructor() : BaseFragment(), IOnBackPressed {

    private lateinit var model: ServersViewModel
    private lateinit var binding: FragmentServersBinding
    private val adapter = BaseRecyclerAdapter()

    override fun getLayoutRes() = R.layout.fragment_servers

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = ViewModelProviders.of(this, viewModelFactory).get(ServersViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentServersBinding.inflate(inflater, container, false)
        binding.model = model
        subscribe()
        return binding.root
    }

    private fun subscribe() {
        binding.serversRecycler.layoutManager = LinearLayoutManager(activity)
        binding.serversRecycler.adapter = adapter
        model.subscribe(true)
        model.isNeedShowProgressBarLiveData.observe(this, Observer<Boolean> {
            if (it != null && it) showProgressbarDialog()
            else hideProgressbarDialog()
        })
        model.updateServersListLiveData.observe(this, Observer<MutableList<AbstractRecyclerItem>> { adapter.addButch(it) })
        binding.serversClose.setOnClickListener { onBackPressed() }
        binding.serversSearchView.searchCallback = object : SearchView.SearchCallback {
            override fun onChange(search: String) {
                if (TextUtils.isEmpty(search)) model.subscribe(false)
                else
                    model.searchItems(search)
            }

            override fun onCancel() {
                model.subscribe(false)
            }
        }

    }

    override fun onBackPressed(): Boolean {
        showFragment(VpnFragment::class.java.simpleName)
        return false
    }

}