package com.keepsolid.vpn.buddy.utils

import android.app.ActivityManager
import android.content.Context
import com.keepsolid.vpn.buddy.network.KeepSolidApi
import com.keepsolid.vpn.buddy.vpn.VpnActivity
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class MainUtils @Inject constructor(private val context: Context,
                                    private val ksApi: KeepSolidApi) {


    internal fun isAppLaunched(): Boolean {
        return ksApi.currentUser != null && isVpnActivityLoaded()
    }


    private fun isVpnActivityLoaded(): Boolean {
        var isMainActivityLoaded = false
        val am = context.applicationContext.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val taskList = am.getRunningTasks(10)
        for (i in taskList.indices) {
            if (taskList[i].baseActivity.className.equals(VpnActivity::class.java.name, ignoreCase = true)) {
                isMainActivityLoaded = true
                break
            }
        }
        return isMainActivityLoaded
    }
}