package com.keepsolid.vpn.buddy.utils

import android.content.Context
import android.content.Intent
import com.keepsolid.vpn.buddy.auth.SignInActivity
import com.keepsolid.vpn.buddy.vpn.VpnActivity

class NavigationManager {

    companion object {

        internal fun openVPNActivity(context: Context) = openVPNActivity(context, false)

        internal fun openVPNActivity(context: Context, isNoActivityContext: Boolean) {
            val intent = Intent(context, VpnActivity::class.java)
            if (isNoActivityContext) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
            }
            context.startActivity(intent)
        }

        internal fun openSignInActivity(context: Context) = openSignInActivity(context, false)

        internal fun openSignInActivity(context: Context, isNoActivityContext: Boolean) {
            val intent = Intent(context, SignInActivity::class.java)
            if (isNoActivityContext) {
                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP)
            }
            context.startActivity(intent)
        }
    }

}