package com.keepsolid.vpn.buddy.utils

import android.content.Context
import android.util.Log
import com.android.billingclient.api.SkuDetails
import com.appsflyer.AFInAppEventParameterName
import com.appsflyer.AFInAppEventType
import com.appsflyer.AppsFlyerConversionListener
import com.appsflyer.AppsFlyerLib
import java.util.*
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class EventManager @Inject constructor(private val context: Context) : AppsFlyerConversionListener {

    internal fun init() {
        AppsFlyerLib.getInstance().init(
                context.getString(com.keepsolid.vpn.buddy.R.string.apps_flyer_key),
                this,
                context.applicationContext)
    }

    internal fun addRegistrationEvent() {
        AppsFlyerLib.getInstance().trackEvent(context.applicationContext, AFInAppEventType.COMPLETE_REGISTRATION, HashMap<String, Any>())
    }

    internal fun addPurchaseEvent(purchase: SkuDetails) {
        var cost = purchase.priceAmountMicros.toFloat()
        try {
            cost /= 1_000_000f
        } catch (e: Exception) {
            Log.d("EventManager", "EventManager - error - ${e.message}")
        }
        val eventValues = HashMap<String, Any>()
        eventValues[AFInAppEventParameterName.REVENUE] = cost
        eventValues[AFInAppEventParameterName.CURRENCY] = purchase.priceCurrencyCode

        AppsFlyerLib.getInstance().trackEvent(context.applicationContext, AFInAppEventType.PURCHASE, eventValues)
    }

    internal fun addPurchaseEvent(price: Float, currency: String) {
        var cost = price
        try {
            cost /= 1_000_000f
        } catch (e: Exception) {
            Log.d("EventManager", "EventManager - error - ${e.message}")
        }
        val eventValues = HashMap<String, Any>()
        eventValues[AFInAppEventParameterName.REVENUE] = cost
        eventValues[AFInAppEventParameterName.CURRENCY] = currency

        AppsFlyerLib.getInstance().trackEvent(context.applicationContext, AFInAppEventType.PURCHASE, eventValues)
    }

    override fun onAppOpenAttribution(p0: MutableMap<String, String>?) {
        //Do nothing
    }

    override fun onAttributionFailure(p0: String?) {
        //Do nothing
    }

    override fun onInstallConversionDataLoaded(p0: MutableMap<String, String>?) {
        //Do nothing
    }

    override fun onInstallConversionFailure(p0: String?) {
        //Do nothing
    }
}