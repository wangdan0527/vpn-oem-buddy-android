package com.keepsolid.vpn.buddy.di

import android.app.Application
import com.keepsolid.vpn.buddy.VpnBuddyApp

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import dagger.android.support.DaggerApplication
import javax.inject.Singleton

@Singleton
@Component(modules = [
    ApplicationModule::class,
    ActivityBindingModule::class,
    BroadcastReceiverBindingModule::class,
    AndroidSupportInjectionModule::class]
)
interface AppComponent : AndroidInjector<DaggerApplication> {

    fun inject(application: VpnBuddyApp)

    override fun inject(instance: DaggerApplication)

    @Component.Builder
    interface Builder {

        @BindsInstance
        fun application(application: Application): AppComponent.Builder

        fun build(): AppComponent
    }
}
