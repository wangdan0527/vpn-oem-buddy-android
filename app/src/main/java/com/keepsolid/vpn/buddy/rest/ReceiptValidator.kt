package com.keepsolid.vpn.buddy.rest

import android.content.Context
import com.android.billingclient.api.Purchase
import com.android.billingclient.api.SkuDetails
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential
import com.google.api.client.http.HttpTransport
import com.google.api.client.http.javanet.NetHttpTransport
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.services.androidpublisher.AndroidPublisher
import com.google.api.services.androidpublisher.AndroidPublisherScopes
import com.google.api.services.androidpublisher.model.SubscriptionPurchase
import com.keepsolid.vpn.buddy.R
import com.keepsolid.vpn.buddy.utils.EventManager
import com.keepsolid.vpn.buddy.utils.SharedPrefManager
import org.json.JSONObject
import java.util.*

class ReceiptValidator
{

    companion object{

        const val FIELD_CURRENCY:     String = "FIELD_CURRENCY"
        const val FIELD_PRICE:        String = "FIELD_PRICE"
        const val FIELD_TOKEN:        String = "FIELD_TOKEN"
        const val FIELD_PRODUCT_ID:   String = "FIELD_PRODUCT_ID"

        fun validate(context: Context)
        {
            Thread(Runnable {
                var httpTransport: HttpTransport = NetHttpTransport()
                var jsonFactory: JacksonFactory = JacksonFactory.getDefaultInstance()
                var applicationName: String = context.resources.getString(R.string.app_name)
                var packageName: String = context.packageName

                var scopes : Set<String> = Collections.singleton(AndroidPublisherScopes.ANDROIDPUBLISHER)

                var credential : GoogleCredential = GoogleCredential.fromStream(context.assets.open("validator.json"))
                credential = credential.createScoped(scopes)
                var pub : AndroidPublisher = AndroidPublisher.Builder(httpTransport, jsonFactory, credential)
                        .setApplicationName(applicationName)
                        .build()

                var transactionData = getTransactionData(context)

                var eventManager = EventManager(context)
                var newTransactionData : MutableSet<JSONObject> = mutableSetOf()

                for(i in 0 until transactionData.size)
                {
                    var skuDetails : JSONObject = transactionData.elementAt(i)

                    var get: AndroidPublisher.Purchases.Subscriptions.Get = pub.Purchases().Subscriptions().get(packageName, skuDetails.getString(FIELD_PRODUCT_ID), skuDetails.getString(FIELD_TOKEN))
                    var purchase : SubscriptionPurchase = get.execute()
                    if(purchase != null && purchase.paymentState != null)
                    {
                        var paymentState : Int = purchase.paymentState
                        if(paymentState == 1)
                        {
                            ChartMogulService.postPuchaseData(context, skuDetails.getString(FIELD_PRODUCT_ID), skuDetails.getString(FIELD_TOKEN))
                            eventManager.addPurchaseEvent(skuDetails.getDouble(FIELD_PRICE).toFloat(), skuDetails.getString(FIELD_CURRENCY))
                        }
                        else
                        {
                            newTransactionData.add(skuDetails)
                        }
                    }
                }

                putTransactionDataList(context, newTransactionData)
            }).start()

        }

        fun putTransactionData(context: Context, transactionData: SkuDetails, purchase: Purchase)
        {
            var jsonObject = JSONObject()
            jsonObject.put(FIELD_TOKEN, purchase.purchaseToken)
            jsonObject.put(FIELD_PRICE, transactionData.priceAmountMicros.toFloat())
            jsonObject.put(FIELD_CURRENCY, transactionData.priceCurrencyCode)
            jsonObject.put(FIELD_PRODUCT_ID, transactionData.sku)

            var sharedPrefManager = SharedPrefManager(context)
            sharedPrefManager.putTransactionData(jsonObject)
        }

        fun putTransactionDataList(context: Context, transactionData: Set<JSONObject>)
        {
            var sharedPrefManager = SharedPrefManager(context)
            sharedPrefManager.putTransactionDataList(transactionData)
        }

        fun getTransactionData(context: Context) : Set<JSONObject>
        {
            var sharedPrefManager = SharedPrefManager(context)
            return sharedPrefManager.getTransactionData()
        }

    }
}
