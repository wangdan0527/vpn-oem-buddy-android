package com.keepsolid.vpn.buddy.purchase

import android.annotation.SuppressLint
import android.view.View
import android.widget.TextView
import com.keepsolid.vpn.buddy.R
import com.keepsolid.vpn.buddy.utils.recycler.AbstractRecyclerItem
import com.keepsolid.vpn.buddy.utils.recycler.AbstractViewHolder
import com.keepsolid.vpn.buddy.utils.recycler.PurchaseRecyclerItem

class PurchaseViewHolder(itemView: View) : AbstractViewHolder(itemView) {


    val name: TextView = itemView.findViewById(R.id.item_purchase_name)
    private val price: TextView = itemView.findViewById(R.id.item_purchase_price)

    @SuppressLint("SetTextI18n")
    override fun buildView(metadata: AbstractRecyclerItem) {
        val recyclerItem = metadata as PurchaseRecyclerItem

        var nLastIndex = recyclerItem.purchaseInfo.name.indexOf('(')
        if(nLastIndex == -1)
        {
            nLastIndex = recyclerItem.purchaseInfo.name.length
        }

        name.text = recyclerItem.purchaseInfo.name.substring(0, nLastIndex)
        price.text = recyclerItem.purchaseInfo.priceString

        itemView.setOnClickListener(recyclerItem.onClickListener)
    }

}