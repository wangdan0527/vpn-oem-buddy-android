package com.keepsolid.vpn.buddy.settings.protocol

import android.content.Context
import android.util.Log
import android.view.View
import android.widget.CompoundButton
import androidx.lifecycle.MutableLiveData
import com.keepsolid.androidkeepsolidcommon.commonsdk.api.exceptions.KSException
import com.keepsolid.androidkeepsolidcommon.vpnunlimitedsdk.entities.VPNUProtoConfig
import com.keepsolid.androidkeepsolidcommon.vpnunlimitedsdk.entities.VpnStatus
import com.keepsolid.vpn.buddy.R
import com.keepsolid.vpn.buddy.base.BaseViewModel
import com.keepsolid.vpn.buddy.network.KeepSolidApi
import com.keepsolid.vpn.buddy.utils.EventManager
import com.keepsolid.vpn.buddy.utils.SharedPrefManager
import com.keepsolid.vpn.buddy.utils.recycler.AbstractRecyclerItem
import com.keepsolid.vpn.buddy.utils.recycler.OptimalProtocolRecyclerItem
import com.keepsolid.vpn.buddy.utils.recycler.ProtocolRecyclerItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class ProtocolsViewModel(private val context: Context,
                         private val ksApi: KeepSolidApi,
                         private val eventManager: EventManager,
                         private val sharedPrefManager: SharedPrefManager) : BaseViewModel() {

    val plainConfig: VPNUProtoConfig = VPNUProtoConfig(VPNUProtoConfig.ProtocolType.OVPN, VPNUProtoConfig.Transport.UNDEF)
    val wiseTCPConfig: VPNUProtoConfig = VPNUProtoConfig(VPNUProtoConfig.ProtocolType.WISE, VPNUProtoConfig.Transport.TCP)
    val wiseUDPConfig: VPNUProtoConfig = VPNUProtoConfig(VPNUProtoConfig.ProtocolType.WISE, VPNUProtoConfig.Transport.UDP)
    val ikev2Config: VPNUProtoConfig = VPNUProtoConfig(VPNUProtoConfig.ProtocolType.IKEV2, VPNUProtoConfig.Transport.UDP)

    val updateProtocolListLiveData = MutableLiveData<MutableList<AbstractRecyclerItem>>()
    val vpnRestartedLiveData = MutableLiveData<Boolean>()
    val hideIkeDescLiveData = MutableLiveData<Boolean>()
    val hideOvpnDescLiveData = MutableLiveData<Boolean>()
    val hideWiseDescLiveData = MutableLiveData<Boolean>()

    private val protocolItems = mutableListOf<AbstractRecyclerItem>()
    private lateinit var optimalProtocolRecyclerItem: AbstractRecyclerItem
    private val positionMapping = HashMap<String, Int>()
    private var listEnabled: Boolean = true

    fun viewSubscribed() {
        uiScope.launch {
            generateRecyclerItems()

            if (sharedPrefManager.isVpnProtoAuto()) {
                setModeChecked(sharedPrefManager.getPreferredProtocol().protoString)
                setListEnabled(false)
            } else {
                setModeChecked(sharedPrefManager.getCurrentVPNUProtoConfig().protoString)
                setListEnabled(true)
            }
        }

    }

    private suspend fun generateRecyclerItems() {

        withContext(Dispatchers.IO) {
            val protocols = sharedPrefManager.getProtocolList()

            protocolItems.clear()

            optimalProtocolRecyclerItem = OptimalProtocolRecyclerItem(context.getString(R.string.proto_optimal), "")
            (optimalProtocolRecyclerItem as OptimalProtocolRecyclerItem).isChecked = sharedPrefManager.isVpnProtoAuto()
            (optimalProtocolRecyclerItem as OptimalProtocolRecyclerItem)
                    .onCheckedChangeListener = CompoundButton.OnCheckedChangeListener { _, isChecked ->
                sharedPrefManager.setVpnProtoAuto(isChecked)
                toggleMode(sharedPrefManager.getPreferredProtocol())
            }
            optimalProtocolRecyclerItem.onClickListener = View.OnClickListener {}
            protocolItems.add(optimalProtocolRecyclerItem)


            protocols.forEach { protoString ->
                val item = ProtocolRecyclerItem("", "")

                if (protoString == ikev2Config.protoString) {
                    item.protoname = context.getString(R.string.proto_ikev2)
                    item.onClickListener = View.OnClickListener { toggleMode(ikev2Config) }
                } else if (protoString == plainConfig.protoString) {
                    item.protoname = context.getString(R.string.proto_openvpn)
                    item.onClickListener = View.OnClickListener { toggleMode(plainConfig) }
                } else if (protoString == wiseTCPConfig.protoString) {
                    item.protoname = context.getString(R.string.proto_wise_tcp)
                    item.onClickListener = View.OnClickListener { toggleMode(wiseTCPConfig) }
                } else if (protoString == wiseUDPConfig.protoString) {
                    item.protoname = context.getString(R.string.proto_wise_udp)
                    item.onClickListener = View.OnClickListener { toggleMode(wiseUDPConfig) }
                } else {
                    return@forEach
                }
                protocolItems.add(item)
                positionMapping[protoString] = protocolItems.size - 1

                hideIkeDescLiveData.postValue(!protocols.contains(ikev2Config.protoString))
                hideOvpnDescLiveData.postValue(!protocols.contains(plainConfig.protoString))
                hideWiseDescLiveData.postValue(!protocols.contains(wiseUDPConfig.protoString) && !protocols.contains(wiseTCPConfig.protoString))

            }
        }

    }

    private fun setModeChecked(proto: String) {

        val listPosition = getPositionByProtoString(proto)
        if (listPosition >= 0) {
            for (i in positionMapping.values) {
                (protocolItems[i] as ProtocolRecyclerItem).isChecked = false
            }
            (protocolItems[listPosition] as ProtocolRecyclerItem).isChecked = true
        }

        updateProtocolListLiveData.postValue(protocolItems)

    }

    private fun toggleMode(protoConfig: VPNUProtoConfig) {
        uiScope.launch {
            delay(250)
            val isAutoProto = sharedPrefManager.isVpnProtoAuto()

            setListEnabled(!isAutoProto)
            setModeChecked(protoConfig.protoString)

            if (sharedPrefManager.getCurrentVPNUProtoConfig() == protoConfig) {
                return@launch
            }

            sharedPrefManager.setProfileReloadNeeded(true)
            sharedPrefManager.saveCurrentVPNUProtoConfig(protoConfig)
            //Log.d(LOG_TAG, "Config: " + config.toString() + " is set.");

            val status = ksApi.getVpnStatus()
            if (status.statusCode != VpnStatus.DISABLED) {
                //ksApi.stopVpn() //This produces vpn stop and not enable on some devices.
                ksApi.setConnectionStateHolder(true)
                vpnRestartedLiveData.postValue(true)
                try {
                    ksApi.startVPN()
                } catch (e: KSException) {
                    sharedPrefManager.setProfileReloadNeeded(true)
                    Log.d("ProtocolsVM", "Exception:" + e.response.responseMessage)
                }

            }
        }

    }

    private fun setListEnabled(isEnabled: Boolean) {
        if (listEnabled == isEnabled) {
            return
        }
        listEnabled = isEnabled

        for (i in 1 until protocolItems.size) {
            (protocolItems[i] as ProtocolRecyclerItem).isEnabled = isEnabled
        }

        updateProtocolListLiveData.postValue(protocolItems)

    }

    private fun getPositionByProtoString(proto: String): Int {
        return positionMapping[proto] ?: -1
    }


}