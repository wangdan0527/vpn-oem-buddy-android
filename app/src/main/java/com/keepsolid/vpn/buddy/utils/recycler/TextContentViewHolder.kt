package com.keepsolid.vpn.buddy.utils.recycler

import android.view.View
import android.widget.TextView
import com.keepsolid.vpn.buddy.R

class TextContentViewHolder(itemView: View) : AbstractViewHolder(itemView) {

    val name: TextView = itemView.findViewById(R.id.tv_content)

    override fun buildView(metadata: AbstractRecyclerItem) {
        val recyclerItem = metadata as TextContentRecyclerItem
        name.text = recyclerItem.title
    }

}
