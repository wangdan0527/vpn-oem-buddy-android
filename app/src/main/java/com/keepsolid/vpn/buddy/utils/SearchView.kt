package com.keepsolid.vpn.buddy.utils

import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.*
import android.util.DisplayMetrics
import android.view.inputmethod.EditorInfo
import android.text.Editable
import android.text.TextWatcher
import com.keepsolid.vpn.buddy.R

class SearchView : RelativeLayout {
    constructor(context: Context) : super(context) {
        initState()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        initState()
    }

    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        initState()
    }

    lateinit var searchCallback: SearchCallback

    private var llHolder: LinearLayout = LinearLayout(context)
    //private var llSearch: LinearLayout = LinearLayout(context)
    private val etSearch = EditText(context)
    private val ivCancel = ImageView(context)

    private fun initState() {
        setBackgroundResource(R.drawable.search_bg)

        llHolder.orientation = LinearLayout.HORIZONTAL
        val llParams = RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
        llParams.addRule(CENTER_IN_PARENT)
        llHolder.layoutParams = llParams
        llHolder.gravity = CENTER_VERTICAL

        val ivHolder = ImageView(context)
        ivHolder.setImageResource(R.drawable.ic_search)
        llHolder.addView(ivHolder)

        val tvHolder = TextView(context)
        val tvHolderParams = LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT)
        tvHolder.layoutParams = tvHolderParams
        tvHolder.gravity = CENTER_VERTICAL
        tvHolder.text = context.getString(R.string.search_view_hint)
        tvHolder.setTextAppearance(context, R.style.TextViewRobotoMedium)
        tvHolder.alpha = 0.5f
        tvHolder.setTextColor(resources.getColor(R.color.dark_grey_blue))
        tvHolder.textSize = 16f

        llHolder.addView(tvHolder)

        addView(llHolder)

        val ivParams = RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT)
        ivParams.addRule(CENTER_VERTICAL)
        ivParams.addRule(ALIGN_PARENT_END)
        ivParams.marginEnd = dpToPx(10)
        ivCancel.layoutParams = ivParams
        ivCancel.setImageResource(R.drawable.ic_cancel_small)
        ivCancel.alpha = 0.3f
        ivCancel.setOnClickListener {
            holderState()
            searchCallback.onCancel()
        }
        addView(ivCancel)

        val etParams = RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT)
        etParams.marginStart = dpToPx(10)
        etParams.marginEnd = dpToPx(40)
        etParams.addRule(CENTER_VERTICAL)
        etSearch.layoutParams = etParams
        etSearch.setPadding(0, 0, 0, 0)
        etSearch.setTextAppearance(context, R.style.TextViewRobotoMedium)
        etSearch.setTextColor(resources.getColor(R.color.dark_grey_blue))
        etSearch.background = null
        etSearch.setSingleLine()
        etSearch.imeOptions = EditorInfo.IME_ACTION_DONE
        etSearch.setOnEditorActionListener { _, _, keyEvent ->
            searchCallback.onChange(etSearch.text.toString())
            hideKeyboard()
            return@setOnEditorActionListener true
        }
        etSearch.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {}
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
                searchCallback.onChange(s.toString())
            }
        })
        addView(etSearch)

        setOnClickListener { searchState() }
        holderState()
    }

    fun holderState() {
        llHolder.visibility = View.VISIBLE
        etSearch.visibility = View.GONE
        ivCancel.visibility = View.GONE
        hideKeyboard()
    }

    fun searchState() {
        llHolder.visibility = View.GONE
        etSearch.visibility = View.VISIBLE
        ivCancel.visibility = View.VISIBLE
        etSearch.setText("")
        etSearch.requestFocus()
        showKeyboard()
    }

    private fun showKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.showSoftInput(etSearch, 0)
    }

    private fun hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(rootView.windowToken, 0)
    }

    fun pxToDp(px: Float): Float {
        return px / (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
    }

    fun dpToPx(dp: Int): Int {
        val resources = context.resources
        val metrics = resources.displayMetrics
        return dp * (metrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT).toInt()
    }

    interface SearchCallback {
        fun onChange(search: String)
        fun onCancel()
    }
}