package com.keepsolid.vpn.buddy.vpn

import android.content.Context
import android.text.TextUtils
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.keepsolid.androidkeepsolidcommon.vpnunlimitedsdk.entities.VpnStatus
import com.keepsolid.androidkeepsolidcommon.vpnunlimitedsdk.vpn.VpnStatusChangedListener
import com.keepsolid.vpn.buddy.base.BaseViewModel
import com.keepsolid.vpn.buddy.network.KeepSolidApi
import com.keepsolid.vpn.buddy.utils.models.AccountStatusModel
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class VpnViewModel(private val context: Context,
                   private val ksApi: KeepSolidApi) : BaseViewModel() {

    val vpnStatusChangedLiveData = MutableLiveData<Int>()
    val accountStatusChangedLiveData = MutableLiveData<AccountStatusModel>()
    val isNeedShowPurchasesScreenLiveData = MutableLiveData<Boolean>()
    val isNeedShowInterstitialScreenLiveData = MutableLiveData<Boolean>()
    val isNeedShowTrustedAddDialogLiveData = MutableLiveData<Boolean>()
    val isNeedShowTrustedRemoveDialogLiveData = MutableLiveData<Boolean>()

    private var currentVpnState = VPN_DISCONNECTED
    private var realIP: String? = null

    fun vpnViewSubscribed() {
        uiScope.launch {
            try {
                val status = ksApi.getVpnStatus()
                if (ksApi.connectionStateHolder) {
                    vpnStatusChanged(VpnStatus(VpnStatus.CONNECTING))
                    ksApi.setConnectionStateHolder(false)
                } else {
                    vpnStatusChanged(status)
                }

            } catch (e: Exception) {
                Log.d(LOG_TAG, "Exception - vpnViewSubscribed - getVpnStatus - ${e.message}")
            }
        }
        ksApi.setVpnStatusListener(VpnStatusChangedListener { vpnStatusChanged(it) })
        checkLastSelectedServerFromServerList()
    }

    private fun checkLastSelectedServerFromServerList() {
        if (ksApi.lastSelectedServerFromServerList == null || currentVpnState == VPN_CONNECTING ||
                equalsSelectedServerWithCurrent()) return
        ksApi.currentUser!!.lastConfiguredServer = ksApi.lastSelectedServerFromServerList
        onVpnBtnClick()
    }

    private fun vpnStatusChanged(vpnStatus: VpnStatus) {
        if (ksApi.currentUser!!.accountStatus.isExpired && vpnStatus.statusCode != VpnStatus.DISABLED) beginStopVPN()
        when (vpnStatus.statusCode) {
            VpnStatus.CONNECTED -> {
                if (currentVpnState == VPN_CONNECTED) return
                updateIpOnScreen()
                currentVpnState = VPN_CONNECTED
                vpnStatusChangedLiveData.postValue(currentVpnState)
                ksApi.setConnectionFlagEnabled()
                GlobalScope.launch {
                    try {
                        ksApi.updateAccountStatus()
                        updateIpOnScreen()
                    } catch (e: Exception) {
                        Log.d(LOG_TAG, "Exception - VpnViewModel - vpnStatusChangedLiveData - updateIpOnScreen ${e.message}")
                    }
                }
                checkLastSelectedServerFromServerList()
            }
            VpnStatus.CONNECTING, VpnStatus.AUTHORIZATION -> {
                if (currentVpnState == VPN_CONNECTING) return
                ksApi.setConnectionFlagDisabled()
                currentVpnState = VPN_CONNECTING
                vpnStatusChangedLiveData.postValue(currentVpnState)
                updateIpOnScreen()
            }
            VpnStatus.WAITING_FOR_NETWORK -> {
                currentVpnState = VPN_DISCONNECTED
                ksApi.setConnectionFlagDisabled()
                vpnStatusChangedLiveData.postValue(currentVpnState)
            }
            VpnStatus.EXECUTABLES_NOT_LINKED, VpnStatus.AUTH_FAILED, VpnStatus.TUN_ERROR -> {
                currentVpnState = VPN_DISCONNECTED
                ksApi.setConnectionFlagDisabled()
                vpnStatusChangedLiveData.postValue(currentVpnState)
            }
            VpnStatus.DISABLED -> {
                currentVpnState = VPN_DISCONNECTED
                ksApi.setConnectionFlagDisabled()
                vpnStatusChangedLiveData.postValue(currentVpnState)
                updateIpOnScreen()
            }
        }
    }

    fun onVpnBtnClick() {

        if(ksApi.currentUser == null)
        {
            return
        }

        if (ksApi.currentUser!!.accountStatus == null)
        {
            return
        }

        if (ksApi.currentUser!!.accountStatus.isExpired || ksApi.currentUser!!.accountStatus.isTrialPeriod) {
            isNeedShowInterstitialScreenLiveData.postValue(true)
            ksApi.lastSelectedServerFromServerList = null
            return
        }

        when (currentVpnState) {
            VPN_DISCONNECTED -> {
                if (ksApi.networkPrefsManager.isTrustedNetworksEnabled) {
                    beginSetupVPN()
                    isNeedShowTrustedRemoveDialogLiveData.postValue(true)
                } else {
                    vpnStatusChanged(VpnStatus(VpnStatus.CONNECTING))
                    beginStartedVPN()
                }

            }
            VPN_CONNECTED -> {
                if (ksApi.lastSelectedServerFromServerList != null) {
                    reconnectServer()
                } else {
                    if (ksApi.networkPrefsManager.isTrustedNetworksEnabled) {
                        isNeedShowTrustedAddDialogLiveData.postValue(true)
                    } else {
                        beginStopVPN()
                    }
                }

            }
        }
    }

    fun onTrustedAddDialogNegativeClick() {
        isNeedShowTrustedAddDialogLiveData.value = false
        ksApi.lastSelectedServerFromServerList = null
    }

    fun onTrustedRemoveDialogNegativeClick() {
        isNeedShowTrustedRemoveDialogLiveData.value = false
        ksApi.lastSelectedServerFromServerList = null
    }

    fun onTrustedAddDialogPositiveClick(ssid: String?) {
        isNeedShowTrustedAddDialogLiveData.value = false
        addNetworkToTrusted(ssid)
        beginStopVPN()
    }

    fun onTrustedRemoveDialogPositiveClick(ssid: String?) {
        isNeedShowTrustedRemoveDialogLiveData.value = false
        removeNetworkFromTrusted(ssid)
        beginStartedVPN()
    }

    private fun reconnectServer() {
        if (ksApi.lastSelectedServerFromServerList == null) return
        GlobalScope.launch {
            try {
                ksApi.stopVpn()
                ksApi.startVPN()
                ksApi.lastSelectedServerFromServerList = null
            } catch (e: Exception) {
                Log.d(LOG_TAG, "Exception - VpnViewModel - reconnectServer ${e.message}")
            }
        }
    }

    private fun beginStartedVPN() = GlobalScope.launch {
        try {
            ksApi.startVPN()
        } catch (e: Exception) {
            Log.d(LOG_TAG, "Exception - VpnViewModel - onVpnBtnClick - beginStartedVPN ${e.message}")
        }
    }

    private fun beginSetupVPN() = GlobalScope.launch {
        try {
            ksApi.setupVpnWithputStart()
        } catch (e: Exception) {
            Log.d(LOG_TAG, "Exception - VpnViewModel - onVpnBtnClick - beginStartedVPN ${e.message}")
        }
    }

    private fun beginStopVPN() = GlobalScope.launch {
        try {
            ksApi.stopVpn()
        } catch (e: Exception) {
            Log.d(LOG_TAG, "Exception - VpnViewModel - onVpnBtnClick - beginStopVPN ${e.message}")
        }
    }

    private fun getRealIP(): String {
        return if (!TextUtils.isEmpty(realIP)) realIP!!
        else if (ksApi.currentUser!!.accountStatus.realIP.isNotEmpty()) ksApi.currentUser!!.accountStatus.realIP
        else ""
    }

    private fun addNetworkToTrusted(ssid: String?) {
        if (ssid == null) {
            ksApi.networkPrefsManager.isCellularNetworkTrusted = true
        } else {
            ksApi.networkPrefsManager.addNetworkToTrusted(ssid)
        }
    }

    private fun removeNetworkFromTrusted(ssid: String?) {
        if (ssid == null) {
            ksApi.networkPrefsManager.isCellularNetworkTrusted = false
        } else {
            ksApi.networkPrefsManager.removeNetworkFromTrusted(ssid)
        }
    }

    private fun updateIpOnScreen() {
        cashedRealIp()
        accountStatusChangedLiveData.postValue(AccountStatusModel(
                getRealIP(),
                ksApi.currentUser!!.accountStatus.serverIP,
                ksApi.currentUser!!.lastConfiguredServer,
                currentVpnState == VPN_CONNECTED,
                ksApi.currentUser!!.accountStatus.timeLeftString))
    }

    private fun cashedRealIp() {
        if (TextUtils.isEmpty(realIP)) realIP = ksApi.currentUser!!.accountStatus.serverIP
    }

    private fun equalsSelectedServerWithCurrent() = ksApi.lastSelectedServerFromServerList != null &&
            currentVpnState == VPN_CONNECTED && ksApi.currentUser?.lastConfiguredServer == ksApi.lastSelectedServerFromServerList

    override fun onCleared() {
        super.onCleared()
        ksApi.removeVpnStatusListener()
        isNeedShowPurchasesScreenLiveData.value = false
        isNeedShowInterstitialScreenLiveData.value = false
    }

    companion object {
        val LOG_TAG = ViewModel::class.java.simpleName
        const val VPN_DISCONNECTED = 0
        const val VPN_CONNECTING = 1
        const val VPN_CONNECTED = 2
    }
}