package com.keepsolid.vpn.buddy.utils.recycler

class TrustedNetworkRecyclerItem(var networkName: String) : AbstractRecyclerItem() {

    override val viewHolderType: Int = ViewHolderTypeProvider.ITEM_TRUSTED_NETWORK

    override var sortPriority: Int = 0

    var isChecked: Boolean = false

    override var sortName: String = networkName.toLowerCase()


}