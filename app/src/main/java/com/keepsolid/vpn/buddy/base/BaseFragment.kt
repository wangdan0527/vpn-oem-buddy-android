package com.keepsolid.vpn.buddy.base

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.annotation.LayoutRes
import com.keepsolid.vpn.buddy.R
import com.keepsolid.vpn.buddy.interstitial.InterstitialFragment
import com.keepsolid.vpn.buddy.purchase.PurchaseFragment
import com.keepsolid.vpn.buddy.servers.ServersFragment
import com.keepsolid.vpn.buddy.settings.SettingsFragment
import com.keepsolid.vpn.buddy.settings.killswitch.KillSwitchFragment
import com.keepsolid.vpn.buddy.settings.protocol.ProtocolsFragment
import com.keepsolid.vpn.buddy.settings.trustednet.TrustedNetworksFragment
import com.keepsolid.vpn.buddy.utils.ProgressDialog
import com.keepsolid.vpn.buddy.utils.ShowMsgManager
import com.keepsolid.vpn.buddy.vpn.VpnFragment
import dagger.android.support.DaggerFragment
import javax.inject.Inject


abstract class BaseFragment : DaggerFragment() {

    companion object {
        const val PROGRESS_TAG = "progress_bar"
    }

    @Inject
    protected lateinit var viewModelFactory: ViewModelFactory

    private var isInstanceStateSaved = false

    @LayoutRes
    internal abstract fun getLayoutRes(): Int

    override fun onResume() {
        super.onResume()
        isInstanceStateSaved = false
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        isInstanceStateSaved = true
    }

    protected fun showProgressbarDialog() {
        if (isInstanceStateSaved) return

        val fragment = childFragmentManager.findFragmentByTag(PROGRESS_TAG)

        if (fragment == null) {
            ProgressDialog().show(childFragmentManager, PROGRESS_TAG)
            childFragmentManager.executePendingTransactions()
        }
    }

    protected fun hideProgressbarDialog() {
        if (isInstanceStateSaved) return

        val fragment = childFragmentManager.findFragmentByTag(PROGRESS_TAG)

        fragment?.let {
            (fragment as ProgressDialog).dismissAllowingStateLoss()
            childFragmentManager.executePendingTransactions()
        }
    }

    protected fun showMsgNoInternet() {
        activity?.let { ShowMsgManager.showLongToast(it, it.resources.getString(R.string.error_no_internet)) }
    }

    protected fun showKeyboard() {
        val imm = activity?.applicationContext?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm?.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }

    protected fun showKeyboard(v: View) {
        val imm = activity?.applicationContext?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm?.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT)
    }

    protected fun hideKeyboard() {
        val imm = activity?.applicationContext?.getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm?.hideSoftInputFromWindow(activity?.currentFocus?.windowToken, 0)
    }

    protected fun showFragment(newFragment: String) {
        when (newFragment) {
            VpnFragment::class.java.simpleName -> (activity as? IActivityCallbacks)?.showVpnFragment()
            ServersFragment::class.java.simpleName -> (activity as? IActivityCallbacks)?.showServersFragment()
            SettingsFragment::class.java.simpleName -> (activity as? IActivityCallbacks)?.showSettingsFragment()
            PurchaseFragment::class.java.simpleName -> (activity as? IActivityCallbacks)?.showSubscriptionsFragment()
            InterstitialFragment::class.java.simpleName -> (activity as? IActivityCallbacks)?.showInterstitialFragment()
            ProtocolsFragment::class.java.simpleName -> (activity as? IActivityCallbacks)?.showProtocolsFragment()
            KillSwitchFragment::class.java.simpleName -> (activity as? IActivityCallbacks)?.showKillSwitchFragment()
            TrustedNetworksFragment::class.java.simpleName -> (activity as? IActivityCallbacks)?.showTrustedNetworksFragment()
        }
        hideKeyboard()
    }

}