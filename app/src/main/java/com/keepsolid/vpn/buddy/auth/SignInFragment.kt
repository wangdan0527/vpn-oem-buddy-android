package com.keepsolid.vpn.buddy.auth

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.TextUtils
import android.text.method.LinkMovementMethod
import android.view.*
import android.view.inputmethod.EditorInfo
import android.widget.FrameLayout
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.crashlytics.android.Crashlytics
import com.keepsolid.androidkeepsolidcommon.commonsdk.utils.stringutils.StringUtils
import com.keepsolid.vpn.buddy.R
import com.keepsolid.vpn.buddy.auth.SignInViewModel.Companion.AFTER_SPLASH_VIEW_STATE
import com.keepsolid.vpn.buddy.auth.SignInViewModel.Companion.DEFAULT_TOP_SATE
import com.keepsolid.vpn.buddy.auth.SignInViewModel.Companion.DEFAULT_VIEW_SATE
import com.keepsolid.vpn.buddy.auth.SignInViewModel.Companion.LOGIN_LOCAL_VIEW_STATE
import com.keepsolid.vpn.buddy.auth.SignInViewModel.Companion.LOGIN_VIEW_SATE
import com.keepsolid.vpn.buddy.auth.SignInViewModel.Companion.REGISTRATION_VIEW_SATE
import com.keepsolid.vpn.buddy.base.BaseFragment
import com.keepsolid.vpn.buddy.base.IOnBackPressed
import com.keepsolid.vpn.buddy.databinding.FragmentSignInBinding
import com.keepsolid.vpn.buddy.utils.AnimationUtils
import com.keepsolid.vpn.buddy.utils.NavigationManager
import com.keepsolid.vpn.buddy.utils.ShowMsgManager
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent
import java.util.*
import javax.inject.Inject

class SignInFragment @Inject constructor() : BaseFragment(), IOnBackPressed {

    private lateinit var model: SignInViewModel
    private lateinit var binding: FragmentSignInBinding

    private var alertDialog: AlertDialog? = null
    private var isKeyBoardShowing = false

    override fun getLayoutRes() = R.layout.fragment_sign_in

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = ViewModelProviders.of(this, viewModelFactory).get(SignInViewModel::class.java)

//        Crashlytics.getInstance().crash()
    }

    @SuppressLint("SetTextI18n")
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentSignInBinding.inflate(inflater, container, false)
        subscribe()
        model.subscribe()
        binding.model = model
        binding.signInBottomTv.text = "© ${Calendar.getInstance().get(Calendar.YEAR)} ${getString(R.string.app_name)}"
        binding.signInTermsTv.movementMethod = LinkMovementMethod.getInstance()
        binding.signInTermsTv.text = Html.fromHtml(StringUtils.getStringById(activity!!.applicationContext, R.string.gdpr_agree)
                .replace(activity!!.resources?.getColor(R.color.colorTextBlack).toString(), activity!!.resources?.getColor(R.color.colorLink).toString()))
        return binding.root
    }

    override fun onStop() {
        super.onStop()
        alertDialog?.dismiss()
    }

    override fun onBackPressed(): Boolean {
        return when {
            model.currentViewState == AFTER_SPLASH_VIEW_STATE -> false
            isKeyBoardShowing -> {
                hideKeyboard()
                false
            }
            model.currentViewState != DEFAULT_VIEW_SATE -> {
                setViewState(DEFAULT_VIEW_SATE)
                model.currentViewState = DEFAULT_VIEW_SATE
                return false
            }
            else -> true
        }
    }

    private fun setViewState(stateView: Int) {
        when (stateView) {
            AFTER_SPLASH_VIEW_STATE -> {
                hideKeyboard()
                binding.signInSplashImg.visibility = View.GONE
                binding.signInSplashProgress.visibility = View.GONE
                binding.signInSplashTvAppName.visibility = View.GONE

                AnimationUtils.showSlideUp(binding.signInBottomTv)
                binding.signInMainTopLayout.visibility = View.VISIBLE
                binding.signInBtnNext.visibility = View.VISIBLE
                binding.signInEmailEtLayout.visibility = View.VISIBLE
                binding.signInImgMain.visibility = View.VISIBLE
                binding.signInTvAppName.visibility = View.VISIBLE

                binding.signInEmailEt.clearFocus()

            }
            DEFAULT_VIEW_SATE -> {
                hideKeyboard()

                binding.signInEmailEt.isEnabled = true
                binding.signInEmailEtLayout.isEnabled = true

                binding.signInEmailEtLayout.error = null
                binding.signInPassEtLayout.error = null
                binding.signInPassConfEtLayout.error = null
                binding.signInBtnNext.text = activity?.resources?.getString(R.string.sign_in_btn_next_title)

                binding.signInBackLayout.visibility = View.GONE
                binding.signInPassEtLayout.visibility = View.GONE
                binding.signInPassConfEtLayout.visibility = View.GONE
                binding.signInTermsLayout.visibility = View.GONE
                binding.signInBtnForgotPassword.visibility = View.GONE

                val params = binding.signInMainLayout.layoutParams as FrameLayout.LayoutParams
                params.gravity = Gravity.BOTTOM
                binding.signInMainLayout.layoutParams = params

                binding.signInTvAppName.visibility = View.VISIBLE
                binding.signInImgMain.visibility = View.VISIBLE
                binding.signInBottomTv.visibility = View.VISIBLE

                binding.signInEmailEt.clearFocus()
            }
            DEFAULT_TOP_SATE -> {
                binding.signInPassEtLayout.error = null
                binding.signInPassConfEtLayout.error = null
                binding.signInBackLayoutTvTitle.text = activity?.resources?.getString(R.string.enter_your_email)
                binding.signInBtnNext.text = activity?.resources?.getString(R.string.sign_in_btn_next_title)

                binding.signInBackLayout.visibility = View.VISIBLE

                binding.signInBottomTv.visibility = View.GONE
                binding.signInTvAppName.visibility = View.GONE
                binding.signInImgMain.visibility = View.GONE
                binding.signInPassEtLayout.visibility = View.GONE
                binding.signInPassConfEtLayout.visibility = View.GONE
                binding.signInTermsLayout.visibility = View.GONE
                binding.signInBtnForgotPassword.visibility = View.GONE

                val params = binding.signInMainLayout.layoutParams as FrameLayout.LayoutParams
                params.gravity = Gravity.TOP
                binding.signInMainLayout.layoutParams = params

            }
            LOGIN_VIEW_SATE -> {
                binding.signInPassEtLayout.error = null
                binding.signInPassConfEtLayout.error = null

                binding.signInBackLayoutTvTitle.text = activity?.resources?.getString(R.string.sign_in)
                binding.signInBtnNext.text = activity?.resources?.getString(R.string.sign_in)
                binding.signInPassEt.setText("")
                binding.signInPassConfEt.setText("")
                binding.signInPassEt.requestFocus()

                binding.signInBottomTv.visibility = View.GONE
                binding.signInPassConfEtLayout.visibility = View.GONE
                binding.signInTermsLayout.visibility = View.GONE
                binding.signInTvAppName.visibility = View.GONE
                binding.signInImgMain.visibility = View.GONE

                binding.signInBackLayout.visibility = View.VISIBLE
                binding.signInEmailEtLayout.visibility = View.VISIBLE
                binding.signInPassEtLayout.visibility = View.VISIBLE
                binding.signInBtnForgotPassword.visibility = View.VISIBLE

            }

            LOGIN_LOCAL_VIEW_STATE -> {
                val params = binding.signInMainLayout.layoutParams as FrameLayout.LayoutParams
                params.gravity = Gravity.TOP
                binding.signInMainLayout.layoutParams = params
                binding.signInSplashImg.visibility = View.GONE
                binding.signInSplashProgress.visibility = View.GONE
                binding.signInSplashTvAppName.visibility = View.GONE

                binding.signInPassEtLayout.error = null
                binding.signInPassConfEtLayout.error = null

                binding.signInBackLayoutTvTitle.text = activity?.resources?.getString(R.string.sign_in)
                binding.signInBtnNext.text = activity?.resources?.getString(R.string.sign_in)
                binding.signInPassConfEt.setText("")

                binding.signInBottomTv.visibility = View.GONE
                binding.signInPassConfEtLayout.visibility = View.GONE
                binding.signInTermsLayout.visibility = View.GONE
                binding.signInTvAppName.visibility = View.GONE
                binding.signInImgMain.visibility = View.GONE

                binding.signInBackLayout.visibility = View.VISIBLE
                binding.signInEmailEtLayout.visibility = View.VISIBLE
                binding.signInPassEtLayout.visibility = View.VISIBLE
                binding.signInBtnForgotPassword.visibility = View.VISIBLE
                binding.signInBtnNext.visibility = View.VISIBLE

                binding.signInEmailEt.isEnabled = false
                binding.signInEmailEtLayout.isEnabled = false

                binding.signInPassEt.requestFocus()
                binding.signInPassEt.setText("")
                binding.signInPassEt.postDelayed({ showKeyboard(binding.signInPassEt) }, 500)

            }
            REGISTRATION_VIEW_SATE -> {
                binding.signInPassEtLayout.error = null
                binding.signInPassConfEtLayout.error = null
                binding.signInBackLayoutTvTitle.text = activity?.resources?.getString(R.string.create_account)
                binding.signInBtnNext.text = activity?.resources?.getString(R.string.btn_create)
                binding.signInPassEt.setText("")
                binding.signInPassConfEt.setText("")
                binding.signInPassEt.requestFocus()

                binding.signInBottomTv.visibility = View.GONE
                binding.signInBtnForgotPassword.visibility = View.GONE

                binding.signInBackLayout.visibility = View.VISIBLE
                binding.signInEmailEtLayout.visibility = View.VISIBLE
                binding.signInPassEtLayout.visibility = View.VISIBLE
                binding.signInPassConfEtLayout.visibility = View.VISIBLE
                binding.signInTermsLayout.visibility = View.VISIBLE
                binding.signInTvAppName.visibility = View.GONE
                binding.signInImgMain.visibility = View.GONE
            }
        }
    }

    private fun subscribe() {
        binding.signInEmailEt.setOnFocusChangeListener { _, hasFocus -> model.onFocusChange(hasFocus) }
        //KeyBoard listener
        KeyboardVisibilityEvent.setEventListener(activity) { isOpen -> isKeyBoardShowing = isOpen }
        //Listener for show/hide progressBar
        model.isNeedShowProgressBarLiveData.observe(this, Observer<Boolean> {
            if (it != null && it) showProgressbarDialog()
            else hideProgressbarDialog()
        })
        //Listener for show error on the backend
        model.showAlertTextLiveData.observe(this, Observer<String> {
            if (alertDialog != null || TextUtils.isEmpty(it)) return@Observer
            alertDialog = ShowMsgManager.showAlertDialog(
                    context = activity!!,
                    title = activity!!.getString(R.string.information),
                    msg = it,
                    positiveBtnTitle = activity!!.getString(R.string.btn_ok_title))
            alertDialog?.show()
            alertDialog?.setOnDismissListener { alertDialog = null }
        })
        //Listener for showing error by email field
        model.showErrorForEmailLayoutLiveData.observe(this, Observer<String> {
            binding.signInEmailEtLayout.isErrorEnabled = it == null
            binding.signInEmailEtLayout.error = it
        })
        model.showErrorForPasswordLayoutLiveData.observe(this, Observer<String> {
            binding.signInPassEtLayout.isErrorEnabled = it == null
            binding.signInPassEtLayout.error = it
        })
        model.showErrorForConfirmPasswordLayoutLiveData.observe(this, Observer<String> {
            binding.signInPassConfEtLayout.isErrorEnabled = it == null
            binding.signInPassConfEtLayout.error = it
        })

        model.showFingerPrintDialogLiveData.observe(this, Observer { if (it) showFingerprintDialog() })
        binding.signInBackBtn.setOnClickListener {
            isKeyBoardShowing = false
            onBackPressed()
        }

        model.changeCurrentViewStateLiveData.observe(this, Observer<Int> { setViewState(it) })

        model.isNeedShowVpnActivityLiveData.observe(this, Observer<Boolean> {
            if (it && activity != null) {
                model.showAlertTextLiveData.value = null
                NavigationManager.openVPNActivity(activity!!)
                activity!!.finish()
            }
        })

        model.isNeedShowNoInternetErrorLiveData.observe(this,
                Observer<Boolean> {
                    if (it) {
                        showMsgNoInternet()
                        model.isNeedShowNoInternetErrorLiveData.value = false
                    }
                })

        binding.signInEmailEt.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    model.onNextBtnClick()
                    return true
                }
                return false
            }
        })

        binding.signInPassEt.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    model.onNextBtnClick()
                    return true
                }
                return false
            }
        })

        binding.signInPassConfEt.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    model.onNextBtnClick()
                    return true
                }
                return false
            }
        })
    }

    private fun showFingerprintDialog() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && activity != null) {
            model.showFingerprintDialog(activity!!)
        }
    }

}