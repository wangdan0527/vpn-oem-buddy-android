package com.keepsolid.vpn.buddy.utils.recycler

class TitleRecyclerItem(val title: String,
                        override var sortPriority: Int = 0,
                        override var sortName: String = "") : AbstractRecyclerItem() {

    override val viewHolderType: Int = ViewHolderTypeProvider.ITEM_TITLE

}