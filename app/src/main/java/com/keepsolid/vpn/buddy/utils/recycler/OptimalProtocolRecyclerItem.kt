package com.keepsolid.vpn.buddy.utils.recycler

import android.widget.CompoundButton

class OptimalProtocolRecyclerItem(val protoname: String, val protoDesc: String) : AbstractRecyclerItem() {

    lateinit var onCheckedChangeListener: CompoundButton.OnCheckedChangeListener

    override val viewHolderType: Int = ViewHolderTypeProvider.ITEM_PROTOCOL_OPTIMAL

    override var sortPriority: Int = 0

    var isChecked: Boolean = false

    override var sortName: String = protoname.toLowerCase()

}