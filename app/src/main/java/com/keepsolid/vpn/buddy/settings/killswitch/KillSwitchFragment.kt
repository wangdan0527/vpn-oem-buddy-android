package com.keepsolid.vpn.buddy.settings.killswitch

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.lifecycle.ViewModelProviders
import com.keepsolid.vpn.buddy.R
import com.keepsolid.vpn.buddy.base.BaseFragment
import com.keepsolid.vpn.buddy.base.IOnBackPressed
import com.keepsolid.vpn.buddy.databinding.FragmentKillswitchBinding
import com.keepsolid.vpn.buddy.settings.SettingsFragment
import javax.inject.Inject

class KillSwitchFragment @Inject constructor() : BaseFragment(), IOnBackPressed {

    override fun getLayoutRes() = R.layout.fragment_killswitch

    private lateinit var binding: FragmentKillswitchBinding
    private lateinit var model: KillSwitchViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        model = ViewModelProviders.of(this, viewModelFactory).get(KillSwitchViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentKillswitchBinding.inflate(inflater, container, false)
        subscribe()
        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.N)
    private fun openSettingsScreen() {
        startActivityForResult(Intent(Settings.ACTION_VPN_SETTINGS), 0)
    }

    private fun subscribe() {
        binding.btnClose.setOnClickListener { onBackPressed() }
        binding.openSettingsBtn.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                openSettingsScreen()
            }
        }
    }

    override fun onBackPressed(): Boolean {
        showFragment(SettingsFragment::class.java.simpleName)
        return false
    }


}