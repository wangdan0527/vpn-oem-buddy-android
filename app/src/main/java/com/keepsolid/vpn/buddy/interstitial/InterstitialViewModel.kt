package com.keepsolid.vpn.buddy.interstitial

import android.content.Context
import android.text.TextUtils
import android.util.Log
import android.view.View
import androidx.lifecycle.MutableLiveData
import com.android.billingclient.api.*
import com.keepsolid.androidkeepsolidcommon.commonsdk.entities.KSPurchaseItem
import com.keepsolid.androidkeepsolidcommon.vpnunlimitedsdk.api.VPNUFacade
import com.keepsolid.vpn.buddy.R
import com.keepsolid.vpn.buddy.base.BaseViewModel
import com.keepsolid.vpn.buddy.network.KeepSolidApi
import com.keepsolid.vpn.buddy.rest.ChartMogulService
import com.keepsolid.vpn.buddy.rest.ReceiptValidator
import com.keepsolid.vpn.buddy.utils.EventManager
import com.keepsolid.vpn.buddy.utils.SharedPrefManager
import com.keepsolid.vpn.buddy.utils.models.PurchaseInfo
import com.keepsolid.vpn.buddy.utils.recycler.PurchaseRecyclerItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.json.JSONObject
import java.util.*

class InterstitialViewModel(private val context: Context,
                            private val ksApi: KeepSolidApi,
                            private val eventManager: EventManager,
                            private val sharedPrefManager: SharedPrefManager) : BaseViewModel(),
        SkuDetailsResponseListener, PurchasesUpdatedListener, BillingClientStateListener, AcknowledgePurchaseResponseListener {

    var purchase: Purchase? = null

    override fun onAcknowledgePurchaseResponse(billingResult: BillingResult?) {

    }

    var billingClient: BillingClient? = null

    override fun onPurchasesUpdated(billingResult: BillingResult?, purchases: MutableList<Purchase>?) {
        if (billingResult!!.responseCode != BillingClient.BillingResponseCode.OK || purchases == null) {
            Log.d("PurchaseViewModel", "PurchaseViewModel onPurchasesUpdated - error - code = " +
                    "${billingResult!!.responseCode} - isCanceled = ${billingResult!!.responseCode == BillingClient.BillingResponseCode.USER_CANCELED}")
            return
        }

        if(purchases.size > 0)
        {
            val email = sharedPrefManager.getEmail()
            ChartMogulService.Companion.postPuchaseData(context, purchases.get(0).sku, purchases.get(0).purchaseToken)
            purchase = purchases.get(0)
        }

        for (purchase in purchases)
        {
            handlePurchase(purchase)
        }

        GlobalScope.launch {
            try {
                isNeedShowProgressBarLiveData.postValue(true)
                withContext(Dispatchers.IO) {
                    purchases.forEach { purchase ->
                        val receipt = JSONObject()

                        receipt.put("INAPP_PURCHASE_DATA", purchase.originalJson)
                        receipt.put("INAPP_DATA_SIGNATURE", purchase.signature)

                        val storagePurchaseId = UUID.randomUUID().toString()
                        val purchaseItem = KSPurchaseItem(receipt.toString(), storagePurchaseId, true)

                        val isSucсess = VPNUFacade.getInstance().purchaseManager.validatePurchaseItem(purchaseItem)

                        val email = sharedPrefManager.getEmail()
                        val pass = sharedPrefManager.getPassword()
                        if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(pass)) {
                            ksApi.authInKsBackend(false, email, pass, false)
                        }
                    }
                }
                showAlertTextLiveData.postValue(context.getString(R.string.purchase_success))
            } catch (e: Exception) {
                Log.d("PurchaseViewModel", "PurchaseViewModel onPurchasesUpdated - validatePurchaseItem - error = ${e.message}")
                showAlertTextLiveData.postValue(e.message)
            } finally {
                isNeedShowProgressBarLiveData.postValue(false)
            }
        }
    }

    private fun handlePurchase(purchase: Purchase) {
        if (purchase.purchaseState === Purchase.PurchaseState.PURCHASED)
        {
            if(!purchase.isAcknowledged)
            {
                val acknowledgePurchaseParams = AcknowledgePurchaseParams.newBuilder()
                        .setPurchaseToken(purchase.purchaseToken)
                        .build()
                billingClient?.acknowledgePurchase(acknowledgePurchaseParams, this)
            }
        }
    }

    override fun onSkuDetailsResponse(billingResult: BillingResult?, skuDetailsList: MutableList<SkuDetails>?) {
        if (billingResult!!.responseCode != BillingClient.BillingResponseCode.OK && skuDetailsList!!.isNotEmpty()) {
            isNeedClosePurchaseLiveData.postValue(true)
            return
        }
        uiScope.launch {
            try {
                loadSubscriptions(skuDetailsList!!)
            } catch (e: Exception) {
                Log.d("PurchaseViewModel", "PurchaseViewModel loadSubscriptions - error = ${e.message}")
            } finally {
                isNeedShowProgressBarLiveData.postValue(false)
            }
        }
    }

    override fun onBillingSetupFinished(billingResult: BillingResult?) {
        if (billingResult!!.responseCode != BillingClient.BillingResponseCode.OK) {
            isNeedClosePurchaseLiveData.postValue(true)
            return
        }
        uiScope.launch {
            try {
                isNeedShowProgressBarLiveData.postValue(true)
                purchaseFromBack = ksApi.getPurchases()
                if (purchaseFromBack.isEmpty()) isNeedClosePurchaseLiveData.postValue(true)

                val skuList = mutableListOf<String>()
                purchaseFromBack.forEach { skuList.add(it.identifier) }
                getSkuListLiveData.postValue(skuList)
            } catch (e: Exception) {
                isNeedClosePurchaseLiveData.postValue(true)
                Log.d("PurchaseViewModel", "PurchaseViewModel loadSubscriptions - error = ${e.message}")
            }
        }
    }

    val isNeedShowProgressBarLiveData = MutableLiveData<Boolean>()
    val updateSubscriptionsListLiveData = MutableLiveData<MutableList<PurchaseRecyclerItem>>()
    val showPurchaseDialogLiveData = MutableLiveData<PurchaseRecyclerItem>()
    val showAlertTextLiveData = MutableLiveData<String>()
    val getSkuListLiveData = MutableLiveData<MutableList<String>>()
    val isNeedClosePurchaseLiveData = MutableLiveData<Boolean>()


    private val purchaseItems = mutableListOf<PurchaseRecyclerItem>()
    private var purchaseFromBack: MutableList<PurchaseInfo> = mutableListOf()

    private val skuDetailsMap = HashMap<String, SkuDetails>()

    private suspend fun loadSubscriptions(purchasesFromGoogle: MutableList<SkuDetails>) {
        withContext(Dispatchers.IO) {
            purchaseItems.clear()

            purchasesFromGoogle.forEach { skuDetailsMap[it.sku] = it }

            purchaseFromBack.forEach { info ->

                val googleSkuDetails = skuDetailsMap[info.identifier] ?: return@forEach

                info.name = googleSkuDetails.title
                info.priceString = googleSkuDetails.price
                info.cost = googleSkuDetails.priceAmountMicros.toInt()
                info.priceType = googleSkuDetails.priceCurrencyCode
                info.details = googleSkuDetails

                val item = PurchaseRecyclerItem(info)
                item.onClickListener = View.OnClickListener {
                    showPurchaseDialogLiveData.postValue(item)
                    showPurchaseDialogLiveData.value = null
                }

                purchaseItems.add(item)
            }
            updateSubscriptionsListLiveData.postValue(ArrayList(purchaseItems))
        }
    }

    override fun onBillingServiceDisconnected() {
        //Do nothing
    }

}