package com.keepsolid.vpn.buddy.vpn


import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.keepsolid.vpn.buddy.base.BaseActivity
import com.keepsolid.vpn.buddy.base.IActivityCallbacks
import com.keepsolid.vpn.buddy.base.ViewModelFactory
import com.keepsolid.vpn.buddy.interstitial.InterstitialFragment
import com.keepsolid.vpn.buddy.purchase.PurchaseFragment
import com.keepsolid.vpn.buddy.servers.ServersFragment
import com.keepsolid.vpn.buddy.servers.ServersViewModel
import com.keepsolid.vpn.buddy.settings.SettingsFragment
import com.keepsolid.vpn.buddy.settings.SettingsViewModel
import com.keepsolid.vpn.buddy.settings.killswitch.KillSwitchFragment
import com.keepsolid.vpn.buddy.settings.protocol.ProtocolsFragment
import com.keepsolid.vpn.buddy.settings.trustednet.TrustedNetworksFragment
import com.keepsolid.vpn.buddy.utils.NavigationManager
import dagger.Lazy
import javax.inject.Inject
import android.R
import com.noelchew.ncapprating.library.NcAppRating
import com.noelchew.ncapprating.library.NcAppRatingConfig
import com.noelchew.ncapprating.library.NcAppRatingListener
import com.noelchew.ncapprating.library.PlayStoreUtil




class VpnActivity : BaseActivity(), IActivityCallbacks {

    @Inject
    internal lateinit var providerVpnFragment: Lazy<VpnFragment>
    @Inject
    internal lateinit var providerServersFragment: Lazy<ServersFragment>
    @Inject
    internal lateinit var providerSettingsFragment: Lazy<SettingsFragment>
    @Inject
    internal lateinit var providerPurchaseFragment: Lazy<PurchaseFragment>
    @Inject
    internal lateinit var providerInterstitialFragment: Lazy<InterstitialFragment>
    @Inject
    internal lateinit var providerProtocolsFragment: Lazy<ProtocolsFragment>
    @Inject
    internal lateinit var providerKillSwitchFragment: Lazy<KillSwitchFragment>
    @Inject
    internal lateinit var providerTrustedNetworksFragment: Lazy<TrustedNetworksFragment>
    @Inject
    internal lateinit var viewModelFactory: ViewModelFactory

    private lateinit var modelServers: ServersViewModel
    private lateinit var modelSettings: SettingsViewModel

    override fun getLayoutRes(): Int = com.keepsolid.vpn.buddy.R.layout.activity_sign_in

    override fun getContainerId() = com.keepsolid.vpn.buddy.R.id.fragment_container

    override fun getDefaultFragment() = providerVpnFragment.get()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        modelServers = ViewModelProviders.of(this, viewModelFactory).get(ServersViewModel::class.java)
        modelSettings = ViewModelProviders.of(this, viewModelFactory).get(SettingsViewModel::class.java)

        modelServers.showVpnFragmentLiveData.observe(this, Observer<Boolean> { if (it) showVpnFragment() })
        modelSettings.showVpnFragmentLiveData.observe(this, Observer<Boolean> { if (it) showVpnFragment() })
        modelSettings.showSigInInActivityLiveData.observe(this, Observer<Boolean> { if (it) showSignInActivity() })

//        showRatingDialog()
    }

    private fun showRatingDialog() {

        val ncAppRatingListener = object : NcAppRatingListener {
            override fun onOpenMarket(rating: Int) {

                // opens Google Play
                PlayStoreUtil.rateUs(this@VpnActivity)
            }

            override fun onNoClicked() {
            }

            override fun onCancelClicked() {
            }

            override fun onShowFeedbackDialog(rating: Int) {
                // TODO: implement your own feedback mechanism here
            }
        }


        val config = NcAppRatingConfig(0, 1, 3, ncAppRatingListener)
        var ncAppRating = NcAppRating(this, config)
        ncAppRating.showRateDialogIfNeeded()
    }

    override fun showVpnFragment() {
        modelServers.showVpnFragmentLiveData.value = false
        supportFragmentManager.beginTransaction()
                .replace(getContainerId(), providerVpnFragment.get())
                .commitAllowingStateLoss()
    }

    override fun showServersFragment() {
        supportFragmentManager.beginTransaction()
                .replace(getContainerId(), providerServersFragment.get())
                .commitAllowingStateLoss()
    }

    override fun showSettingsFragment() {
        supportFragmentManager.beginTransaction()
                .replace(getContainerId(), providerSettingsFragment.get())
                .commitAllowingStateLoss()
    }

    override fun showSubscriptionsFragment() {
        supportFragmentManager.beginTransaction()
                .replace(getContainerId(), providerPurchaseFragment.get())
                .commitAllowingStateLoss()
    }

    override fun showInterstitialFragment() {
        supportFragmentManager.beginTransaction()
                .replace(getContainerId(), providerInterstitialFragment.get())
                .commitAllowingStateLoss()
    }

    override fun showProtocolsFragment() {
        supportFragmentManager.beginTransaction()
                .replace(getContainerId(), providerProtocolsFragment.get())
                .commitAllowingStateLoss()
    }

    override fun showKillSwitchFragment() {
        supportFragmentManager.beginTransaction()
                .replace(getContainerId(), providerKillSwitchFragment.get())
                .commitAllowingStateLoss()
    }

    override fun showTrustedNetworksFragment() {
        supportFragmentManager.beginTransaction()
                .replace(getContainerId(), providerTrustedNetworksFragment.get())
                .commitAllowingStateLoss()
    }

    private fun showSignInActivity() {
        modelSettings.showSigInInActivityLiveData.value = false
        NavigationManager.openSignInActivity(this)
        finish()
    }
}
