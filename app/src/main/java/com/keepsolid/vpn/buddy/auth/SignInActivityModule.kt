package com.keepsolid.vpn.buddy.auth

import com.keepsolid.vpn.buddy.di.FragmentScoped
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class SignInActivityModule {

    @FragmentScoped
    @ContributesAndroidInjector
    internal abstract fun provideSignInFragment(): SignInFragment

}