package com.keepsolid.vpn.buddy.utils.models;

public enum PurchasePeriodType {

    DAY, YEAR, MONTH;

    public static PurchasePeriodType parse(String val) {

        PurchasePeriodType type;

        try {
            type = valueOf(val.toUpperCase());
        } catch (IllegalArgumentException | NullPointerException e) {
            type = null;
        }

        return type;

    }


    @Override
    public String toString() {
        return name().toLowerCase();
    }

    public int days() {
        switch (this) {
            case DAY:
                return 1;
            case MONTH:
                return 30;
            case YEAR:
                return 365;
        }

        return 0;
    }
}
