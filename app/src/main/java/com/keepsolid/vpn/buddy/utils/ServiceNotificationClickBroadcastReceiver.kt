package com.keepsolid.vpn.buddy.utils

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import dagger.android.AndroidInjection
import javax.inject.Inject

class ServiceNotificationClickBroadcastReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {
        AndroidInjection.inject(this, context)
        NavigationManager.openSignInActivity(context, true)
    }

}