package com.keepsolid.vpn.buddy.utils

import android.content.Context
import android.text.TextUtils
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ShowMsgManager @Inject constructor(private val context: Context) {

    companion object {

        internal fun showAlertDialog(context: Context, title: String? = null, msg: String,
                                     positiveBtnTitle: String, positiveBtnCallback: Runnable? = null,
                                     negativeBtnTitle: String? = null, negativeBtnCallback: Runnable? = null): AlertDialog {

            val builder = AlertDialog.Builder(context)
            builder.setMessage(msg)
            builder.setPositiveButton(positiveBtnTitle) { _, _ -> positiveBtnCallback?.run() }
            if (!TextUtils.isEmpty(title)) builder.setTitle(title)
            if (!TextUtils.isEmpty(negativeBtnTitle)) builder.setNegativeButton(negativeBtnTitle) { _, _ -> negativeBtnCallback?.run() }

            return builder.create()
        }

        internal fun getLongToast(context: Context, msg: String) = Toast.makeText(context, msg, Toast.LENGTH_LONG)

        internal fun getShortToast(context: Context, msg: String) = Toast.makeText(context, msg, Toast.LENGTH_SHORT)

        internal fun showLongToast(context: Context, msg: String) = Toast.makeText(context, msg, Toast.LENGTH_LONG).show()

        internal fun showShortToast(context: Context, msg: String) = Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()

    }


}