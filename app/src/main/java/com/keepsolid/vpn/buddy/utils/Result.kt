package com.keepsolid.vpn.buddy.utils

data class Result<out T>(val success: T? = null, val error: Throwable? = null, val exception: Exception? = null) {

    fun isSuccess() = success != null && !isException() && !isError()

    fun isException() = exception != null

    fun isError() = error != null


}