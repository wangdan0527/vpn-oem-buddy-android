package com.keepsolid.vpn.buddy.settings.protocol

import android.text.TextUtils
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.AppCompatRadioButton
import androidx.core.content.res.ResourcesCompat
import com.keepsolid.vpn.buddy.R
import com.keepsolid.vpn.buddy.utils.recycler.AbstractRecyclerItem
import com.keepsolid.vpn.buddy.utils.recycler.AbstractViewHolder
import com.keepsolid.vpn.buddy.utils.recycler.ProtocolRecyclerItem

class ProtocolViewHolder(itemView: View) : AbstractViewHolder(itemView) {

    val name: TextView = itemView.findViewById(R.id.standard_recycler_item_title)
    private val description: TextView = itemView.findViewById(R.id.standard_recycler_item_subtitle)
    private val radio: AppCompatRadioButton = itemView.findViewById(R.id.rb_radio_button)
    private val holder: View = itemView.findViewById(R.id.standard_recycler_item_cv)

    override fun buildView(metadata: AbstractRecyclerItem) {
        val recyclerItem = metadata as ProtocolRecyclerItem

        name.text = recyclerItem.protoname

        radio.setOnCheckedChangeListener(null)
        radio.isClickable = false
        radio.isChecked = recyclerItem.isChecked

        if (TextUtils.isEmpty(recyclerItem.protoDesc)) {
            description.visibility = View.GONE
        } else {
            description.text = recyclerItem.protoDesc
            description.visibility = View.VISIBLE
        }

        radio.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) {
                holder.requestFocus()
            }
        }

        holder.setOnClickListener(recyclerItem.onClickListener)

        if (recyclerItem.isEnabled) {
            radio.isEnabled = true
            holder.isClickable = true
            holder.isFocusable = true
            itemView.isClickable = true
            name.setTextColor(ResourcesCompat.getColor(name.resources, R.color.colorTextBlack, null))
        } else {
            holder.isClickable = false
            holder.isFocusable = false
            radio.isEnabled = false
            itemView.isClickable = false
            name.setTextColor(ResourcesCompat.getColor(name.resources, R.color.serversTitleColor, null))
        }

    }

}
